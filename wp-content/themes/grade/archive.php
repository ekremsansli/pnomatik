<?php
defined('ABSPATH') or die();
/**
 * archive
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Grade
 * @since Grade 1.0
 */

get_header(); 

get_template_part('pagetemplates/scrollingsidebar');

$sidebar=is_active_sidebar( 'detheme-sidebar' )?'detheme-sidebar':false;
$sidebar_position= (!$sidebar) ? "nosidebar" : get_grade_sidebar_position();

set_query_var('sidebar',$sidebar);
$class_sidebar = $sidebar_position;
$vertical_menu_container_class = (get_grade_option('dt-header-type')=='leftbar')?" vertical_menu_container":"";
?>
<div class="content <?php print sanitize_html_class($class_sidebar)." ".sanitize_html_class($vertical_menu_container_class); ?>">
	<div class="container">
		<div class="row">

		<?php if(get_grade_option('dt-show-title-page')):?>
		<h1 class="page-title"><?php print get_grade_option('page-title');?></h1>
		<?php endif;?>


<?php if ($sidebar_position=='nosidebar') { ?>
			<div class="col-sm-12">
<?php	} else { ?>
			<div class="col-sm-8 <?php print ($sidebar_position=='sidebar-left')?" col-sm-push-4":"";?> col-md-9 <?php print ($sidebar_position=='sidebar-left')?" col-md-push-3":"";?>">
<?php	} ?>

			<?php if ( have_posts() ) : ?>

				<?php if ( category_description() ) : // Show an optional category description ?>
				<div class="archive-meta"><?php echo category_description(); ?></div>
				<?php endif; ?>

			<?php 
				$i = 0;
				$reveal_area_class = '';
				while ( have_posts() ) : the_post();
					$i++;
					if ($i==1) :
			?>
						<div class="blank-reveal-area"></div>
			<?php endif; ?>

				<?php get_template_part( 'content', get_post_format() ); ?>
				<div class="clearfix">
					<div class="col-xs-12 postseparator"></div>
				</div>
			<?php endwhile; ?>



			<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
			<?php endif; ?>	

				<!-- Pagination -->
				<div class="row">
					<?php get_template_part('pagetemplates/pagination'); ?>
				</div>

		</div>
<?php if ('sidebar-right'==$sidebar_position) { ?>
			<div class="col-sm-4 col-md-3 sidebar">
				<?php get_sidebar(); ?>
			</div>
<?php }
	elseif ($sidebar_position=='sidebar-left') { ?>
			<div class="col-sm-4 col-md-3 sidebar col-sm-pull-8 col-md-pull-9">
				<?php get_sidebar(); ?>
			</div>
<?php }?>

		</div>
	</div>
</div>	
<?php get_footer(); ?>