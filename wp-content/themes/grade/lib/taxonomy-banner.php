<?php
defined('ABSPATH') or die();

function grade_add_custom_column_banner_image($taxonomy){

    add_action("{$taxonomy}_add_form_fields", 'grade_edit_taxonomy_field_banner',999);
    add_action("{$taxonomy}_edit_form_fields", 'grade_edit_taxonomy_field_banner',999);
}

function grade_category_banner_install(){

  if(get_grade_option('dt-show-banner-page')!='featured' || !get_grade_option('show-banner-area'))
    return;

  $taxonomies=get_taxonomies();

  if(!$taxonomies) return;


  add_action( 'edit_term', "grade_category_banner_save_form", 10, 3 );
  add_action( 'create_term', "grade_category_banner_save_form", 10, 3 );

  foreach ($taxonomies as $taxonomy) {
    grade_add_custom_column_banner_image($taxonomy);
  }
}

function grade_category_banner_save_form($term_id){


    $attachment_id = isset($_POST['category_banner_image']) ? (int) $_POST['category_banner_image'] : null;

    $old_image=get_term_meta($term_id, '_category_banner_image', true);
    $new_image='';

    if (! is_null($attachment_id) && $attachment_id > 0 && !empty($attachment_id)) {
      $new_image=$attachment_id;
    }

    update_term_meta($term_id, '_category_banner_image', $new_image, $old_image);


}


function grade_edit_taxonomy_field_banner($tag){

    wp_enqueue_media();
    wp_enqueue_script('detheme-media', get_template_directory_uri() . '/lib/js/media.min.js', array('jquery','media-views','media-editor'),'',true);

    $term_id= is_object($tag) && $tag->term_id ? $tag->term_id : 0;

    $category_banner_image=get_term_meta($term_id, '_category_banner_image', true);
    $banner_url="";

  if($category_banner_image){

    $banner_image_data=wp_get_attachment_metadata(intval($category_banner_image));

    if(isset($banner_image_data['mime_type']) && preg_match('/video/', $banner_image_data['mime_type'])){

        $video_url=wp_get_attachment_url(intval($banner_image));

        $videoformat="video/mp4";
        if(is_array($banner_image_data) && $banner_image_data['mime_type']=='video/webm'){
             $videoformat="video/webm";
        }

        $banner_url='<video autoplay width="266">';
        $banner_url.="<source src=\"".esc_url($video_url)."\" type=\"".$videoformat."\" />";
        $banner_url.="</video>";

    }
    else{

      $banner_url = wp_get_attachment_image( $category_banner_image, array( 266,266 ));
    }
  }
?>
<table class="form-table"> 
<tr class="form-field">
    <th scope="row">
        <label for="taxonomy_banner_image"><?php esc_html_e('Banner','grade') ?></label>
    </th>
    <td>
        <div class="detheme-field-image page-banner">
          <input type="hidden" name="category_banner_image" id="category_banner_image" value="<?php echo $category_banner_image; ?>" />
          <p class="preview-image">
          <a title="<?php esc_html_e('Set Page Banner','grade');?>" href="#" id="set-page-banner" data-type="image,video"  data-select-label="<?php esc_html_e('Select Banner','grade');?>" data-insert-label="<?php esc_html_e('Insert Banner','grade');?>" class="add_detheme_image"><?php echo (""!==$banner_url)?$banner_url:esc_html__('Set Page Banner','grade');?></a>
          </p>
          <a title="<?php esc_html_e('Remove Page Banner','grade');?>" style="display:<?php echo (""==$banner_url)?"none":"block";?>" href="#" id="clear-page-banner" class="remove_detheme_image"><?php esc_html_e('Remove Page Banner','grade');?></a>
        </div>
    </td>
</tr>
</table>
<?php

}
add_action('admin_init','grade_category_banner_install');
?>