<?php
defined('ABSPATH') or die();

function grade_add_font_selector($buttons) {    
    
    $buttons[] = 'fontsizeselect';

    return $buttons;
}

add_filter('mce_buttons_2', 'grade_add_font_selector');

function grade_get_font_sizes($in){
    $in['fontsize_formats']=esc_html__("Bigger",'grade')."=1.2em ".esc_html__('Big','grade')."=1.1em ".esc_html__("Small",'grade')."=0.9em ".esc_html__("Smaller",'grade')."=0.8em";
 return $in;
}

add_filter('tiny_mce_before_init', 'grade_get_font_sizes');
?>