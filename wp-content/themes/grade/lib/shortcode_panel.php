<?php
defined('ABSPATH') or die();

/**
 * @package WordPress
 * @subpackage Grade
 */

$type=esc_attr(trim($_GET['type'] ));
$errors = array();
if ( !empty($_POST) ) {
	$return = grade_configuration_form_handler($type);
	if ( is_string($return) )
		return $return;
	if ( is_array($return) )
		$errors = $return;
}

return grade_popup_configuration_form($type,$errors);

function grade_configuration_form_handler($type){

	switch($type){
		case 'button':
		$text=esc_attr($_POST['text']);
		$style=esc_attr($_POST['style']);
		$url=esc_url($_POST['url']);
		$size=esc_attr($_POST['size']);
		$target=esc_attr($_POST['target']);
		$skin=esc_attr($_POST['skin']);

		if(!empty($text)){
			
			$errors=grade_popup_send_to_editor(array('type'=>$type,'style'=>$style,'text'=>$text,'url'=>$url,'size'=>$size,'target'=>$target,'skin'=>$skin));
		}
		else{
			
			$errors=array('errors'=>array('style'=>$style,'text'=>$text,'url'=>$url,'size'=>$size,'target'=>$target,'skin'=>$skin));
		}
		break;	
		case 'icon':
		$icon=esc_attr($_POST['icon']);
		$size=esc_attr($_POST['size']);
		$color=esc_attr($_POST['color']);
		$style=esc_attr($_POST['style']);

		if(count($icon)){
			
			$errors=grade_popup_send_to_editor(array('type'=>$type,'icon'=>$icon,'size'=>$size,$icon,'color'=>$color,$icon,'style'=>$style));
		}
		else{
			
			$errors=array('errors'=>array('icon'=>$icon,'size'=>$size,$icon,'color'=>$color,$icon,'style'=>$style));
		}
		break;
		case 'counto':
		$number=intval($_POST['number']);
		$sepcolor=esc_attr($_POST['sepcolor']);

		if(!empty($number)){
			$errors=grade_popup_send_to_editor(array('type'=>$type,'number'=>$number,'sepcolor'=>$sepcolor));
		}
		else{
			
			$errors=array('errors'=>array('number'=>$number,'sepcolor'=>$sepcolor));
		}
		break;
		default:
		break;
	}
	return $errors;
}

function grade_ajax_load_script(){

	wp_dequeue_script('installer-admin');

	wp_enqueue_style( 'popup-style',get_template_directory_uri() . '/lib/css/popup.css', array(), '', 'all' );
	wp_enqueue_script( 'icon-picker',get_template_directory_uri() . '/lib/js/icon_picker.js', array('jquery'));
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.css', array(), '3.0' );	
	wp_enqueue_style( 'icon_picker-font',get_template_directory_uri() . '/lib/css/fontello.css', array(), '', 'all' );
	wp_enqueue_style( 'wp-color-picker' );
   	wp_localize_script( 'icon-picker', 'picker_i18nLocale', array(
      'search_icon'=>esc_html__('Search Icon','grade'),
    ) );
}



function grade_popup_configuration_form($type,$errors=array()){

	add_action( 'wp_enqueue_scripts','grade_ajax_load_script');

	?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<?php wp_head();?>
	</head>
	<body>
		<div id="jayd-popup">
			<div id="jayd-shortcode-wrap">
				<div id="jayd-sc-form-wrap">
					<div id="jayd-sc-form-head"><h2>DT <?php print ucwords($type);?></h2></div>
					<form method="post" action="" id="jayd-sc-form">
						<table cellspacing="2" cellpadding="5" id="jayd-sc-form-table" class="form-table">
							<tbody>
								<?php 
								if($type=='button'){

									$styles=array(
										'color-primary'=>esc_html__('Primary','grade'),
										'color-secondary' => esc_html__('Secondary','grade'),
										'ghost'=>esc_html__('Ghost Button','grade'),
										'link'=>esc_html__('Link','grade'),
										'underlined'=>esc_html__('Underlined','grade'),
										'rounded'=>esc_html__('Rounded','grade'),
										);

									$sizes = array(
										'btn-lg' => esc_html__('Large','grade'),
										'btn-default' => esc_html__('Default','grade'),
										'btn-sm' => esc_html__('Small','grade'),
										'btn-xs' => esc_html__('Extra small','grade')
										);

									$errors=wp_parse_args($errors,array('size'=>'','url'=>'','style'=>'','target'=>'','text'=>'','skin'=>'dark'));

									?>
									<tr>
										<td><label><?php esc_html_e('Button URL','grade');?></label></td>
										<td><input class="form-control" type="text" maxlength="100" name="url" id="url" class="jayd-form-input" value="<?php print $errors['url'];?>" /> 
											<span class="child-clone-row-desc"><?php esc_html_e('Add the button\'s url eg http://example.com','grade');?></span>
										</td>
									</tr>
									<tr>
										<td><label><?php esc_html_e("Button skin", 'grade');?></label></td>
										<td>
										<select class="jayd-form-select form-control" name="skin" id="skin">
										<option value="dark" <?php echo(($errors['skin']=='dark')?" selected=\"selected\"":"");?>><?php esc_html_e('Dark (default)','grade');?></option> 
										<option value="light" <?php echo(($errors['skin']=='light')?" selected=\"selected\"":"");?>><?php esc_html_e('Light','grade');?></option> 
										</select>
										<span class="child-clone-row-desc"><?php esc_html_e('Select the button\'s skin','grade');?></span> 
										</td>
									</tr>
									<tr>
										<td><label><?php esc_html_e("Button style", 'grade');?></label></td>
										<td>
										<select class="jayd-form-select form-control" name="style" id="style">
											<?php 	
											if($styles){

												foreach ( $styles as $style=>$label ){

													echo "<option value=\"".$style."\" ".(($style==$errors['style'])?" selected=\"selected\"":"").">".$label."</option>"; 
												}
											}
											?>

										</select> 
										<span class="child-clone-row-desc"><?php esc_html_e('Select the button\'s style, ie the button\'s colour','grade');?></span>
										</td>
									</tr>
								<tr>
									<td><label><?php esc_html_e("Button size", 'grade');?></label></td>
									<td><select class="jayd-form-select form-control" name="size" id="size">
										<?php 	
										if($sizes){

											foreach ( $sizes as $size=>$label ){

												echo "<option value=\"".$size."\" ".(($size==$errors['size'])?" selected=\"selected\"":"").">".$label."</option>"; 
											}
										}
										?>

									</select> <span class="child-clone-row-desc">Select the icon's size</span></td>
								</tr>
								<tr>
									<td><label><?php esc_html_e('Button Target','grade');?></label></td>
									<td><select class="jayd-form-select form-control" name="target" id="t	arget">
										<option value="_self" <?php echo(($errors['target']=='_self')?" selected=\"selected\"":"");?>><?php esc_html_e('Self','grade');?></option> 
										<option value="_blank" <?php echo(($errors['target']=='_blank')?" selected=\"selected\"":"");?>><?php esc_html_e('Blank','grade');?></option> 
									</select> <span class="child-clone-row-desc"><?php esc_html_e('Select the button\'s target','grade');?></span></td>
								</tr>
								<tr>
									<td><label><?php esc_html_e('Button Text','grade');?></label></td>
									<td><input class="form-control" type="text" name="text" maxlength="100" id="text" class="jayd-form-input" value="<?php print $errors['text'];?>"/> 
										<span class="child-clone-row-desc"><?php esc_html_e('Select the button\'s text','grade');?></span></td>
									</tr>

									<?php }
									elseif($type=='counto'){

								
									$errors=wp_parse_args($errors,array('number'=>'100','sepcolor'=>''));
								?>
								<tr>
									<td><label><?php esc_html_e('Number','grade');?></label></td>
									<td><input class="form-control" type="text" name="number" maxlength="100" id="number" class="jayd-form-input" value="<?php print $errors['number'];?>"/> 
										<span class="child-clone-row-desc"><?php esc_html_e('The value must be numeric','grade');?></span></td>
								</tr>
								<tr>
									<td><label><?php esc_html_e('Separator Color','grade');?></label></td>
									<td><input class="form-control wp-color-picker" type="text" name="sepcolor" id="sepcolor" class="jayd-form-input" value="<?php print $errors['sepcolor'];?>"/> 
								</tr>

									<?php }
									elseif($type=='icon'){

										$icons=(function_exists('detheme_font_list'))?detheme_font_list():apply_filters('detheme_font_list',array());

										$sizes = array(
											'' => esc_html__('Default','grade'),
											'size-sm' => esc_html__('Small','grade'),
											'size-md' => esc_html__('Medium','grade'),
											'size-lg' => esc_html__('Large','grade')
											);

										$errors=wp_parse_args($errors,array('size'=>'','color'=>'','style'=>'square'));

										?>
										<tr>
											<td><label><?php esc_html_e('Icon Size','grade');?></label></td>
											<td><select class="form-control jayd-form-select" name="size" id="size">
												<?php 

												if($sizes){

													foreach ( $sizes as $size=>$label ){

														echo "<option value=\"".$size."\" ".(($size==$errors['size'])?" selected=\"selected\"":"").">".$label."</option>"; 
													}
												}
												?>

											</select> <span class="child-clone-row-desc"><?php esc_html_e("Select the button's size",'grade');?></span></td>
										</tr>
										<tr>
											<td><label><?php esc_html_e('Icon Color','grade');?></label></td>
											<td><select class="jayd-form-select form-control" name="color" id="color">
												<option value="" <?php echo(($errors['color']=='')?" selected=\"selected\"":"");?>><?php esc_html_e('Default','grade');?></option> 
												<option value="primary" <?php echo(($errors['color']=='primary')?" selected=\"selected\"":"");?>><?php esc_html_e('Primary','grade');?></option> 
												<option value="secondary" <?php echo(($errors['color']=='secondary')?" selected=\"selected\"":"");?>><?php esc_html_e('Secondary','grade');?></option> 
											</select> <span class="child-clone-row-desc"><?php esc_html_e('Select the button\'s color','grade');?></span></td>
										</tr>
										<tr>
											<td><label><?php esc_html_e('Icon Style','grade');?></label></td>
											<td><select class="jayd-form-select form-control" name="style" id="style">
												<option value=""><?php esc_html_e('None','grade');?></option> 
												<option value="circle" <?php echo(($errors['style']=='circle')?" selected=\"selected\"":"");?>><?php esc_html_e('Circle','grade');?></option> 
												<option value="square" <?php echo(($errors['style']=='square')?" selected=\"selected\"":"");?>><?php esc_html_e('Square','grade');?></option> 
												<option value="ghost" <?php echo(($errors['style']=='ghost')?" selected=\"selected\"":"");?>><?php esc_html_e('Ghost','grade');?></option> 
												</select> <span class="child-clone-row-desc"><?php esc_html_e('Select the button\'s style','grade');?></span></td>
											</tr>

											<tr>
											<td><label><?php esc_html_e('Icon','grade');?></label></td>
											<td><?php if(count($icons)):?>
													<script type="text/javascript">
													jQuery(document).ready(function($){

														var options={
															icons:new Array('<?php print @implode("','",$icons);?>')
														};

														$(".icon-picker").iconPicker(options);
													});

													</script>
													<input type="text" class="icon-picker" id="icon" name="icon" value="" />
												<?php endif;?>
											</td>
										</tr>
										<?php }
										?>
									</tbody>
								</table>
								<br/>
								<center>
									<input type="submit" id="form-insert" class="btn btn-default content_jayd_button" value="<?php esc_html_e('Insert Shortcode','grade');?>">
								</center>
						</form>
					</div>
				</div>
			</div>
		</body>
		</html>
		<?php }

		function grade_popup_send_to_editor($options=array()) {

			$string="";

			switch ($options['type']){
				case 'button':	$string="[dt_button url=\"".$options['url']."\" style=\"".$options['style']."\" size=\"".$options['size']."\" skin=\"".$options['skin']."\" target=\"".$options['target']."\"]".$options['text']."[/dt_button]";
				break;
				case 'counto':	$string="[dt_counto to=\"".$options['number']."\"".
				((!empty($options['sepcolor']))?" sepcolor=\"".esc_attr($options['sepcolor'])."\"":"").
				"]".$options['number']."[/dt_counto]";
				break;
				case 'icon':
				$string.="[dticon ico=\"".$options['icon']."\"".
				((!empty($options['size']))?" size=\"".$options['size']."\"":"").
				((!empty($options['color']))?" color=\"".$options['color']."\"":"").
				((!empty($options['style']))?" style=\"".$options['style']."\"":"")."][/dticon]";
				break;

			}
			$string=preg_replace("/\r\n|\n|\r/","<br/>",$string);

			?>
			<script type="text/javascript">

			/* <![CDATA[ */
			var win = window.dialogArguments || opener || parent || top;
			if(win.tinyMCE)
			{

				win.send_to_editor('<?php echo addslashes($string); ?>');
win.tb_remove();
}
else if(win.send_to_editor)
{
	win.send_to_editor('<?php echo addslashes($string); ?>');
	win.tb_remove();
}


/* ]]> */
</script>
<?php
exit;
}
?>
