<?php 
defined('ABSPATH') or die();

add_filter('detheme_options_config','grade_configuration');

function grade_configuration($grade_config){

	$grade_config['body_background']=$grade_config['body_tag']="";
	$post_id=get_the_id();

	if(!in_array($post_type=get_post_type(),grade_post_use_sidebar_layout()) && isset($grade_config['layout_'.$post_type])){
		$grade_config['layout']=$grade_config['layout_'.$post_type];
	}

	$sidebars=wp_get_sidebars_widgets();

	if(!isset($sidebars['detheme-scrolling-sidebar']) || !count($sidebars['detheme-scrolling-sidebar'])){
	    $grade_config['dt_scrollingsidebar_on']=false;
	}

	if(is_front_page()){

		$post_id = get_option('page_on_front');

		if(function_exists('is_shop') && is_shop()){
			 $post_id=get_option( 'woocommerce_shop_page_id');
		}
		if($post_id){

			 $grade_config['page-title']=get_the_title($post_id);
			
			if(get_post_meta( $post_id,'_hide_loader', true )){
				$grade_config['page_loader']=false;
			}


			if($hide_title=get_post_meta( $post_id, '_hide_title', true )){
				$grade_config['dt-show-title-page']=false;
			}

			$grade_config['show-banner-area']=$grade_config['show-banner-area'] && $grade_config['show-banner-homepage'] && !get_post_meta( $post_id,'_hide_banner', true );

			$page_background=get_post_meta( $post_id,'_page_background', true );
			$background_style=get_post_meta( $post_id, '_background_style', true );
			$page_background_color=get_post_meta( $post_id,'_page_background_color', true );

			$background=grade_getBackgroundStyle($page_background,$background_style,$page_background_color);

			if($background){

				$grade_config['body_background']="body{".$background['css']."}";
				$grade_config['body_tag']=($background_style!='default')?$background['body']:
				($grade_config['body_background_style']=='parallax'|| $grade_config['body_background_style']=='parallax_all')?" data-speed=\"3\" data-type=\"background\" ":"";

			}

		}
		else{
			 $grade_config['page-title']=get_bloginfo('name');
			 $grade_config['show-banner-area']=$grade_config['show-banner-area'] && $grade_config['show-banner-homepage'];
		}



	}
	elseif(is_page()){

		$grade_config['page-title']=the_title('','',false);

		if(get_post_meta( $post_id,'_hide_loader', true )){
			$grade_config['page_loader']=false;
		}

		$grade_config['show-banner-area']=$grade_config['show-banner-area'] && !get_post_meta( $post_id,'_hide_banner', true );

		if($hide_title=get_post_meta( $post_id, '_hide_title', true )){
			$grade_config['dt-show-title-page']=false;
		}

		$page_background=get_post_meta( $post_id,'_page_background', true );
		$background_style=get_post_meta( $post_id, '_background_style', true );
		$page_background_color=get_post_meta( $post_id,'_page_background_color', true );

		$background=grade_getBackgroundStyle($page_background,$background_style,$page_background_color);

		if($background){

			$grade_config['body_background']="body{".$background['css']."}";
			$grade_config['body_tag']=($background_style!='default')?$background['body']:
			($grade_config['body_background_style']=='parallax'|| $grade_config['body_background_style']=='parallax_all')?" data-speed=\"3\" data-type=\"background\" ":"";

		}

	}
	elseif(is_category()){


		$grade_config['page-title']=sprintf(esc_html__('Category : %s','grade'), single_cat_title( ' ', false ));

	}
	elseif(is_archive()){

		if(is_tag()){
			$title=sprintf(esc_html__('Tag : %s','grade'), single_tag_title( ' ', false ));
		}
		elseif(is_tax()){
			$title=single_tag_title( ' ', false );
		}
		elseif(function_exists('is_shop') && is_shop()){

			if (!empty($grade_config['dt-shop-title-page'])) {
				$title = $grade_config['dt-shop-title-page'];
			} else {
				$title=woocommerce_page_title(false);	
			}

			$post_id=get_option( 'woocommerce_shop_page_id');

			if(get_post_meta( $post_id,'_hide_loader', true )){
				$grade_config['page_loader']=false;
			}

			if(get_post_meta( $post_id,'_hide_banner', true )){
				$grade_config['show-banner-area']=false;
			}

			if($hide_title=get_post_meta( $post_id, '_hide_title', true )){
				$grade_config['dt-show-title-page']=false;
				add_filter('woocommerce_show_page_title',create_function('','return false;'));
			}

			$page_background=get_post_meta( $post_id,'_page_background', true );
			$background_style=get_post_meta( $post_id, '_background_style', true );
			$page_background_color=get_post_meta( $post_id,'_page_background_color', true );

			$background=grade_getBackgroundStyle($page_background,$background_style,$page_background_color);

			if($background){

				$grade_config['body_background']="body{".$background['css']."}";
				$grade_config['body_tag']=($background_style!='default')?$background['body']:
				($grade_config['body_background_style']=='parallax'|| $grade_config['body_background_style']=='parallax_all')?" data-speed=\"3\" data-type=\"background\" ":"";

			}

		}	
		else{
			$title=sprintf((is_rtl()?esc_html__('%s : Archive','grade'):esc_html__('Archive : %s','grade')), single_month_title( ' ', false ));

		}

		$grade_config['page-title']=$title;

	}
	elseif(is_search()){
			$grade_config['page-title']=esc_html__('Search','grade');
	}
	elseif(is_home()){
		 $post_id=get_option( 'page_for_posts');
		 $title=get_the_title($post_id);
		 $grade_config['page-title']=$title;

		if(get_post_meta( $post_id,'_hide_loader', true )){
			$grade_config['page_loader']=false;
		}


		if($hide_title=get_post_meta( $post_id, '_hide_title', true )){
			$grade_config['dt-show-title-page']=false;
		}


		$grade_config['show-banner-area']=$grade_config['show-banner-area'] && !get_post_meta( $post_id,'_hide_banner', true );

		$page_background=get_post_meta( $post_id,'_page_background', true );
		$background_style=get_post_meta( $post_id, '_background_style', true );
		$page_background_color=get_post_meta( $post_id,'_page_background_color', true );

		$background=grade_getBackgroundStyle($page_background,$background_style,$page_background_color);

		if($background){

			$grade_config['body_background']="body{".$background['css']."}";
			$grade_config['body_tag']=($background_style!='default')?$background['body']:
			($grade_config['body_background_style']=='parallax'|| $grade_config['body_background_style']=='parallax_all')?" data-speed=\"3\" data-type=\"background\" ":"";

		}
	}
	elseif(is_404()){
		$page_background=get_post_meta( $post_id,'_page_background', true );
		$background_style=get_post_meta( $post_id, '_background_style', true );
		$page_background_color=get_post_meta( $post_id,'_page_background_color', true );

		$background=grade_getBackgroundStyle($page_background,$background_style,$page_background_color);

		if($background){

			$grade_config['body_background']="body{".$background['css']."}";
			$grade_config['body_tag']=($background_style!='default')?$background['body']:
			($grade_config['body_background_style']=='parallax'|| $grade_config['body_background_style']=='parallax_all')?" data-speed=\"3\" data-type=\"background\" ":"";

		}
	}
	elseif(function_exists('is_product')  && (is_product() || is_product_category())){

		$grade_config['page-title']=isset($grade_config['dt-shop-title-page'])?$grade_config['dt-shop-title-page']:"";

	}
	else{


		$post_id=get_the_ID();
		$post_type=get_post_type();
		$grade_config['page-title']=the_title('','',false);

		if($post_id && in_array($post_type, array_keys(get_detheme_page_attributes()))){
			if(get_post_meta( $post_id,'_hide_banner', true )){
				$grade_config['show-banner-area']=false;

			}

			$grade_config['dt-show-title-page']=!get_post_meta( $post_id, '_hide_title', true ) && (isset($grade_config[$post_type.'-title']) && (bool)$grade_config[$post_type.'-title']);

			if(get_post_meta( $post_id,'_hide_loader', true )){
				$grade_config['page_loader']=false;
			}

		}
	}

	/* banner section */

	if($grade_config['show-banner-area']){
		
		$grade_config['banner']="";
		$grade_config['bannercolor']="";
		add_filter('woocommerce_show_page_title',create_function('','return false;'));

		switch ($grade_config['dt-show-banner-page']) {
			case 'featured':
					if(function_exists('is_product')  && (is_product() || is_product_category())){
						$banner=isset($grade_config['dt-shop-banner-image'])?$grade_config['dt-shop-banner-image']:false;
						if($banner && $image=wp_get_attachment_image_src( $banner['id'], 'full' )){
							$grade_config['banner']=$image[0];
						}else{
							$grade_config['bannercolor']=(!empty($grade_config['banner-color']))?$grade_config['banner-color']:"";
						}
					}
					elseif(function_exists('is_shop') && is_shop()){

						$post_id=get_option( 'woocommerce_shop_page_id');
						$banner=$grade_config['dt-shop-banner-image'];


						if ($page_banner=get_post_meta( $post_id, '_page_banner', true )) {

								$featured_img_fullsize_url = wp_get_attachment_image_src( $page_banner, 'full' );
								$banner=(!empty($featured_img_fullsize_url['0']))?$featured_img_fullsize_url['0']:"";
								if(!empty($banner)) $grade_config['banner']=$banner;

						}
						elseif(isset($banner['id']) && $image=wp_get_attachment_image_src( $banner['id'], 'full' )){
							$grade_config['banner']=$image[0];
						}else{
							$grade_config['bannercolor']=(!empty($grade_config['banner-color']))?$grade_config['banner-color']:"";
						}

					}
					elseif(is_front_page() || is_home() || is_page() || is_single()){

		                 if($page_banner=get_post_meta( $post_id, '_page_banner', true )){

		                 	$bannerdata=wp_get_attachment_metadata(intval($page_banner));

			                 if( isset($bannerdata['mime_type']) && preg_match('/video/', $bannerdata['mime_type'])){
			                 	$grade_config['dt-show-banner-page']='video';
			                 	$grade_config['dt-banner-video']=$page_banner;
			                 }
			                 else{
								$featured_img_fullsize_url = wp_get_attachment_image_src( $page_banner, 'full' );
								$banner=(!empty($featured_img_fullsize_url['0']))?$featured_img_fullsize_url['0']:"";
								if(!empty($banner)) $grade_config['banner']=$banner;
			                 }
			             }
			             else{
							$banner=$grade_config['dt-banner-image'];
							if($banner && $image=wp_get_attachment_image_src( $banner['id'], 'full' )) {
								$grade_config['banner']=$image[0];
							} else {
								$grade_config['bannercolor']=(!empty($grade_config['banner-color']))?$grade_config['banner-color']:"";
							}

			             }
					}
					elseif(is_tax() || is_category()){

						if (get_option( 'db_version' ) >= 34370) {

							$category = get_queried_object();
							$term_id = $category->term_id;
						    $page_banner=get_term_meta($term_id, '_category_banner_image', true);
						}

						if($page_banner){

							$bannerdata=wp_get_attachment_metadata(intval($page_banner));

				                 if( isset($bannerdata['mime_type']) && preg_match('/video/', $bannerdata['mime_type'])){
				                 	$grade_config['dt-show-banner-page']='video';
				                 	$grade_config['dt-banner-video']=$page_banner;
				                 }
				                 else{
									$featured_img_fullsize_url = wp_get_attachment_image_src( $page_banner, 'full' );
									$banner=(!empty($featured_img_fullsize_url['0']))?$featured_img_fullsize_url['0']:"";
									if(!empty($banner)) $grade_config['banner']=$banner;
				                 }
						}
						elseif(($banner=$grade_config['dt-banner-image']) && $image=wp_get_attachment_image_src( $banner['id'], 'full' )){
							$grade_config['banner']=$image[0];
						} 

						else {
							$grade_config['bannercolor']=(!empty($grade_config['banner-color']))?$grade_config['banner-color']:"";
						}
					}
					elseif( is_archive() || is_search()){

						$banner=$grade_config['dt-banner-image'];

						if($banner && $image=wp_get_attachment_image_src( $banner['id'], 'full' )) {
								$grade_config['banner']=$image[0];
						} else {
							$grade_config['bannercolor']=(!empty($grade_config['banner-color']))?$grade_config['banner-color']:"";
						}
					}

				break;
			case 'image':
		
					$banner=$grade_config['dt-banner-image'];

					if(function_exists('is_product')  && (is_product() || is_shop() || is_cart() || is_checkout() || is_account_page() || is_product_category())){
						$banner=$grade_config['dt-shop-banner-image'];
					}
					elseif(function_exists('is_shop') && is_shop()){
						$banner=$grade_config['dt-shop-banner-image'];
					}

					if($banner && $image=wp_get_attachment_image_src( $banner['id'], 'full' )) { 
						$grade_config['banner']=$image[0];
					} else {
						$grade_config['bannercolor']=(!empty($grade_config['banner-color']))?$grade_config['banner-color']:"";
					}
				break;
			case 'color':
					$grade_config['bannercolor']=(!empty($grade_config['banner-color']))?$grade_config['banner-color']:"";
				break;
			case 'none':
			default:
				break;
		}

		if($grade_config['dt-show-title-page']){
			$grade_config['dt-show-banner-title']=true;
			$grade_config['dt-show-title-page']=false;
		}

	}
	else{
		$grade_config['dt-show-banner-title']=false;
		$grade_config['banner']="";
		$grade_config['bannercolor']="";

	}

	/* header section */
	if($grade_config['dt-show-header']){
		if(is_front_page() || is_grade_home(get_post())){
			$grade_config['dt-logo-image']=$grade_config['homepage-dt-logo-image'];
			$grade_config['dt-logo-image-transparent']=$grade_config['homepage-dt-logo-image-transparent'];
		}

		$grade_config['logo-width']=(!empty($grade_config['dt-logo-image']['url']) && (int)$grade_config['dt-logo-width'] > 0 )?$grade_config['dt-logo-width']:"";
		$grade_config['logo-top']=(!empty($grade_config['dt-logo-margin']) && (int)$grade_config['dt-logo-margin'] !== '10' )?(int)$grade_config['dt-logo-margin']:"10";
		$grade_config['logo-left']=(!empty($grade_config['dt-logo-leftmargin']) && (int)$grade_config['dt-logo-leftmargin'] !== '0' )?(int)$grade_config['dt-logo-leftmargin']:"";
	}
	else{
		$grade_config['logo-width']="";
		$grade_config['logo-top']="";
		$grade_config['logo-left']="";
	}

	return $grade_config;
}


if(is_single()){
	grade_set_post_views(get_the_ID()); 
}


function grade_getBackgroundStyle($image_id="",$background_style="",$background_color=""){

	$featured_img_fullsize_url = wp_get_attachment_image_src( $image_id, 'full' );
	$css_background=$parallax=$backgroundattr="";

	if($featured_img_fullsize_url){

		$css_background="background-image:url('".esc_url($featured_img_fullsize_url[0])."') !important;";

		switch($background_style){
		    case'parallax':
		        $parallax=" data-speed=\"3\" data-type=\"background\" ";
		        $backgroundattr="background-position: 0% 0%; background-repeat: no-repeat; background-size: cover";
		        break;
		    case'parallax_all':
		        $parallax=" data-speed=\"3\" data-type=\"background\" ";
		        $backgroundattr="background-position: 0% 0%; background-repeat: repeat; background-size: cover";
		        break;
		    case'cover':
		        $parallax="";
		        $backgroundattr="background-position: center !important; background-repeat: no-repeat !important; background-size: cover!important";
		        break;
		    case'cover_all':
		        $parallax="";
		        $backgroundattr="background-position: center !important; background-repeat: repeat !important; background-size: cover!important";
		        break;
		    case'no-repeat':
		        $parallax="";
		        $backgroundattr="background-position: center !important; background-repeat: no-repeat !important;background-size:auto !important";
		        break;
		    case'repeat':
		        $parallax="";
		        $backgroundattr="background-position: 0 0 !important;background-repeat: repeat !important;background-size:auto !important";
		        break;
		    case'contain':
		        $parallax="";
		        $backgroundattr="background-position: center !important; background-repeat: no-repeat !important;background-size: contain!important";
		        break;
		    case 'fixed':
		        $parallax="";
		        $backgroundattr="background-position: center !important; background-repeat: no-repeat !important; background-size: cover!important;background-attachment: fixed !important";
		        break;
		}

		$css_background.=$backgroundattr;
	}

	if(!empty($background_color)){
		$css_background.="\n"."background-color:".esc_url($background_color)."!important;";
	}

	if(empty($css_background)) return false;

	return array('css'=>$css_background,'body'=>$parallax);
}


function grade_body_class($classes=array()){

	$classes[]='dt_custom_body';

	if(is_grade_home(get_post())){
		$classes[]="home";
	}

	return $classes;
}

add_filter( 'body_class', 'grade_body_class');


function grade_post_class($classes=array(), $class="", $post_id=0){

	if(is_single()){
		$classes[]='blog';
		$classes[]='single-post';

		if( !(function_exists('is_product') && is_product())){
			$classes[]='content';
		}
	}
	return $classes;
}

add_filter( 'post_class', 'grade_post_class');

?>