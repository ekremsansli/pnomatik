<?php
defined('ABSPATH') or die();
/**
 * @package WordPress
 * @subpackage Grade
 * @since Grade 1.0
 */

function grade_sanitize_html_class($classes=""){

  if(!empty($classes)){

    $classes_array=@explode(" ",$classes);
    $newclasses=array_filter($classes_array, "sanitize_html_class");
    return join(" ",$newclasses);
  }
  return $classes;
}

function grade_get_banner_height(){

  $banner_height=get_grade_option('dt-banner-height');
  $height=(strpos($banner_height, "px") || strpos($banner_height, "%"))?$banner_height:$banner_height."px";

  return apply_filters('detheme_banner_height',$height,$banner_height);
}

function grade_plugin_is_active( $plugin ) {
  return in_array( $plugin, (array) get_option( 'active_plugins', array() ) ) || grade_plugin_is_active_for_network( $plugin );
}

function grade_plugin_is_active_for_network( $plugin ) {
  if ( !is_multisite() )
    return false;

  $plugins = get_site_option( 'active_sitewide_plugins');
  if ( isset($plugins[$plugin]) )
    return true;

  return false;
}

function get_grade_sidebar_position(){

  if(function_exists('is_shop') && is_shop()){

   $post_id=get_option( 'woocommerce_shop_page_id');
  }
  elseif(is_home()){
    $post_id=get_option( 'page_for_posts');
  }
  elseif (is_page()){
    $post_id= get_the_ID();
  }

  $sidebar_position = isset($post_id) ?get_post_meta( $post_id, '_sidebar_position', true ):'default';

  if(!isset($sidebar_position) || empty($sidebar_position) || $sidebar_position=='default'){

    switch (get_grade_option('layout')) {
      case 1:
        $sidebar_position = "nosidebar";
        break;
      case 2:
        $sidebar_position = "sidebar-left";
        break;
      case 3:
        $sidebar_position = "sidebar-right";
        break;
      case 4:
        $sidebar_position = "fullwidth";
        break;
      default:
        $sidebar_position = "sidebar-left";
    }


  }

  return $sidebar_position;
}


add_filter('nav_menu_link_attributes','grade_formatMenuAttibute',2,2);

function grade_page_attibutes_metabox($posttypes){

  return array('page'=>$posttypes['page'],'essential_grid'=>esc_html__('Page Atribute','grade'),'post'=>esc_html__('Page Attribute','grade'),'dtpost'=>esc_html__('Page Attribute','grade'));
}

add_filter('detheme_page_metaboxes','grade_page_attibutes_metabox');

function grade_page_metaboxes_title_show($titles){

  return array('page','essential_grid','post','dtpost');
}

function grade_page_metaboxes_banner_show($titles){

  return array('page','essential_grid','post','dtpost');

}

add_filter('detheme_page_metaboxes_title','grade_page_metaboxes_title_show');
add_filter('detheme_page_metaboxes_banner','grade_page_metaboxes_banner_show');

function grade_formatMenuAttibute($atts, $item){

  global $dropdownmenu;

  if(in_array('dropdown', $item->classes)){
    $atts['class']="dropdown-toggle";
    $atts['data-toggle']="dropdown";
    $dropdownmenu=$item;
  }
  return $atts;
}

function grade_createFontelloIconMenu($css,$item,$args=array()){

  $css=@implode(" ",$css);
  $args->link_before="";
  $args->link_after="";

  if(preg_match('/([-_a-z-0-9]{0,})icon([-_a-z-0-9]{0,})/', $css, $matches)){

    $css=preg_replace('/'.$matches[0].'/', "", $css);
    $item->title="<i class=\"".$matches[0]."\"></i>";
  }
  return @explode(" ",$css);
}


function grade_createFontelloMenu($css,$item,$args=array()){

  $css=@implode(" ",$css);
  $args->link_before="";
  $args->link_after="";

  if(preg_match('/([-_a-z-0-9]{0,})icon([-_a-z-0-9]{0,})/', $css, $matches)){

    $css=preg_replace('/'.$matches[0].'/', "", $css);
    $args->link_before.="<i class=\"".$matches[0]."\"></i>";
  }

  $args->link_before.="<span>";
  $args->link_after="</span>";

  return @explode(" ",$css);
}

add_filter( 'nav_menu_css_class', 'grade_createFontelloMenu', 10, 3 );
add_filter( 'grade_nav_menu_icon_css_class', 'grade_createFontelloIconMenu', 10, 3 );


class grade_iconmenu_walker extends Walker_Nav_Menu {
  function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
    $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

    $class_names = $value = '';

    $classes = empty( $item->classes ) ? array() : (array) $item->classes;
    $classes[] = 'menu-item-' . $item->ID;

    /**
     * Filter the CSS class(es) applied to a menu item's <li>.
     *
     * @since 3.0.0
     *
     * @param array  $classes The CSS classes that are applied to the menu item's <li>.
     * @param object $item    The current menu item.
     * @param array  $args    An array of arguments. @see wp_nav_menu()
     */
    $class_names = join( ' ', apply_filters('grade_nav_menu_icon_css_class',array_filter( $classes ), $item, $args));
    $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';


    /**
     * Filter the ID applied to a menu item's <li>.
     *
     * @since 3.0.1
     *
     * @param string The ID that is applied to the menu item's <li>.
     * @param object $item The current menu item.
     * @param array $args An array of arguments. @see wp_nav_menu()
     */
    $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
    $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

    $output .= $indent . '<li' . $id . $value . $class_names .'>';

    $atts = array();
    $atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
    $atts['target'] = ! empty( $item->target )     ? $item->target     : '';
    $atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
    $atts['href']   = ! empty( $item->url )        ? $item->url        : '';

    /**
     * Filter the HTML attributes applied to a menu item's <a>.
     *
     * @since 3.6.0
     *
     * @param array $atts {
     *     The HTML attributes applied to the menu item's <a>, empty strings are ignored.
     *
     *     @type string $title  The title attribute.
     *     @type string $target The target attribute.
     *     @type string $rel    The rel attribute.
     *     @type string $href   The href attribute.
     * }
     * @param object $item The current menu item.
     * @param array  $args An array of arguments. @see wp_nav_menu()
     */
    $atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );

    $attributes = '';
    foreach ( $atts as $attr => $value ) {
      if ( ! empty( $value ) ) {
        $value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
        $attributes .= ' ' . $attr . '="' . $value . '"';
      }
    }

    $item_output = $args->before;
    $item_output .= '<a'. $attributes .'>';
    $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
    $item_output .= '</a>';
    $item_output .= $args->after;

    /**
     * Filter a menu item's starting output.
     *
     * The menu item's starting output only includes $args->before, the opening <a>,
     * the menu item's title, the closing </a>, and $args->after. Currently, there is
     * no filter for modifying the opening and closing <li> for a menu item.
     *
     * @since 3.0.0
     *
     * @param string $item_output The menu item's starting HTML output.
     * @param object $item        Menu item data object.
     * @param int    $depth       Depth of menu item. Used for padding.
     * @param array  $args        An array of arguments. @see wp_nav_menu()
     */
    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
  }
}


class grade_topbarmenuright_walker extends Walker_Nav_Menu {
  function start_lvl( &$output, $depth = 0, $args = array() ) {
      $tem_output = $output . 'akhir';

      $found = preg_match_all('/<li (.*)<span>(.*?)<\/span><\/a>akhir/s', $tem_output, $matches);

      $foundid = preg_match_all('/<li id="menu\-item\-(.*?)"/s', $tem_output, $ids);

      if ($found) {
        $menu_title = $matches[count($matches)-1][0];

        if (count($ids[1])>0) {
          $menu_id = $ids[1][count($ids[1])-1];
        } else {
          $menu_id = rand (1000,9999);
        }


        $output .= '<label for="topright'.$menu_id.'" class="toggle-sub" onclick="">&rsaquo;</label>
        <input id="topright'.$menu_id.'" class="sub-nav-check" type="checkbox">
        <ul id="topright-sub-'.$menu_id.'" class="sub-nav"><li class="sub-heading">'. $menu_title .' <label for="topright'.$menu_id.'" class="toggle" onclick="" title="'.esc_attr__('Back','grade').'">&lsaquo; '.esc_html__('Back','grade').'</label></li>';
      }
  }

}

class grade_topbarmenuleft_walker extends Walker_Nav_Menu {
  function start_lvl( &$output, $depth = 0, $args = array() ) {
      $tem_output = $output . 'akhir';

      $found = preg_match_all('/<li (.*)<span>(.*?)<\/span><\/a>akhir/s', $tem_output, $matches);

      $foundid = preg_match_all('/<li id="menu\-item\-(.*?)"/s', $tem_output, $ids);

      if ($found) {
        $menu_title = $matches[count($matches)-1][0];

        if (count($ids[1])>0) {
          $menu_id = $ids[1][count($ids[1])-1];
        } else {
          $menu_id = rand (1000,9999);
        }


        $output .= '<label for="topleft'.$menu_id.'" class="toggle-sub" onclick="">&rsaquo;</label>
        <input id="topleft'.$menu_id.'" class="sub-nav-check" type="checkbox">
        <ul id="topleft-sub-'.$menu_id.'" class="sub-nav"><li class="sub-heading">'. $menu_title .' <label for="topleft'.$menu_id.'" class="toggle" onclick="" title="'.esc_attr__('Back','grade').'">&lsaquo; '.esc_html__('Back','grade').'</label></li>';
      }
  }

}

function grade_add_class_to_first_submenu($items) {
  $menuhaschild = array();

  foreach($items as $key => $item) {

    if (in_array('menu-item-has-children',$item->classes)) {
      $menuhaschild[] = $item->ID;
    }

  }

  foreach($menuhaschild as $key => $parent_id) {
    foreach($items as $key => $item) {
      if ($item->menu_item_parent==$parent_id) {
        $item->classes[] = 'menu-item-first-child';
        break;
      }
    }
  }


  return $items;
}


function grade_nav_menu_item_id( $menuitem_id, $item, $args){

  if(($args= (object) $args) &&  isset($args->nav_menu_item_id) && !empty($args->nav_menu_item_id)){

    $menuitem_id=trim($args->nav_menu_item_id).$item->ID;
  }

  return $menuitem_id;
}


add_filter( 'nav_menu_item_id', 'grade_nav_menu_item_id',11,3);
add_filter('wp_nav_menu_objects', 'grade_add_class_to_first_submenu');

function grade_tag_cloud_args($args=array()){
  $args['filter']=1;
  return $args;

}

function grade_tag_cloud($return="",$tags, $args = '' ){

  if(!count($tags))
    return $return;
  $return='<ul class="list-unstyled">';
  foreach ($tags as $tag) {
    $return.='<li class="tag"><a href="'.esc_url($tag->link).'">'.ucwords($tag->name).'</a></li>';
  }
  $return.='</ul>';
  return $return;
}

function grade_widget_title($title="",$instance=array(),$id=null){

  if(empty($instance['title']))
      return "";
  return $title;
}

add_filter('widget_tag_cloud_args','grade_tag_cloud_args');
add_filter('wp_generate_tag_cloud','grade_tag_cloud',1,3);
add_filter('widget_title','grade_widget_title',1,3);

function grade_get_avatar_url($get_avatar){
    preg_match("/src='(.*?)'/i", $get_avatar, $matches);
    if (isset($matches[1])) {
      return $matches[1];
    } else {
      return;
    }
}


// Comment Functions
function grade_comment_form( $args = array(), $post_id = null ) {
  if ( null === $post_id )
    $post_id = get_the_ID();
  else
    $id = $post_id;

  $commenter = wp_get_current_commenter();
  $user = wp_get_current_user();
  $user_identity = $user->exists() ? $user->display_name : '';

  $args = wp_parse_args( $args );
  if ( ! isset( $args['format'] ) )
    $args['format'] = current_theme_supports( 'html5', 'comment-form' ) ? 'html5' : 'xhtml';

  $req      = get_option( 'require_name_email' );
  $aria_req = ( $req ? " aria-required='true'" : '' );
  $html5    = 'html5' === $args['format'];

  $fields   =  array(
    'author' => '<div class="row">
                    <div class="form-group col-sm-12">
                      <input type="text" class="form-control" name="author" id="author" placeholder="'.esc_attr__('full name','grade').'" required>
                  </div>',
    'email' => '<div class="form-group col-xs-12 col-sm-6">
                      <input type="email" class="form-control"  name="email" id="email" placeholder="'.esc_attr__('email address','grade').'" required>
                  </div>',
    'url' => '<div class="form-group col-xs-12 col-sm-6">
                  <input type="text" class="form-control icon-user-7" name="url" id="url" placeholder="'.esc_attr__('website','grade').'">
                </div>
              </div>',
  );

  $required_text = sprintf( ' ' . esc_html__('Required fields are marked %s','grade'), '<span class="required">*</span>' );
  $defaults = array(
    'fields'               => apply_filters( 'comment_form_default_fields', $fields ),
    'comment_field'        => '<div class="row">
                                  <div class="form-group col-xs-12">
                                    <textarea class="form-control" rows="3" name="comment" id="comment" placeholder="'.esc_html__('your message','grade').'" required></textarea>
                                  </div>
                              </div>',
    'must_log_in'          => '<p class="must-log-in">' . sprintf( wp_kses(__( 'You must be <a href="%s">logged in</a> to post a comment.','grade'),array('a'=>array('href'=>array()))), wp_login_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>',
    'logged_in_as'         => '<p class="logged-in-as">' . sprintf( wp_kses(__( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>','grade'),array('a'=>array('href'=>array(),'title'=>array()))), get_edit_user_link(), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>',
    'comment_notes_before' => '<p class="comment-notes">' . esc_html__( 'Your email address will not be published.','grade') . ( $req ? $required_text : '' ) . '</p>',
    'comment_notes_after'  => '',
    'id_form'              => 'commentform',
    'id_submit'            => 'submit',
    'title_reply'          => esc_html__('Leave a Comment','grade'),
    'title_reply_to'       => esc_html__( 'Leave a Comment to %s','grade'),
    'cancel_reply_link'    => esc_html__( 'Cancel reply','grade'),
    'label_submit'         => esc_html__( 'Submit','grade' ),
    'format'               => 'html5',
  );

  $args = wp_parse_args( $args, apply_filters( 'comment_form_defaults', $defaults ) );

  ?>
    <?php if ( comments_open( $post_id ) ) : ?>
      <?php do_action( 'comment_form_before' ); ?>
      <section id="respond" class="comment-respond">
        <h3 id="reply-title" class="comment-reply-title"><?php comment_form_title( $args['title_reply'], $args['title_reply_to'] ); ?> <small><?php cancel_comment_reply_link( $args['cancel_reply_link'] ); ?></small></h3>
        <?php if ( get_option( 'comment_registration' ) && !is_user_logged_in() ) : ?>
          <?php echo $args['must_log_in']; ?>
          <?php do_action( 'comment_form_must_log_in_after' ); ?>
        <?php else : ?>
          <form action="<?php echo site_url( '/wp-comments-post.php' ); ?>" method="post" id="<?php echo esc_attr( $args['id_form'] ); ?>" class="comment-form"<?php echo ($html5) ? ' novalidate' : ''; ?> data-abide>
            <?php do_action( 'comment_form_top' ); ?>
            <?php
              if ( is_user_logged_in() ) :
                echo apply_filters( 'comment_form_logged_in', $args['logged_in_as'], $commenter, $user_identity );
                do_action( 'comment_form_logged_in_after', $commenter, $user_identity );
                echo $args['comment_notes_before'];
              else :
                do_action( 'comment_form_before_fields' );
                foreach ( (array) $args['fields'] as $name => $field ) {
                  echo apply_filters( "comment_form_field_{$name}", $field ) . "\n";
                }
                do_action( 'comment_form_after_fields' );
              endif;
            ?>
            <?php echo apply_filters( 'comment_form_field_comment', $args['comment_field'] ); ?>
            <?php echo $args['comment_notes_after']; ?>
            <p class="form-submit">
              <input name="submit" type="submit" id="<?php echo esc_attr( $args['id_submit'] ); ?>" value="<?php echo esc_attr( $args['label_submit'] ); ?>" class="btn btn-primary" />
              <?php comment_id_fields( $post_id ); ?>
            </p>
            <?php do_action( 'comment_form', $post_id ); ?>
          </form>
        <?php endif; ?>
      </section><!-- #respond -->
      <?php do_action( 'comment_form_after' ); ?>
    <?php else : ?>
      <?php do_action( 'comment_form_comments_closed' ); ?>
    <?php endif; ?>
  <?php
}

/**
 * Retrieve HTML content for reply to comment link.
 *
 * The default arguments that can be override are 'add_below', 'respond_id',
 * 'reply_text', 'login_text', and 'depth'. The 'login_text' argument will be
 * used, if the user must log in or register first before posting a comment. The
 * 'reply_text' will be used, if they can post a reply. The 'add_below' and
 * 'respond_id' arguments are for the JavaScript moveAddCommentForm() function
 * parameters.
 *
 * @since 2.7.0
 *
 * @param array $args Optional. Override default options.
 * @param int $comment Optional. Comment being replied to.
 * @param int $post Optional. Post that the comment is going to be displayed on.
 * @return string|bool|null Link to show comment form, if successful. False, if comments are closed.
 */
function grade_get_comment_reply_link($args = array(), $comment = null, $post = null) {
  global $user_ID;

  $defaults = array('add_below' => 'comment', 'respond_id' => 'respond', 'reply_text' => esc_html__('Reply','grade'),
    'login_text' => esc_html__('Log in to Reply','grade'), 'depth' => 0, 'before' => '', 'after' => '');

  $args = wp_parse_args($args, $defaults);

  if ( 0 == $args['depth'] || $args['max_depth'] <= $args['depth'] )
    return;

  extract($args, EXTR_SKIP);

  $comment = get_comment($comment);
  if ( empty($post) )
    $post = $comment->comment_post_ID;
  $post = get_post($post);

  if ( !comments_open($post->ID) )
    return false;

  $link = '';

  if ( get_option('comment_registration') && !$user_ID )
    $link = '<a rel="nofollow" class="comment-reply-login" href="' . esc_url( wp_login_url( get_permalink() ) ) . '">' . $login_text . '</a>';
  else
    $link = "<a class='reply comment-reply-link btn btn-ghost skin-dark' href='#' onclick='return addComment.moveForm(\"$add_below-$comment->comment_ID\", \"$comment->comment_ID\", \"$respond_id\", \"$post->ID\")'>$reply_text</a>";

  return apply_filters('comment_reply_link', $before . $link . $after, $args, $comment, $post);
}

/**
 * Displays the HTML content for reply to comment link.
 *
 * @since 2.7.0
 * @see grade_get_comment_reply_link() Echoes result
 *
 * @param array $args Optional. Override default options.
 * @param int $comment Optional. Comment being replied to.
 * @param int $post Optional. Post that the comment is going to be displayed on.
 * @return string|bool|null Link to show comment form, if successful. False, if comments are closed.
 */
function grade_comment_reply_link($args = array(), $comment = null, $post = null) {
  echo grade_get_comment_reply_link($args, $comment, $post);
}

if ( ! function_exists( 'grade_edit_comment_link' ) ) :
  function grade_edit_comment_link( $link = null, $before = '', $after = '' ) {
    global $comment;

    if ( !current_user_can( 'edit_comment', $comment->comment_ID ) )
      return;

    if ( null === $link )
      $link = esc_html__('Edit This','grade');

    $link = '<a class="comment-edit-link primary_color_button btn btn-ghost skin-dark" href="' . esc_url(get_edit_comment_link( $comment->comment_ID )) . '">' . $link . '</a>';
    echo $before . apply_filters( 'edit_comment_link', $link, $comment->comment_ID ) . $after;
  }
endif;

if( ! function_exists( 'grade_comment_end_callback' )){

  function grade_comment_end_callback( $comment, $args, $depth){
    ?>
</li>
<?php
  }

}
if ( ! function_exists( 'grade_comment' ) ) :
function grade_comment( $comment, $args, $depth ) {

  $GLOBALS['comment'] = $comment;
  switch ( $comment->comment_type ) :
    case 'pingback' :
    case 'trackback' :
      // Display trackbacks differently than normal comments.
      ?>
      <li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
      <p><?php esc_html_e( 'Pingback:', 'grade' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( esc_html__( '(Edit)', 'grade' ), '<span class="edit-link">', '</span>' ); ?></p>
      </li>
      <?php
    break;

    default :
      // Proceed with normal comments.

      ?>
              <li class="comment_item media" id="comment-<?php print $comment->comment_ID; ?>">
                <div class="pull-<?php print is_rtl()?"right":"left";?> text-center">
                  <?php $avatar_url = grade_get_avatar_url(get_avatar( $comment, 100 )); ?>
                  <a href="<?php echo esc_url(comment_author_url()); ?>"><img src="<?php echo esc_url($avatar_url); ?>" class="author-avatar img-responsive img-circle" alt="<?php comment_author(); ?>"></a>
                </div>
                <div class="media-body">
                  <div class="col-xs-12 col-sm-5<?php print is_rtl()?" col-sm-push-7":"";?> dt-comment-author"><?php comment_author(); ?></div>
                  <div class="col-xs-12 col-sm-7<?php print is_rtl()?" col-sm-pull-5":"";?> dt-comment-date secondary_color_text text-<?php print is_rtl()?"left":"right";?>"><?php comment_date('d.m.Y') ?></div>
                  <div class="col-xs-12 dt-comment-comment"><?php comment_text(); ?></div>
                  <div class="col-xs-12 text-<?php print is_rtl()?"left":"right";?> dt-comment-buttons">
                      <?php grade_comment_reply_link( array_merge( $args, array( 'reply_text' => esc_html__( 'Reply', 'grade' ), 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
                      <?php grade_edit_comment_link( esc_html__( 'Edit', 'grade' ), '', '' ); ?>
                  </div>
                </div>
      <?php
    break;
  endswitch; // end comment_type check
}
endif;

// function to display number of posts.
function grade_get_post_views($postID){

    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return sprintf(esc_html__("%d View",'grade'),0);
    } elseif ($count<=1) {
        return sprintf(esc_html__("%d View",'grade'),$count);
    }


    $output = str_replace('%', number_format_i18n($count),esc_html__('% Views','grade'));
    return $output;
}

// function to count views.
function grade_set_post_views($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

function grade_post_view_column(){

  $post_types = get_post_types( array(),'names' );

      foreach ( $post_types as $post_type ) {
        if ( in_array($post_type,array('page','attachment','wpcf7_contact_form','vc_grid_item','nav_menu_item','revision')))
            continue;

          add_filter('manage_'.$post_type.'_posts_columns', 'grade_posts_column_views');
          add_action('manage_'.$post_type.'_posts_custom_column', 'grade_posts_custom_column_views',5,2);
    }
}

add_action('admin_init','grade_post_view_column');

function grade_posts_column_views($defaults){
    $defaults['post_views'] = esc_html__('Views','grade');
    return $defaults;
}

function grade_posts_custom_column_views($column_name, $id){

  if($column_name === 'post_views'){
        echo grade_get_post_views(get_the_ID());
    }
}

if(!function_exists('grade_is_ssl_mode')){
function grade_is_ssl_mode(){
  $ssl=strpos("a".site_url(),'https://');

  return (bool)$ssl;
}}

function grade_maybe_ssl_url($url=""){
  return grade_is_ssl_mode()?str_replace('http://', 'https://', $url):$url;
}

if (!function_exists('grade_aq_resize')) {
  function grade_aq_resize( $url, $width, $height = null, $crop = null, $single = true ) {

    if(!$url OR !($width || $height)) return false;

    //define upload path & dir
    $upload_info = wp_upload_dir();
    $upload_dir = $upload_info['basedir'];
    $upload_url = $upload_info['baseurl'];

    //check if $img_url is local
    /* Gray this out because WPML doesn't like it.
    if(strpos( $url, home_url('/') ) === false) return false;
    */

    //define path of image
    $rel_path = str_replace( str_replace( array( 'http://', 'https://' ),"",$upload_url), '', str_replace( array( 'http://', 'https://' ),"",$url));
    $img_path = $upload_dir . $rel_path;

    //check if img path exists, and is an image indeed
    if( !file_exists($img_path) OR !getimagesize($img_path) ) return false;

    //get image info
    $info = pathinfo($img_path);
    $ext = $info['extension'];
    list($orig_w,$orig_h) = getimagesize($img_path);

    $dims = image_resize_dimensions($orig_w, $orig_h, $width, $height, $crop);
    if(!$dims){
      return $single?$url:array('0'=>$url,'1'=>$orig_w,'2'=>$orig_h);
    }

    $dst_w = $dims[4];
    $dst_h = $dims[5];

    //use this to check if cropped image already exists, so we can return that instead
    $suffix = "{$dst_w}x{$dst_h}";
    $dst_rel_path = str_replace( '.'.$ext, '', $rel_path);
    $destfilename = "{$upload_dir}{$dst_rel_path}-{$suffix}.{$ext}";

    //if orig size is smaller
    if($width >= $orig_w) {

      if(!$dst_h) :
        //can't resize, so return original url
        $img_url = $url;
        $dst_w = $orig_w;
        $dst_h = $orig_h;

      else :
        //else check if cache exists
        if(file_exists($destfilename) && getimagesize($destfilename)) {
          $img_url = "{$upload_url}{$dst_rel_path}-{$suffix}.{$ext}";
        }
        else {

          $imageEditor=wp_get_image_editor( $img_path );

          if(!is_wp_error($imageEditor)){

              $imageEditor->resize($width, $height, $crop );
              $imageEditor->save($destfilename);

              $resized_rel_path = str_replace( $upload_dir, '', $destfilename);
              $img_url = $upload_url . $resized_rel_path;


          }
          else{
              $img_url = $url;
              $dst_w = $orig_w;
              $dst_h = $orig_h;
          }

        }

      endif;

    }
    //else check if cache exists
    elseif(file_exists($destfilename) && getimagesize($destfilename)) {
      $img_url = "{$upload_url}{$dst_rel_path}-{$suffix}.{$ext}";
    }
    else {

      $imageEditor=wp_get_image_editor( $img_path );

      if(!is_wp_error($imageEditor)){
          $imageEditor->resize($width, $height, $crop );
          $imageEditor->save($destfilename);

          $resized_rel_path = str_replace( $upload_dir, '', $destfilename);
          $img_url = $upload_url . $resized_rel_path;
      }
      else{
          $img_url = $url;
          $dst_w = $orig_w;
          $dst_h = $orig_h;
      }


    }

    if(!$single) {
      $image = array (
        '0' => $img_url,
        '1' => $dst_w,
        '2' => $dst_h
      );

    } else {
      $image = $img_url;
    }

    return $image;
  }
}


function grade_responsiveVideo($html, $url,$attr=array(),$post_ID=0) {

  $html=grade_add_video_wmode_transparent($html);

  if (!is_admin() && !preg_match("/flex\-video/mi", $html) ) {
    $html = str_replace('frameborder="0"',' ',$html);
    $html="<div class=\"flex-video widescreen\">".$html."</div>";
  }
  return $html;
}

add_filter('embed_handler_html', 'grade_responsiveVideo', 92, 3 );
add_filter('oembed_dataparse', 'grade_responsiveVideo', 90, 3 );
add_filter('embed_oembed_html', 'grade_responsiveVideo', 91, 4 );

function grade_add_video_wmode_transparent($html) {
   if (strpos($html, "<iframe " ) !== false) {
      $search = array('?feature=oembed');
      $replace = array('?feature=oembed&amp;wmode=transparent&amp;rel=0&amp;autohide=1&amp;showinfo=0');
      $html = str_replace($search, $replace, $html);
      $html = preg_replace('/frameborder/si','style="border:none;" data-border',$html);
      return $html;
   } else {
      return $html;
   }
}

function grade_makeBottomWidgetColumn($params){

  if('detheme-bottom'==$params[0]['id']){

    $class="col-sm-4";

    if($col=(int)get_grade_option('dt-footer-widget-column')){

      switch($col){

          case 2:
                $class='col-md-6 col-sm-6 col-xs-6';
            break;
          case 3:
                $class='col-md-4 col-sm-6 col-xs-6';
            break;
          case 4:
                $class='col-lg-3 col-md-4 col-sm-6 col-xs-6';
            break;
          case 1:
          default:
                $class='col-sm-12';
            break;
      }
    }


    $makerow="";

    $params[0]['before_widget']='<div class="border-left '.$class.' col-'.$col.'">'.$params[0]['before_widget'];
    $params[0]['after_widget']=$params[0]['after_widget'].'</div>'.$makerow;

 }

  return $params;

}

function grade_protected_meta($protected, $meta_key, $meta_type){

 $protected=(in_array($meta_key,
    array('vc_teaser','slide_template','pagebuilder','masonrycolumn','portfoliocolumn','portfoliotype','post_views_count','show_comment','show_social','sidebar_position','subtitle')
  ))?true:$protected;

  return $protected;
}

add_filter('is_protected_meta','grade_protected_meta',1,3);
add_filter( 'dynamic_sidebar_params', 'grade_makeBottomWidgetColumn' );

function grade_fill_width_dummy_widget (){

   $col=1;
   if(get_grade_option('dt-footer-widget-column')) {
      $col=(int)get_grade_option('dt-footer-widget-column');
   }


   $sidebar = wp_get_sidebars_widgets();


   $itemCount=(isset($sidebar['detheme-bottom']))?count($sidebar['detheme-bottom']):0;

   switch($col){

          case 2:
                $class='col-md-6 col-sm-6 col-xs-6';
            break;
          case 3:
                $class='col-md-4 col-sm-6 col-xs-6';
            break;
          case 4:
                $class='col-lg-3 col-md-4 col-sm-6 col-xs-6';
            break;
          case 1:
          default:
                $class='col-sm-12';
            break;
  }


  if($itemCount % $col){
   print str_repeat("<div class=\"border-left dummy ".$class."\"></div>",$col - ($itemCount % $col));
 }
}

add_action('dynamic_sidebar_detheme-bottom','grade_fill_width_dummy_widget');

function grade_remove_shortcode_from_content($content) {
  // remove shortcodes
  $content = strip_shortcodes( $content );

  // remove images
  $content = preg_replace('/<img[^>]+./','', $content);

  return $content;
}

function grade_get_first_image_url_from_content() {
  global $post, $posts;
  $first_img = '';
  ob_start();
  ob_end_clean();
  if (isset($post->post_content)) {
    $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
    if (isset($matches[1][0])) {
      $first_img = $matches[1][0];
    }
  }

  return $first_img;
}

/* vc_set_as_theme */

if(function_exists('vc_set_as_theme'))
{


    function grade_vc_add_element_param( $name, $form_field_callback, $script_url = null ) {
      return WpbakeryShortcodeParams::addField( $name, $form_field_callback, $script_url );
    }

    function grade_add_element_param( $name, $form_field_callback, $script_url = null ) {
      return grade_vc_add_element_param( $name, $form_field_callback, $script_url );
    }


  if(version_compare(WPB_VC_VERSION,'4.9.0','<')):

        function grade_vc_settings_general_callback(){


        $pt_array = ( $pt_array = get_option( 'wpb_js_content_types' ) ) ? ( $pt_array ) : vc_default_editor_post_types();

        $excludePostype=apply_filters( 'vc_settings_exclude_post_type',array( 'attachment', 'revision', 'nav_menu_item', 'mediapage' ));

        foreach ( get_post_types( array( 'public' => true )) as $pt) {
          if ( ! in_array( $pt, $excludePostype ) ) {
             $post_type_object=get_post_type_object($pt);
             $label = $post_type_object->labels->singular_name;
            ?>
            <label>
              <input type="checkbox"<?php echo ( in_array( $pt, $pt_array ) ) ? ' checked="checked"' : ''; ?> value="<?php echo esc_attr($pt); ?>"
                     id="wpb_js_post_types_<?php echo sanitize_title($pt); ?>"
                     name="wpb_js_content_types[]">
              <?php echo ucfirst(esc_html( $label )); ?>
            </label><br>
          <?php
          }
        }
        ?>
        <p
          class="description indicator-hint"><?php esc_html_e( "Select for which content types Visual Composer should be available during post creation/editing.", "js_composer" ); ?></p>
      <?php
          }

        function grade_vc_settings_general(){
            add_settings_field('wpb_js_content_types',esc_html__( "Content types", "js_composer" ),'grade_vc_settings_general_callback','vc_settings_general','wpb_js_composer_settings_general');
        }

        add_action('admin_init','grade_vc_settings_general',9999);

  endif;

  add_action('init','grade_basic_grid_params');

  function grade_basic_grid_params(){

      $post_types = get_post_types( array(),'names' );

      $post_types_list = array();
      foreach ( $post_types as $post_type ) {
          if ( $post_type !== 'revision' && $post_type !== 'nav_menu_item' ) {

              $post_type_object=get_post_type_object($post_type);

              $label = $post_type_object->labels->singular_name;

              $post_types_list[] = array( $post_type, ucfirst(esc_html( $label )) );
          }
      }

      $post_types_list[] = array( 'custom', esc_html__( 'Custom query', 'js_composer' ) );
      $post_types_list[] = array( 'ids', esc_html__( 'List of IDs', 'js_composer' ) );

      vc_add_param( 'vc_basic_grid', array(
              'type' => 'dropdown',
              'heading' => esc_html__( 'Data source', 'js_composer' ),
              'param_name' => 'post_type',
              'value' => $post_types_list,
              'description' => esc_html__( 'Select content type for your grid.', 'js_composer' )
      ));

  }

  if(grade_plugin_is_active('grade_vc_addon/grade_vc_addon.php')){

      function grade_vc_btn(){

             vc_add_param( 'vc_btn', array(
                    'heading' => esc_html__( 'Color', 'js_composer' ),
                    'param_name' => 'color',
                    "type" => "dropdown",
                    'description' => esc_html__( 'Select button color.', 'js_composer' ),
                    // compatible with btn2, need to be converted from btn1
                    'param_holder_class' => 'vc_colored-dropdown vc_btn3-colored-dropdown',
                    'value' => array(
                        // Btn1 Colors
                        esc_html__( 'Brand Primary', 'js_composer' ) => 'brand-primary',
                        esc_html__( 'Classic Grey', 'js_composer' ) => 'default',
                        esc_html__( 'Classic Blue', 'js_composer' ) => 'primary',
                        esc_html__( 'Classic Turquoise', 'js_composer' ) => 'info',
                        esc_html__( 'Classic Green', 'js_composer' ) => 'success',
                        esc_html__( 'Classic Orange', 'js_composer' ) => 'warning',
                        esc_html__( 'Classic Red', 'js_composer' ) => 'danger',
                        esc_html__( 'Classic Black', 'js_composer' ) => 'inverse',
                        // + Btn2 Colors (default color set)
                      ) + getVcShared( 'colors-dashed' ),
                    "std" => 'grey',
                    'dependency' => array(
                      'element' => 'style',
                      'value_not_equal_to' => array(
                        'custom',
                        'outline-custom',
                        'gradient',
                        'gradient-custom',
                      ),
                    ),
                    )
            );


      }

      add_action('init','grade_vc_btn');

      add_action('init','grade_vc_cta_2');

      function grade_vc_cta_2(){

           vc_remove_param('vc_cta_button2','color');
            vc_add_param( 'vc_cta_button2', array(
                    "type" => "dropdown",
                    "heading" => esc_html__("Button style", 'grade'),
                    "param_name" => "btn_style",
                    "value" => array(
                      esc_html__('Primary','grade')=>'color-primary',
                      esc_html__('Secondary','grade')=>'color-secondary',
                      esc_html__('Success','grade')=>'success',
                      esc_html__('Info','grade')=>'info',
                      esc_html__('Warning','grade')=>'warning',
                      esc_html__('Danger','grade')=>'danger',
                      esc_html__('Ghost Button','grade')=>'ghost',
                      esc_html__('Link','grade')=>'link',
                      ),
                    "std" => 'default',
                    "description" => esc_html__("Button style", 'grade')."."
                    )
            );

         vc_add_param( 'vc_cta_button2',
            array(
              "type" => "dropdown",
              "heading" => esc_html__("Size", 'grade'),
              "param_name" => "size",
                  "value" => array(
                    esc_html__('Large','grade')=>'btn-lg',
                    esc_html__('Default','grade')=>'btn-default',
                    esc_html__('Small','grade')=>'btn-sm',
                    esc_html__('Extra small','grade')=>'btn-xs'
                    ),
              "std" => 'btn-default',
              "description" => esc_html__("Button size", 'grade')
            ));
      }

      function grade_remove_meta_box_vc(){
        remove_meta_box( 'vc_teaser','page','side');
      }

      add_action('admin_init','grade_remove_meta_box_vc');

    add_action('init','grade_custom_vc_row');

    function grade_custom_vc_row(){

     vc_add_param( 'vc_row', array(
          'heading' => esc_html__( 'Expand section width', 'grade' ),
          'param_name' => 'expanded',
          'class' => '',
          'value' => array(esc_html__('Expand Column','grade')=>'1',esc_html__('Expand Background','grade')=>'2'),
          'description' => esc_html__( 'Make section "out of the box".', 'grade' ),
          'type' => 'checkbox',
          'group'=>esc_html__('Extended options', 'grade')
      ) );


     vc_add_param( 'vc_row',   array(
            'heading' => esc_html__( 'Background Type', 'grade' ),
            'param_name' => 'background_type',
            'value' => array('image'=>esc_html__( 'Image', 'grade' ),'video'=>esc_html__( 'Video', 'grade' )),
            'type' => 'radio',
            'group'=>esc_html__('Extended options', 'grade'),
            'std'=>'image'
         ));

     if(version_compare(WPB_VC_VERSION,'4.7.0','>=')){

          vc_remove_param('vc_row','full_width');
          vc_remove_param('vc_row','video_bg');
          vc_remove_param('vc_row','video_bg_url');
          vc_remove_param('vc_row','video_bg_parallax');
          vc_remove_param('vc_row','parallax');
          vc_remove_param('vc_row','parallax_image');

          if(version_compare(WPB_VC_VERSION,'4.11.0','>=') || version_compare(WPB_VC_VERSION,'4.11','>=')){

              vc_remove_param('vc_row','parallax_speed_video');
              vc_remove_param('vc_row','parallax_speed_bg');
          }

          vc_add_param( 'vc_row',   array(
                  'heading' => esc_html__( 'Video Source', 'grade' ),
                  'param_name' => 'video_source',
                  'value' => array('local'=>esc_html__( 'Local Server', 'grade' ),'youtube'=>esc_html__( 'Youtube/Vimeo', 'grade' )),
                  'type' => 'radio',
                  'group'=>esc_html__('Extended options', 'grade'),
                  'std'=>'local',
                  'dependency' => array( 'element' => 'background_type', 'value' => array('video') )
           ));


         vc_add_param( 'vc_row', array(
              'heading' => esc_html__( 'Background Video (mp4)', 'grade' ),
              'param_name' => 'background_video',
              'type' => 'attach_video',
              'group'=>esc_html__('Extended options', 'grade'),
              'dependency' => array( 'element' => 'video_source', 'value' => array('local') )
          ) );

         vc_add_param( 'vc_row', array(
              'heading' => esc_html__( 'Background Video (webm)', 'grade' ),
              'param_name' => 'background_video_webm',
              'type' => 'attach_video',
              'group'=>esc_html__('Extended options', 'grade'),
              'dependency' => array( 'element' => 'video_source', 'value' => array('local') )
          ) );

         vc_add_param( 'vc_row', array(
              'heading' => esc_html__( 'Background Image', 'grade' ),
              'param_name' => 'background_image',
              'type' => 'attach_image',
              'group'=>esc_html__('Extended options', 'grade'),
              'dependency' => array( 'element' => 'background_type', 'value' => array('image') )
          ) );

          vc_add_param( 'vc_row',
              array(
                'type' => 'textfield',
                'heading' => esc_html__( 'Video link', 'grade' ),
                'param_name' => 'video_bg_url',
                'group'=>esc_html__('Extended options', 'grade'),
                'description' => esc_html__( 'Add YouTube/Vimeo link', 'grade' ),
                'dependency' => array(
                  'element' => 'video_source',
                  'value' => array('youtube'),
                ),
           ));
      }
      else{

         vc_add_param( 'vc_row', array(
              'heading' => esc_html__( 'Background Video (mp4)', 'grade' ),
              'param_name' => 'background_video',
              'type' => 'attach_video',
              'group'=>esc_html__('Extended options', 'grade'),
              'dependency' => array( 'element' => 'background_type', 'value' => array('video') )
          ) );

         vc_add_param( 'vc_row', array(
              'heading' => esc_html__( 'Background Video (webm)', 'grade' ),
              'param_name' => 'background_video_webm',
              'type' => 'attach_video',
              'group'=>esc_html__('Extended options', 'grade'),
              'dependency' => array( 'element' => 'background_type', 'value' => array('video') )
          ) );

         vc_add_param( 'vc_row', array(
              'heading' => esc_html__( 'Background Image', 'grade' ),
              'param_name' => 'background_image',
              'type' => 'attach_image',
              'group'=>esc_html__('Extended options', 'grade'),
              'dependency' => array( 'element' => 'background_type', 'value' => array('image') )
          ) );


      }

     vc_add_param( 'vc_row', array(
          'heading' => esc_html__( 'Extra id', 'grade' ),
          'param_name' => 'el_id',
          'type' => 'textfield',
          "description" => esc_html__("If you wish to add anchor id to this row. Anchor id may used as link like href=\"#yourid\"", 'grade'),
      ) );


     vc_add_param( 'vc_row_inner', array(
          'heading' => esc_html__( 'Extra id', 'grade' ),
          'param_name' => 'el_id',
          'type' => 'textfield',
          "description" => esc_html__("If you wish to add anchor id to this row. Anchor id may used as link like href=\"#yourid\"", 'grade'),
      ) );

      vc_add_param( 'vc_row', array(
          'heading' => esc_html__( 'Background Style', 'grade' ),
          'param_name' => 'background_style',
          'type' => 'dropdown',
          'value'=>array(
                esc_html__('No Repeat', 'grade') => 'no-repeat',
                esc_html__("Cover", 'grade') => 'cover',
                esc_html__('Contain', 'grade') => 'contain',
                esc_html__('Repeat', 'grade') => 'repeat',
                esc_html__("Parallax", 'grade') => 'parallax',
               esc_html__("Fixed", 'grade') => 'fixed',
              ),
          'group'=>esc_html__('Extended options', 'grade'),
          'dependency' => array( 'element' => 'background_type', 'value' => array('image') )
      ) );
    }


  }

 add_action('init','grade_vc_single_image');

  function grade_vc_single_image(){

      vc_add_param( 'vc_single_image', array(
          'heading' => esc_html__( 'Image Hover Option', 'grade' ),
          'param_name' => 'image_hover',
          'type' => 'radio',
          'value'=>array(
                'none'=>esc_html__("None", 'grade'),
                'image'=>esc_html__("Image", 'grade'),
                'text'=>esc_html__("Text", 'grade'),
              ),
          'group'=>esc_html__('Extended options', 'grade'),
          'std' => 'none',
          'dependency' => array(
            'element' => 'source',
            'value' => array( 'media_library', 'featured_image' )
          ),

      ) );

      vc_add_param( 'vc_single_image', array(
          'heading' => esc_html__( 'Image', 'grade' ),
          'param_name' => 'image_hover_src',
          'type' => 'attach_image',
          'value'=>"",
          'holder'=>'div',
          'param_holder_class'=>'image-hover',
          'group'=>esc_html__('Extended options', 'grade'),
          'dependency' => array( 'element' => 'image_hover','value'=>array('image'))
      ) );

      vc_add_param( 'vc_single_image', array(
          'heading' => esc_html__( 'Animation Style', 'grade' ),
          'param_name' => 'image_hover_type',
          'type' => 'dropdown',
          'value'=>array(
              esc_html__('Default','grade')=>'default',
              esc_html__('From Left','grade')=>'fromleft',
              esc_html__('From Right','grade')=>'fromright',
              esc_html__('From Top','grade')=>'fromtop',
              esc_html__('From Bottom','grade')=>'frombottom',
            ),
          'group'=>esc_html__('Extended options', 'grade'),
          'dependency' => array( 'element' => 'image_hover','value'=>array('image'))
      ) );

        vc_add_param( 'vc_single_image', array(
            'heading' => esc_html__("Image style", "js_composer"),
            'param_name' => 'style',
            'type' => 'dropdown',
            'value'=>array(
                        esc_html__("Default",'grade') => "",
                        esc_html__('Rounded','grade') => 'vc_box_rounded',
                        esc_html__('Border','grade') => 'vc_box_border',
                        esc_html__('Outline','grade') => 'vc_box_outline',
                        esc_html__('Shadow','grade') => 'vc_box_shadow',
                        esc_html__('Bordered shadow','grade') => 'vc_box_shadow_border',
                        esc_html__('3D Shadow','grade') => 'vc_box_shadow_3d',
                        esc_html__('Round','grade') => 'vc_box_circle', //new
                        esc_html__('Round Border','grade') => 'vc_box_border_circle', //new
                        esc_html__('Round Outline','grade') => 'vc_box_outline_circle', //new
                        esc_html__('Round Shadow','grade') => 'vc_box_shadow_circle', //new
                        esc_html__('Round Border Shadow','grade') => 'vc_box_shadow_border_circle', //new
                        esc_html__('Circle','grade') => 'vc_box_circle_2', //new
                        esc_html__('Circle Border','grade') => 'vc_box_border_circle_2', //new
                        esc_html__('Circle Outline','grade') => 'vc_box_outline_circle_2', //new
                        esc_html__('Circle Shadow','grade') => 'vc_box_shadow_circle_2', //new
                        esc_html__('Circle Border Shadow','grade') => 'vc_box_shadow_border_circle_2', //new
                        esc_html__("Diamond",'grade') => "dt_vc_box_diamond" //new from detheme
                    ),
          'dependency' => array(
            'element' => 'source',
            'value' => array( 'media_library', 'featured_image' )
          ),

        ) );



      vc_add_param( 'vc_single_image', array(
          'heading' => esc_html__( 'Pre Title', 'grade' ),
          'param_name' => 'image_hover_pre_text',
          'type' => 'textfield',
          'value'=>"",
          'group'=>esc_html__('Extended options', 'grade'),
          'dependency' => array( 'element' => 'image_hover','value'=>array('text'))
      ) );
      vc_add_param( 'vc_single_image', array(
          'heading' => esc_html__( 'Title', 'grade' ),
          'param_name' => 'image_hover_text',
          'type' => 'textfield',
          'value'=>"",
          'group'=>esc_html__('Extended options', 'grade'),
          'dependency' => array( 'element' => 'image_hover','value'=>array('text'))
      ) );
  }


  /* spectech plugin */

  if(grade_plugin_is_active('detheme-spectech/spectech.php')){

    vc_map( array(
    'name' => esc_html__( 'Spectech Measurement Switcher', 'grade' ),
    'base' => 'spectech_switcher',
    'class' => '',
    'icon' => 'admin-icon-box',
    'category' => esc_html__( 'deTheme', 'grade' ),
    'description'=>__('Show measurement switcher (metric/imperial)','grade'),
    'params' => array(
        array(
          'heading' => esc_html__( 'Label', 'grade' ),
          'param_name' => 'title',
          'admin_label'=>true,
          'value' => '',
          'type' => 'textfield'
         ),
        array(
            'heading' => esc_html__( 'Align', 'grade' ),
            'param_name' => 'align',
            'value' => array('left'=>__( 'Left', 'grade' ),'center'=>__( 'Center', 'grade' ),'right'=>__( 'Right', 'grade' )),
            'type' => 'radio',
            'std'=>'center'
         ),
        )
   ) );

  class WPBakeryShortCode_spectech_switcher extends WPBakeryShortCode {

      function output($atts, $content = null, $base = ''){

          extract( shortcode_atts( array(
              'title'=>'',
              'align'=>'center'
          ), $atts ) );

        $compile='<ul class="spectech-conversion-tool position-'.$align.'">
                    <li class="conversion-tool active conversion-to-metric" data-conversion="metric"><a href="#">'.esc_html__('Metric','grade').'</a></li>
                    <li class="conversion-tool conversion-to-imperial" data-conversion="imperial"><a href="#">'.esc_html__('Imperial','grade').'</a></li>
                  </ul>';

       return $compile;

      }
  }

    function grade_get_specification_field($settings, $value=""){

      $dependency =version_compare(WPB_VC_VERSION,'4.7.0','>=') ? "":vc_generate_dependencies_attributes( $settings );

      $output="";

      $specifications=get_spectech_fields();

      $option_values=!empty($value) ? explode(',', trim($value)) : array();
      $output="<ul class=\"inline-list checkbox_fields ".$settings['param_holder_class']."\" ".$dependency.">";

      if(count($specifications)){
          foreach($specifications as $specification){
            $metaname="_spectech_field_".sanitize_key($specification->field_name);
              $output.='<li><label class="vc_checkbox-label"><input id="'.$settings['param_name'].'-'.$metaname.'" value="'.$metaname.'" class="checkbox_field '.$settings['param_name'].' checkbox" name="'.$settings['param_name'].'_'.$metaname.'" type="checkbox" '.checked(in_array($metaname,$option_values)).'> '.$specification->field_name.'</label></li>';
          }
      }

      $output.='<input id="'.$settings['param_name'].'" value="'.trim($value).'" class="wpb_vc_param_value '.$settings['param_name'].'" name="'.$settings['param_name'].'" type="hidden" />';
      $output.="</ul>";
      return $output;

    }

    grade_add_element_param( 'specification_fields', 'grade_get_specification_field');

    vc_map( array(
    'name' => esc_html__( 'Specification Box', 'grade' ),
    'base' => 'spectech_specsification_box',
    'class' => '',
    'icon' => 'admin-icon-box',
    'category' => esc_html__( 'deTheme', 'grade' ),
    'description'=>esc_html__('Show selected specification of product','grade'),
    'params' => array(
        array(
            'heading' => esc_html__( 'Align', 'grade' ),
            'param_name' => 'align',
            'value' => array('left'=>esc_html__( 'Left', 'grade' ),'center'=>esc_html__( 'Center', 'grade' ),'right'=>esc_html__( 'Right', 'grade' )),
            'type' => 'radio',
            'std'=>'center'
         ),
        array(
            'heading' => esc_html__( 'Spesification Field', 'grade' ),
            'param_name' => 'fields',
            'value' => array('left'=>esc_html__( 'Left', 'grade' ),'center'=>esc_html__( 'Center', 'grade' ),'right'=>esc_html__( 'Right', 'grade' )),
            'type' => 'specification_fields',
            'std'=>''
         ),
        array(
          'heading' => esc_html__( 'Product ID', 'grade' ),
          'param_name' => 'specification_id',
          'value' => '',
          'type' => 'textfield',
          'description'=>esc_html__('Type product ID, leave blank if use current post ID','grade'),
         ),
        )
   ) );

  class WPBakeryShortCode_spectech_specsification_box extends WPBakeryShortCode {

      function output($atts, $content = null, $base = ''){

          extract( shortcode_atts( array(
              'fields'=>'',
              'align'=>'center',
              'specification_id' => 0
          ), $atts ) );

      $fields=!empty($fields) ? explode(",", trim($fields)) : array();

      if(!count($fields)) return "";

      $base_fields=get_spectech_fields();


      $post_id=$specification_id ? $specification_id : get_the_ID();


      $compile="<div class=\"spectech-specsification-box position-".$align."\">";

     foreach ($base_fields as $field) {

                $metaname="_spectech_field_".sanitize_key($field->field_name);

                if(!in_array($metaname,$fields)) continue;

                    $unit_id=isset($field->unit_id) ? $field->unit_id : 0;
                    $value=get_post_meta( $post_id, $metaname, true );
                    $field_value=spectech_get_field_display($value , $unit_id);

                    if(empty($value)) continue;

                    $compile.="<fieldset><legend>".$field->field_name."</legend> <div class=\"spectech-specsification-container\">".$field_value."</div></fieldset>";

      }

      $compile.="</div>";
       return $compile;

      }
  }

    vc_map( array(
    'name' => esc_html__( 'Download Specification', 'grade' ),
    'base' => 'spectech_specsification_download',
    'class' => '',
    'icon' => 'admin-icon-box',
    'category' => esc_html__( 'deTheme', 'grade' ),
    'description'=>esc_html__('Show selected specification of product','grade'),
    'params' => array(
        array(
          'heading' => esc_html__( 'Label', 'grade' ),
          'param_name' => 'title',
          'admin_label'=>true,
          'value' => '',
          'type' => 'textfield'
         ),
        array(
            'heading' => esc_html__( 'Align', 'grade' ),
            'param_name' => 'align',
            'value' => array('left'=>esc_html__( 'Left', 'grade' ),'center'=>esc_html__( 'Center', 'grade' ),'right'=>esc_html__( 'Right', 'grade' )),
            'type' => 'radio',
            'std'=>'center'
         ),
        array(
            'heading' => esc_html__( 'Full Width', 'grade' ),
            'param_name' => 'fullwidth',
            'value' => array('0'=>esc_html__( 'No', 'grade' ),'1'=>esc_html__( 'Yes', 'grade' )),
            'type' => 'radio',
            'std'=>''
         ),
        )
   ) );

  class WPBakeryShortCode_spectech_specsification_download extends WPBakeryShortCode {

      function output($atts, $content = null, $base = ''){

          extract( shortcode_atts( array(
              'title'=>'',
              'align'=>'center',
              'fullwidth'=>''
          ), $atts ) );

      $title=!empty($title) ? $title : esc_html__('Download','grade');

      $spectech_download = get_post_meta( get_the_ID(), '_spectech_download', true );
      $download_url=!empty($spectech_download) ? $spectech_download : "";


      $compile="<a href=\"".esc_url($download_url)."\"><ul class=\"spectech-download-box position-".$align."\">";
      $compile.="<li class=\"spectech-download-icon\"><i class=\"spectech-flaticon-pdf\"></i></li>";
      $compile.="<li class=\"spectech-download-label\">".$title." <i class=\"spectech-flaticon-download\"></i></li>";
      $compile.="</ul></a>";
       return $compile;

      }
  }

  vc_map( array(
    'name' => esc_html__( 'Category Specification', 'grade' ),
    'base' => 'spectech_specification_category',
    'class' => '',
    'icon' => 'admin-icon-box',
    'category' => esc_html__( 'deTheme', 'grade' ),
    'description'=>esc_html__('Show category specification','grade'),
    'params' => array(
        array(
        'heading' => esc_html__( 'Category', 'grade' ),
        'param_name' => 'cat',
        'value' => '',
        'description' => esc_html__( 'Select category', 'grade' ),
        'type' => 'specification_categories',
        'multiple'=>false
         ),
        array(
          'heading' => esc_html__( 'Description Word Length', 'grade' ),
          'param_name' => 'word_length',
          'value' => '20',
          'type' => 'textfield'
         ),
        array(
            'heading' => esc_html__( 'Layout', 'grade' ),
            'param_name' => 'layout_type',
            'param_holder_class'=>'spectech_specification_layout',
            'value'=>array(
              '<img src="'.get_template_directory_uri().'/lib/images/layout-1.png" alt="'.esc_attr__('Layout 1: Image on top','grade').'" />' => '1',
              '<img src="'.get_template_directory_uri().'/lib/images/layout-2.png" alt="'.esc_attr__('Layout 2: Image on left','grade').'"/>' => '2',
            ),
            'type' => 'select_layout',
            'std'=>'1'
         )
        )
   ) );


   function grade_get_specification_categories($settings, $value ) {

       $dependency =version_compare(WPB_VC_VERSION,'4.7.0','>=') ? "":vc_generate_dependencies_attributes( $settings );

        $args = array(
          'orderby' => 'name',
          'show_count' => 0,
          'pad_counts' => 0,
          'taxonomy' => 'spectech_cat',
          'title_li' => '',
        );


    $categories=get_categories($args);

    $output="<div class=\"spectech_cat\" ".$dependency.">";

    $output .= '<select name="'.$settings['param_name'].'" class="wpb_vc_param_value wpb-input wpb-select '.$settings['param_name'].' '.$settings['type'].'">';
    $output .= '<option value="">'.esc_html__('Select Category','grade').'</option>';

    if(count($categories)):

    foreach ( $categories as $category ) {
        $output .= '<option value="'.$category->term_id.'" '.selected($category->term_id, $value).'>'.$category->name.'</option>';
    }

    endif;
    $output .= '</select>';
    $output .="</div>";

    return $output;


    }

    if(version_compare(WPB_VC_VERSION,'4.7.0','>=')){
        grade_vc_add_element_param( 'specification_categories', 'grade_get_specification_categories');
    }
    else{
        grade_add_element_param( 'specification_categories', 'grade_get_specification_categories');
    }


  class WPBakeryShortCode_spectech_specification_category extends WPBakeryShortCode {

      function output($atts, $content = null, $base = ''){

          extract( shortcode_atts( array(
              'layout_type'=>'1',
              'cat'=>0,
              'slug'=>'',
              'word_length'=>20,
          ), $atts ) );

          $category = get_term($cat, 'spectech_cat');

          if((!$category || is_wp_error($category)) && !empty($slug)) {

            $category= get_term_by('slug',$slug,'spectech_cat');
          }

         if(!$category || is_wp_error($category)) return "";

          $description=$category->description;
          $category_short_description=get_term_meta($category->term_id, '_short_description', true);
          $short_description=!empty($category_short_description) ? $category_short_description : $description;
          $category_link=get_term_link( $category);

          $category_image=get_term_meta($category->term_id, '_category_image', true);
          $img_url=$alt_image="";

          $alt_image = get_post_meta($category_image, '_wp_attachment_image_alt', true);


          $category_img_url = wp_get_attachment_url($category_image);
          if(($newsize=grade_aq_resize($category_img_url,640,400,true,false))){
            $img_url=$newsize[0];
          }


ob_start();
?>
<div class="module-specification-category layout-<?php print $layout_type;?>">
  <?php if(!empty($img_url)):?>
  <div class="specification-category-image">
    <a href="<?php print esc_url($category_link);?>"><img class="img-responsive" src="<?php print esc_url($img_url);?>" alt="print esc_attr($alt_image);" /></a>
  </div>
  <?php endif;?>
  <div class="specification-description">
  <a href="<?php print esc_url($category_link);?>"><h2 class="specification-category-title"><?php print $category->name;?></h2></a>
  <?php if ( !empty($short_description) ) : ?>
  <div class="specification-category-shortdescription"><?php echo wp_trim_words($short_description,(int)$word_length,""); ?></div>
  <?php endif; ?>
  <a class="specification-category-link" href="<?php print esc_url($category_link);?>"><?php esc_html_e('read more','grade');?></a>
  </div>
</div>
<?php
        $compile=ob_get_clean();

         return $compile;

      }
  }

  vc_map( array(
    'name' => esc_html__( 'Single Product Specification', 'grade' ),
    'base' => 'spectech_specification_product',
    'class' => '',
    'icon' => 'admin-icon-box',
    'category' => esc_html__( 'deTheme', 'grade' ),
    'description'=>__('Show single product specification','grade'),
    'params' => array(
        array(
          'heading' => esc_html__( 'Product ID', 'grade' ),
          'param_name' => 'specification_id',
          'value' => '',
          'type' => 'textfield'
         ),
        array(
          'heading' => esc_html__( 'Description Word Length', 'grade' ),
          'param_name' => 'word_length',
          'value' => '20',
          'type' => 'textfield'
         )
        )
   ) );


  class WPBakeryShortCode_spectech_specification_product extends WPBakeryShortCode {

      function output($atts, $content = null, $base = ''){

          extract( shortcode_atts( array(
              'specification_id'=>0,
              'word_length'=>20,
          ), $atts ) );

          $specification = get_post($specification_id);

          if(!$specification) return "";

          $specification_link=get_permalink($specification_id);
          $short_description= get_the_excerpt($specification_id);

          if ( ''==$short_description ) {
            $short_description = $specification->post_content;
            $short_description = grade_strip_shortcodes( $short_description );
          }

          $post_title=apply_filters( 'the_title', $specification->post_title, $specification_id );
          $thumb_id = get_post_thumbnail_id($specification->ID);
          $imageurl=$alt_image="";

          $post_img_url = wp_get_attachment_url($thumb_id);

          if(($newsize=grade_aq_resize($post_img_url,640,460,true,false))){
            $imageurl=$newsize[0];
          }

          $alt_image = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);

          $categories=get_the_terms( $specification_id, 'spectech_cat' );

          $category = $categories && isset($categories[0]) ? $categories[0] : false;

ob_start();
?>
<div class="module-specification-product">
  <figure class="spectech-product-container">
    <?php if(!empty($imageurl)):?>
    <div class="spectech-product-image">
      <img class="img-responsive" src="<?php print esc_url($imageurl);?>" alt="<?php print esc_attr($alt_image);?>"/>
    </div>
    <?php endif;?>
    <figcaption class="spectech-product-description">
<div class="figcaption-container">
  <div class="figcaption-inner">

  </div>
</div>
    </figcaption>
  </figure>
  <?php if ($category):?>
  <h2 class="spectech-product-category"><a href="<?php print esc_url($specification_link);?>"><?php print $post_title;?></a></h2>
  <div class="spectech-product-shortdescription" style="display:none;text-align:center;"><?php echo wp_trim_words($short_description,(int)$word_length,""); ?></div>

  <?php endif;?>
</div>
<?php
        $compile=ob_get_clean();

         return $compile;

      }
  }


  vc_map( array(
      'name' => esc_html__( 'Specification Item', 'grade' ),
      'base' => 'dt_specification_item',
      'as_child' => array( 'only' => 'dt_specifications' ),
      'class' => '',
      'controls' => 'full',
      "is_container" => false,
      'icon' => 'admin-icon-box',
      'category' => esc_html__( 'deTheme', 'grade' ),
      'params' => array(
          array(
          'heading' => esc_html__( 'Specification Label', 'grade' ),
          'param_name' => 'block_name',
          'value' => '',
          'admin_label' => true,
          'type' => 'textfield'
           ),
          array(
          'heading' => esc_html__( 'Value', 'grade' ),
          'param_name' => 'block_value',
          'value' => '',
          'type' => 'textfield'
           ),
       )
   ) );



  vc_map( array(
      'name' => esc_html__( 'Specification Table', 'grade' ),
      'base' => 'dt_specifications',
      'as_parent' => array( 'only' => 'dt_specification_item' ),
      'class' => '',
      'controls' => 'full',
      "is_container" => true,
      'icon' => 'admin-icon-box',
      'category' => esc_html__( 'deTheme', 'grade' ),
      'description'=>esc_html__('Shows specification table','grade'),
      'js_view' => 'VcColumnView',
      'params' => array(
          array(
          'heading' => esc_html__( 'Block Title', 'grade' ),
          'param_name' => 'title',
          'admin_label' => true,
          'value' => esc_html__( 'Block Table', 'grade' ),
          'type' => 'textfield'
           ),
      )
   ) );


  class WPBakeryShortCode_dt_specification_item extends WPBakeryShortCode {

       function output($atts, $content = null, $base = ''){

           extract(shortcode_atts(array(
              'block_name' => '',
              'block_value' => '',
          ), $atts));

          $compile = '<li><label>'.trim($block_name).'</label><div class="spectech-specification"><span class="spectech-value">'.$block_value.'</span></div></li>';

          return $compile;
      }
  }



  class WPBakeryShortCode_dt_specifications extends WPBakeryShortCodesContainer {


      function output($atts, $content = null, $base = '') {

          $regexshortcodes=
          '\\['                              // Opening bracket
          . '(\\[?)'                           // 1: Optional second opening bracket for escaping shortcodes: [[tag]]
          . "(dt_specification_item)"                     // 2: Shortcode name
          . '(?![\\w-])'                       // Not followed by word character or hyphen
          . '('                                // 3: Unroll the loop: Inside the opening shortcode tag
          .     '[^\\]\\/]*'                   // Not a closing bracket or forward slash
          .     '(?:'
          .         '\\/(?!\\])'               // A forward slash not followed by a closing bracket
          .         '[^\\]\\/]*'               // Not a closing bracket or forward slash
          .     ')*?'
          . ')'
          . '(?:'
          .     '(\\/)'                        // 4: Self closing tag ...
          .     '\\]'                          // ... and closing bracket
          . '|'
          .     '\\]'                          // Closing bracket
          .     '(?:'
          .         '('                        // 5: Unroll the loop: Optionally, anything between the opening and closing shortcode tags
          .             '[^\\[]*+'             // Not an opening bracket
          .             '(?:'
          .                 '\\[(?!\\/\\2\\])' // An opening bracket not followed by the closing shortcode tag
          .                 '[^\\[]*+'         // Not an opening bracket
          .             ')*+'
          .         ')'
          .         '\\[\\/\\2\\]'             // Closing shortcode tag
          .     ')?'
          . ')'
          . '(\\]?)';                          // 6: Optional second closing brocket for escaping shortcodes: [[tag]]


      if(!preg_match_all( '/' . $regexshortcodes . '/s', $content, $matches, PREG_SET_ORDER ))
          return "";

      extract( shortcode_atts( array(
          'title'=>'',
      ), $atts ) );

      if(!is_admin()){

          wp_enqueue_style( 'detheme-vc');
      }

      $compile='<fieldset class="spectech-group"><h2 class="spectech-group-name">'.$title.'</h2>';
      $compile.='<ul class="spectech-specifications-list">'.do_shortcode($content).'</ul></fieldset>';

      return $compile;
      }
  }




  }
  /* end spectech plugin */

}
/* end vc_set_as_theme */


function grade_strip_shortcodes( $content ) {

      global $shortcode_tags;

      if ( false === strpos( $content, '[' ) ) {
        return $content;
      }

      if (empty($shortcode_tags) || !is_array($shortcode_tags))
        return $content;

      // Find all registered tag names in $content.
      preg_match_all( '@\[([^<>&/\[\]\x00-\x20=]++)@', $content, $matches );
      $tagnames = array_intersect( array_keys( $shortcode_tags ), $matches[1] );

      if ( empty( $tagnames ) ) {
        return $content;
      }


      $content = do_shortcodes_in_html_tags( $content, true, $tagnames );

      $pattern = get_shortcode_regex( $tagnames );

      $content = preg_replace_callback( "/$pattern/", 'grade_strip_shortcode_tag', $content );

      // Always restore square braces so we don't break things like <!--[if IE ]>
      $content = unescape_invalid_shortcodes( $content );

      return $content;
}

function grade_strip_shortcode_tag($m){
  global $shortcode_tags;

  preg_match_all( '@\[([^<>&/\[\]\x00-\x20=]++)@', $m[5], $matches );
  $tagnames = array_intersect( array_keys( $shortcode_tags ), $matches[1] );

  if ( empty( $tagnames ) ) {
    return $m[5];
  }


  $pattern = get_shortcode_regex( $tagnames );


  if( false == strpos( $m[5] , '[')) return preg_replace_callback( "/$pattern/", 'grade_strip_shortcode_tag', $m[5] );

  return $m[5];
}


add_filter( 'get_search_form','grade_get_search_form', 10, 1 );

function grade_get_search_form( $form ) {
    $format = current_theme_supports( 'html5', 'search-form' ) ? 'html5' : 'xhtml';
    $format = apply_filters( 'search_form_format', $format );

    if ( 'html5' == $format ) {
      $form = '<form method="get" class="search-form" action="' . esc_url( home_url( '/' ) ) . '">
        <label>
          <span class="screen-reader-text">' . _x( 'Search for:', 'label','grade' ) . '</span>
          <input type="search" class="search-field" placeholder="'.esc_attr__('search','grade').'" value="' . get_search_query() . '" name="s" title="' . esc_attr_x( 'Search for:', 'label','grade' ) . '" />
          <i class="icon-search-6"></i>
        </label>
        <input type="submit" class="searchsubmit" value="'. esc_attr_x( 'Search', 'submit button', 'grade' ) .'" />
      </form>';
    } else {
      $form = '<form method="get" class="searchform" action="' . esc_url( home_url( '/' ) ) . '">
        <div>
          <label class="screen-reader-text">' . _x( 'Search for:', 'label','grade' ) . '</label>
          <input type="text" value="' . get_search_query() . '" name="s" placeholder="'.esc_attr__('search','grade').'" />
          <i class="icon-search-6"></i>
          <input type="submit" class="searchsubmit" value="'. esc_attr_x( 'Search', 'submit button', 'grade' ) .'" />
        </div>
      </form>';
    }

  return $form;
}


add_filter( 'get_product_search_form','grade_get_product_search_form', 10, 1 );

function grade_get_product_search_form( $form ) {
  $form = '<form method="get" action="' . esc_url( home_url( '/'  ) ) . '">
      <div>
        <label class="screen-reader-text">' . esc_html__( 'Search for:', 'woocommerce' ) . '</label>
        <i class="icon-search-6"></i>
        <input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="' . esc_attr__( 'Search for products', 'woocommerce' ) . '" />
        <input type="submit" class="searchsubmit" value="'. esc_attr__( 'Search', 'woocommerce' ) .'" />
        <input type="hidden" name="post_type" value="product" />
      </div>
    </form>';

  return $form;
}

function is_grade_home($post=null){

  if(!isset($post)) $post=get_post();

  return apply_filters('is_detheme_home',false,$post);
}


function grade_remove_excerpt_more($excerpt_more=""){

  return "&hellip;";
}

add_filter('excerpt_more','grade_remove_excerpt_more');

function grade_prepost_vc_basic_grid_settings($content){

        $regexshortcodes=
        '\\['                              // Opening bracket
        . '(\\[?)'                           // 1: Optional second opening bracket for escaping shortcodes: [[tag]]
        . "(vc_basic_grid|vc_masonry_grid)"// 2: Shortcode name
        . '(?![\\w-])'                       // Not followed by word character or hyphen
        . '('                                // 3: Unroll the loop: Inside the opening shortcode tag
        .     '[^\\]\\/]*'                   // Not a closing bracket or forward slash
        .     '(?:'
        .         '\\/(?!\\])'               // A forward slash not followed by a closing bracket
        .         '[^\\]\\/]*'               // Not a closing bracket or forward slash
        .     ')*?'
        . ')'
        . '(?:'
        .     '(\\/)'                        // 4: Self closing tag ...
        .     '\\]'                          // ... and closing bracket
        . '|'
        .     '\\]'                          // Closing bracket
        .     '(?:'
        .         '('                        // 5: Unroll the loop: Optionally, anything between the opening and closing shortcode tags
        .             '[^\\[]*+'             // Not an opening bracket
        .             '(?:'
        .                 '\\[(?!\\/\\2\\])' // An opening bracket not followed by the closing shortcode tag
        .                 '[^\\[]*+'         // Not an opening bracket
        .             ')*+'
        .         ')'
        .         '\\[\\/\\2\\]'             // Closing shortcode tag
        .     ')?'
        . ')'
        . '(\\]?)';                          // 6: Optional second closing brocket for escaping shortcodes: [[tag]]

  $content=preg_replace('/'.$regexshortcodes.'/s', '[$2 $3 post_id="'.get_the_ID().'"]', $content);
  return $content;

}

function grade_get_pre_footer_page(){

  $post_ID=get_the_ID();

  $originalpost = $GLOBALS['post'];

  if(!get_grade_option('showfooterpage',true) || !get_grade_option('footerpage') || $post_ID==get_grade_option('footerpage'))
    return;

  $post = grade_get_wpml_post(get_grade_option('footerpage'));
  if(!$post)  return;

  $old_sidebar=get_query_var('sidebar');

  set_query_var('sidebar','nosidebar');
  if(get_grade_option('dt-header-type')=='leftbar'){
    $pre_footer_page="<div class=\"vertical_menu_container\">".do_shortcode($post->post_content)."</div>";

  }
  else if($post){


    $GLOBALS['post']=$post;
    $pre_footer_page=do_shortcode(grade_prepost_vc_basic_grid_settings($post->post_content));
    $GLOBALS['post']=$originalpost;


  }
  set_query_var('sidebar',$old_sidebar);
  print "<div class=\"pre-footer-section\">".$pre_footer_page."</div>";

}


add_action('grade_before_footer_section','grade_get_pre_footer_page');

function grade_get_post_footer_page(){

  $post_ID=get_the_ID();

  $originalpost = $GLOBALS['post'];

  if(!get_grade_option('showfooterpage',true) || !get_grade_option('postfooterpage') || $post_ID==get_grade_option('postfooterpage'))
    return;


  $post = grade_get_wpml_post(get_grade_option('postfooterpage'));
  if(!$post)  return;

  $old_sidebar=get_query_var('sidebar');
  set_query_var('sidebar','nosidebar');
  if(get_grade_option('dt-header-type')=='leftbar'){
    $post_footer_page="<div class=\"vertical_menu_container\">".do_shortcode($post->post_content)."</div>";

  }
  else if($post){

    $GLOBALS['post']=$post;
    $post_footer_page=do_shortcode(grade_prepost_vc_basic_grid_settings($post->post_content));
    $GLOBALS['post']=$originalpost;

  }
  set_query_var('sidebar',$old_sidebar);

  print "<div class=\"post-footer-section\">".$post_footer_page."</div>";

}

add_action('grade_after_footer_section','grade_get_post_footer_page');

/*wpml translation */

function grade_get_wpml_post($post_id){

  if(!defined('ICL_LANGUAGE_CODE'))
        return get_post($post_id);

    global $wpdb;

   $postid = $wpdb->get_var(
      $wpdb->prepare("SELECT element_id FROM {$wpdb->prefix}icl_translations WHERE trid=(SELECT trid FROM {$wpdb->prefix}icl_translations WHERE element_id='%d' LIMIT 1) AND element_id!='%d' AND language_code='%s'", $post_id,$post_id,ICL_LANGUAGE_CODE)
   );

  if($postid)
      return get_post($postid);

  return get_post($post_id);
}

function get_grade_menu_pagebar(){

  $post_ID=get_the_ID();
  $originalpost = $GLOBALS['post'];


  if(get_grade_option('dt-header-type')!='pagebar' || !get_grade_option('showpostmenupage') || !get_grade_option('postmenupage') ||
    ($post_ID==get_grade_option('postmenupage') && !is_search())) {
      return;
  }

  $post = grade_get_wpml_post(get_grade_option('postmenupage'));
  if(!$post)  return;

  $old_sidebar=get_query_var('sidebar');

  set_query_var('sidebar','nosidebar');

  $GLOBALS['post']=$post;
  print "<div id=\"".esc_attr('dt_pagebar')."\"><div class=\"dt_pagebar_menu\"><div class=\"menu_background_color\"></div></div><div class=\"dt_pagebar_wrapper\">".do_shortcode(grade_prepost_vc_basic_grid_settings($post->post_content))."</div></div>";

  $GLOBALS['post']=$originalpost;

  set_query_var('sidebar',$old_sidebar);
}

add_action('after_menu_section','get_grade_menu_pagebar');

/* career */

if (grade_plugin_is_active('detheme-career/detheme_career.php')) {


  function get_career_attachment($phpmailer){

     $attachment=$_FILES['attachment'];


      $filesize=$attachment['size'];
      $filesource=$attachment['tmp_name'];
      $filename=$attachment['name'];

      try {
        $phpmailer->AddAttachment($filesource,$filename);
      } catch ( phpmailerException $e ) {

      }
  }

  function grade_proccess_apply_career(){


    global $grade_config;
    $career_id=intVal($_POST['career_id']);
    $fullname=sanitize_text_field($_POST['fullname']);
    $email=sanitize_email($_POST['email']);
    $notes=esc_textarea($_POST['notes']);
    $recipient=$grade_config['career_email'];
    $attachment=$_FILES['attachment'];
    $thankyoumessage=wpautop($grade_config['career_thankyou']);
    $career_attach_type=isset($grade_config['career_attach_type'])?$grade_config['career_attach_type']:false;


    if($recipient==''){

      $super_admins = get_super_admins();
        foreach ($super_admins as $admin) {
            $adminuser=get_user_by('login', $admin);
            $recipient[]=$adminuser->user_email;
        }

    }

    $career=get_post($career_id);

    if(!$career){
      print json_encode(array('error'=>esc_html__('Job position not found or closed','grade')));
      die();

    }

    $from_email = get_bloginfo( 'admin_email' );
    $attachlimit =(isset($grade_config['career_attach_limit']) && ''!=$grade_config['career_attach_limit'])? $grade_config['career_attach_limit']:1024;


    if($attachment){

        if($attachment['size'] > $attachlimit*1024){
           print json_encode(array('error'=>esc_html__('Attachment size exceed allowed','grade')));
           die();

        }

        if($career_attach_type){
            $allowed=false;

            foreach (array_keys($career_attach_type) as $key) {
              if (wp_match_mime_types( $key, $attachment['type'] ) ){
                $allowed=true;
                break;
              }
            }

            if(!$allowed){
               print json_encode(array('error'=>sprintf(esc_html__('%s not allowed','grade'),$attachment['type'])));
               die();
            }

        }
        else{
           print json_encode(array('error'=>esc_html__('Attachment type not allowed','grade')));
           die();

        }
        add_action( 'phpmailer_init', 'get_career_attachment');
    }

    $subject = sprintf(esc_html__('Apply Job for Position %s','grade'),ucwords($career->post_name));
    $headers = "From: " . stripslashes_deep( html_entity_decode( $fullname, ENT_COMPAT, 'UTF-8' ) ) . " <$from_email>\r\n";
    $headers.= "Reply-To: ". $email . "\r\n";
    $headers.= "Return-Path: ".$email."\r\n";
    $headers.= "MIME-Version: 1.0\r\n";
    $headers.= "X-Priority: 1\r\n";


    $headers .= "Content-Type: text/html\n";

    $notes.="\n\n".sprintf(wp_kses(__('This application for job position %s','grade'),array('a'=>array('href'=>array()))),'<a href="'.get_permalink($career_id).'">'.$career->post_name.'</a>');

    $body = wpautop($notes);


    $sendmail=wp_mail( $recipient, $subject, $body, $headers);

    if($sendmail){
      print json_encode(array('success'=>$thankyoumessage));
    }
    else{
      print json_encode(array('error'=>esc_html__('Could not instantiate mail function','grade')));
    }
    die();
  }
  add_action('wp_ajax_grade_apply_career','grade_proccess_apply_career');
  add_action('wp_ajax_nopriv_grade_apply_career','grade_proccess_apply_career');


  function grade_proccess_send_friend_career(){


    global $grade_config;
    $career_id=intVal($_POST['career_id']);
    $fullname=sanitize_text_field($_POST['fullname']);
    $email=sanitize_email($_POST['email']);
    $recipient=sanitize_email($_POST['email_to']);
    $notes=esc_textarea($_POST['notes']);
    $thankyoumessage=__('Your email has been sent','grade');

    $career=get_post($career_id);

    if(!$career){
      print json_encode(array('error'=>esc_html__('Job position not found or closed','grade')));
      die();

    }

    $from_email = $email;


    $subject = sprintf(esc_html__('%s tell you about job for Position %s','grade'),ucfirst($fullname),ucwords($career->post_name));
    $headers = "From: " . stripslashes_deep( html_entity_decode( $fullname, ENT_COMPAT, 'UTF-8' ) ) . " <$from_email>\r\n";
    $headers.= "Reply-To: ". $email . "\r\n";
    $headers.= "Return-Path: ".$email."\r\n";
    $headers.= "MIME-Version: 1.0\r\n";
    $headers.= "X-Priority: 1\r\n";


    $headers .= "Content-Type: text/html\n";


    $body = sprintf(wp_kses(__("Hi,\nYour friend %s about job position %s.\n",'grade'),array('a'=>array('href'=>array()))),$fullname,'<a href="'.get_permalink($career_id).'">'.$career->post_name.'</a>').
    "\n\n".
    wpautop($notes);


    $sendmail=wp_mail( $recipient, $subject, $body, $headers);

    if($sendmail){
      print json_encode(array('success'=>$thankyoumessage));
    }
    else{
      print json_encode(array('error'=>esc_html__('Could not instantiate mail function','grade')));
    }
    die();

  }

  add_action('wp_ajax_grade_send_friend_career','grade_proccess_send_friend_career');
  add_action('wp_ajax_nopriv_grade_send_friend_career','grade_proccess_send_friend_career');

}

/* detheme-post*/

function grade_loadDethemePostTemplate(){

    global $post,$wp_query,$GLOBALS;

    if(!isset($post) || isset($_GET['type']))
        return true;

    $standard_type=$post->post_type;

    if(is_archive() && in_array($standard_type,array('essential_grid'))){

        $post_type_data = get_post_type_object( $standard_type);

        $post_type_slug = $post_type_data->rewrite['slug'];

        if(!$page = get_page_by_path($post_type_slug))
        return true;

        $query_vars=array(
        'post_type' => 'page',
        'page_id'=>$page->ID,
        'posts_per_page'=>1
        );

       $original_query_vars=$wp_query->query_vars;

       $wp_query->query($query_vars);
       if(!$wp_query->have_posts()){
           $wp_query->query($original_query_vars);
           return true;
       }

      $GLOBALS['post']=$page;
    }
    else{
      return true;
    }
}

add_action('template_redirect', 'grade_loadDethemePostTemplate');


/* essential grid post handle */

if (grade_plugin_is_active('essential-grid/essential-grid.php')) {

  function grade_essential_grid_labels($labels){

    $dtpost_settings=get_option('essential_grid_settings');

    if(!$dtpost_settings || !is_array($dtpost_settings)){
      return $labels;
    }

    if(isset($dtpost_settings['label']) && ''!=$dtpost_settings['label']){

      $labels->label=$dtpost_settings['label'];
      $labels->all_items=$dtpost_settings['label'];
      $labels->menu_name=$dtpost_settings['label'];
      $labels->name=$dtpost_settings['label'];

    }

    if(isset($dtpost_settings['singular_label']) && ''!=$dtpost_settings['singular_label']){

      $labels->singular_label=$dtpost_settings['singular_label'];
      $labels->singular_name=$dtpost_settings['singular_label'];

    }

    if(isset($dtpost_settings['slug']) && ''!=$dtpost_settings['slug']){

      $labels->rewrite['slug']=$dtpost_settings['slug'];

    }
    return $labels;
  }

  function grade_essential_grid_setting_page($post){


    $dtpost_settings=get_option('essential_grid_settings',array('label'=>esc_html__("Ess. Grid Posts", 'grade'),'singular_label'=>esc_html__("Ess. Grid Post", 'grade'),'slug'=>''));

    if(wp_verify_nonce( isset($_POST['essential_grid-setting'])?$_POST['essential_grid-setting']:"", 'essential_grid-setting')){

         $dtpost_name=(isset($_POST['dtpost_name']))?$_POST['dtpost_name']:'';
         $singular_name=(isset($_POST['singular_name']))?$_POST['singular_name']:'';
         $rewrite_slug=(isset($_POST['dtpost_slug']))?$_POST['dtpost_slug']:'';

         $do_update=false;

         if($dtpost_name!=$dtpost_settings['label'] && ''!=$dtpost_name){
            $dtpost_settings['label']=$dtpost_name;
            $do_update=true;
         }

         if($singular_name!=$dtpost_settings['singular_label'] && ''!=$singular_name){
            $dtpost_settings['singular_label']=$singular_name;
            $do_update=true;

         }

         if($rewrite_slug!=$dtpost_settings['slug']){
            $dtpost_settings['slug']=$rewrite_slug;
            $do_update=true;

         }

         if($do_update){
             update_option('essential_grid_settings',$dtpost_settings);
         }

    }



    $args = array( 'page' => 'essential_grid_setting');
    $url = esc_url(add_query_arg( $args, admin_url( 'admin.php' )));

    $dtpost_name=$dtpost_settings['label'];
    $singular_name=$dtpost_settings['singular_label'];
    $slug=$dtpost_settings['slug'];
?>
<div class="dtpost-panel">
<h2><?php printf(esc_html__('%s Settings', 'grade'),ucwords($dtpost_name));?></h2>
<form method="post" action="<?php print esc_url($url);?>">
<?php wp_nonce_field( 'essential_grid-setting','essential_grid-setting');?>
<input name="option_page" value="reading" type="hidden"><input name="action" value="update" type="hidden">
<table class="form-table">
<tbody>
<tr>
<th scope="row"><label for="dtpost_name"><?php esc_html_e('Label Name','grade');?></label></th>
<td>
<input name="dtpost_name" id="dtpost_name" max-length="50" value="<?php print sanitize_text_field($dtpost_name);?>" class="" type="text"></td>
</tr>
<tr>
<th scope="row"><label for="singular_name"><?php esc_html_e('Singular Name','grade');?></label></th>
<td>
<input name="singular_name" id="singular_name" max-length="50" value="<?php print sanitize_text_field($singular_name);?>" class="" type="text"></td>
</tr>
<tr>
<th scope="row"><label for="dtpost_slug"><?php esc_html_e('Rewrite Slug','grade');?></label></th>
<td>
<input name="dtpost_slug" id="dtpost_slug" max-length="50" value="<?php print sanitize_text_field($slug);?>" class="" type="text"></td>
</tr>
</tbody></table>


<p class="submit"><input name="submit" id="submit" class="button button-primary" value="<?php esc_html_e('Save Changes','grade');?>" type="submit"></p></form>
</div>
<?php
  }

  function grade_essential_grid_seeting_menu(){
      add_theme_page(esc_html__('Portfolio Settings', 'grade'), esc_html__('Portfolio Settings', 'grade'),'manage_options','essential_grid_setting','grade_essential_grid_setting_page');
  }

  add_action('admin_menu', 'grade_essential_grid_seeting_menu');

  add_filter( 'post_type_labels_essential_grid', 'grade_essential_grid_labels');

  function grade_related_query_post_grid($query){

    if(isset($query['related']) && (bool)$query['related']){

      $query["post_type"]= get_post_type(get_the_id());
      $query['post__not_in']=(isset($query['post__not_in']) && is_array($query['post__not_in']))? array_push(get_the_id(),$query['post__not_in']):array(get_the_id());

      if($query["post_type"] == 'post'){

        $terms = get_the_terms( get_the_id(), 'category' );

        $query['tax_query']=array(
          array(
          'taxonomy'=>'category',
          'field' =>'id',
          'terms'=>$terms
          ),
          'relation'=>'OR');

      }
      elseif($query["post_type"] == 'essential_grid'){

        $terms = get_object_term_cache( get_the_id(), 'essential_grid_category' );

        $terms_ids=array();
        if($terms && count($terms)){
          foreach($terms as $term){
            $terms_ids[]=$term->term_id;
          }

        $query['tax_query']=array(
          array(
          'taxonomy'=>'essential_grid_category',
          'field' =>'id',
          'terms'=>$terms_ids
          ),
          'relation'=>'OR');
        }
        elseif(isset($query['tax_query'])){
          unset($query['tax_query']);
        }
      }
      elseif($query["post_type"] == 'dtpost'){

        $terms = get_object_term_cache( get_the_id(), 'dtpostcat' );

        $terms_ids=array();
        if($terms && count($terms)){
          foreach($terms as $term){
            $terms_ids[]=$term->term_id;
          }

        $query['tax_query']=array(
          array(
          'taxonomy'=>'dtpostcat',
          'field' =>'id',
          'terms'=>$terms_ids
          ),
          'relation'=>'OR');
        }
        elseif(isset($query['tax_query'])){
          unset($query['tax_query']);
        }
      }

    }
    return $query;
  }

  add_filter('essgrid_get_posts','grade_related_query_post_grid');

  add_filter('essgrid_query_caching','__return_false');


  function grade_ess_grid_post_type($post_type, $args){

    global $wp_post_types;

    if($post_type!='essential_grid') return true;

     $dtpost_settings = get_option('essential_grid_settings');

     if(!$dtpost_settings || !isset($dtpost_settings['slug']) || $dtpost_settings['slug']=='') return true;



     $essential_post=$wp_post_types['essential_grid'];
     $essential_post->has_archive=true;
     $essential_post->rewrite['slug']=$dtpost_settings['slug'];

     $wp_post_types['essential_grid']=$essential_post;

     add_rewrite_tag( "%$post_type%", '(.+?)', $args->query_var ? "{$args->query_var}=" : "post_type=$post_type&pagename=" );

     add_rewrite_rule( "{$dtpost_settings['slug']}/?$", "index.php?post_type=$post_type", 'top' );


     $permastruct_args = $args->rewrite;

     $permastruct_args['feed'] = isset($permastruct_args['feeds'])?$permastruct_args['feeds']:false;
     add_permastruct( $post_type, $dtpost_settings['slug']."/%$post_type%", $permastruct_args );
  }

  add_action( 'registered_post_type', 'grade_ess_grid_post_type',999,2);
}


/* comment setting */

function grade_is_comment_open($open, $post_id){

  $post_type = get_post_type($post_id);
  if(!in_array($post_type,grade_post_use_comment())){
    return ((bool)get_grade_option('comment-open-'.$post_type)) && $open;
  }

  return $open;
}

add_filter( 'comments_open','grade_is_comment_open',0,2);

/* dt carousel image size */

function grade_create_carousel_size($out, $id){

  if(!$id) return $out;

  $img_url = wp_get_attachment_url($id);
  if($newsize=grade_aq_resize($img_url,350,230,true,false)){
    return $newsize;
  }
  return $out;
}

add_filter('dt_carousel_pagination_image','grade_create_carousel_size',1,2);

/** Breadcrumbs **/
/** http://dimox.net/wordpress-breadcrumbs-without-a-plugin/ **/

function grade_dimox_breadcrumb($args=array()){

  $args=wp_parse_args($args,array(
    'wrap_before' => '<div class="breadcrumbs">',
    'wrap_after' => '</div>',
    'format' => '<span%s>%s</span>',
    'delimiter'=>'/',
    'current_class' => 'current',
    'home_text' => esc_html__('Home','grade'),
    'home_link' => home_url('/')
   ));

   $breadcrumbs=grade_get_breadcrumbs($args);

    if (grade_plugin_is_active('woocommerce/woocommerce.php') && (is_product()||is_cart()||is_checkout()||is_shop()||is_product_category())) {
      // do nothing
      // woocomerce has different breadcrumb method
    } else {
       $out=$args['wrap_before'];
       $out.=join($args['delimiter']."\n",is_rtl()?array_reverse($breadcrumbs):$breadcrumbs);
       $out.=$args['wrap_after'];
       print $out;
    }
}

if ( ! function_exists( 'detheme_woocommerce_breadcrumb' ) ) {

  /**
   * Output the WooCommerce Breadcrumb
   */
  function grade_woocommerce_breadcrumb(&$breadcrumbs, $args = array() ) {
    $wc_breadcrumb_args = array(
      'delimiter' => $args['delimiter'],
      'wrap_before' => '<div class="breadcrumbs">',
      'wrap_after' => '</div>',
      'before' => '<span>',
      'beforecurrent' => '<span class="current">',
      'after' => '</span>',
      'home' => $args['home_text'],
    );

    woocommerce_breadcrumb($wc_breadcrumb_args);

  }
}



function grade_get_breadcrumbs($breadcrumb_args) {
  global $post;

   $breadcrumbs[]=sprintf($breadcrumb_args['format'],is_front_page()?' class="current"':'','<a href="'.esc_url($breadcrumb_args['home_link']).'" title="'.$breadcrumb_args['home_text'].'">'.$breadcrumb_args['home_text'].'</a>');

  if (is_front_page()) { // home page
  } elseif (is_home()) { // blog page
      grade_get_breadcrumbs_from_menu(get_option('page_for_posts'),$breadcrumbs,$breadcrumb_args);
  } elseif (is_singular()) {
        if (grade_plugin_is_active('woocommerce/woocommerce.php') && (is_product()||is_cart()||is_checkout())) {
            grade_woocommerce_breadcrumb($breadcrumbs,$breadcrumb_args);
        } else {

              if($post->post_type=='post'){
                  grade_get_breadcrumbs_from_menu(get_option('page_for_posts'),$breadcrumbs,$breadcrumb_args,false);
              }
              else{
                $post_type_data = get_post_type_object($post->post_type);
                $post_type_slug = $post_type_data->rewrite['slug'];

                $page = get_page_by_path($post_type_slug);

                if ($page) {
                  grade_get_breadcrumbs_from_menu($page->ID,$breadcrumbs,$breadcrumb_args,false);
                }
                else{
                  array_push($breadcrumbs, sprintf($breadcrumb_args['format']," class=\"".$breadcrumb_args['current_class']."\"",$post_type_data->labels->singular_name));

                }

              }

              array_push($breadcrumbs, sprintf($breadcrumb_args['format']," class=\"".$breadcrumb_args['current_class']."\"",$post->post_title));
        }
  } else {
      $post_id = -1;
      if (grade_plugin_is_active('woocommerce/woocommerce.php') && (is_shop()||is_product_category())) {

        grade_woocommerce_breadcrumb($breadcrumbs,$breadcrumb_args);

      } else {

        if(is_category()){
          $breadcrumbs[]=sprintf($breadcrumb_args['format']," class=\"".$breadcrumb_args['current_class']."\"",single_cat_title(' ',false));
        }
        elseif(is_archive()){
          $breadcrumbs[]=sprintf($breadcrumb_args['format']," class=\"".$breadcrumb_args['current_class']."\"",is_tag()||is_tax()?single_tag_title(' ',false):single_month_title( ' ', false ));
        }
        else{
          if (isset($post->ID)) {
            $post_id = $post->ID;
            grade_get_breadcrumbs_from_menu($post_id,$breadcrumbs,$breadcrumb_args);
          }
        }
      }
  }

  return apply_filters('detheme_dimox_breadcrumbs',$breadcrumbs,$breadcrumb_args);
}


function grade_get_breadcrumbs_from_menu($post_id,&$breadcrumbs,$args,$iscurrent=true) {
  $primary = get_nav_menu_locations();

  if (isset($primary['primary'])) {
    $navs = wp_get_nav_menu_items($primary['primary']);

    if (!empty($navs)) :
      foreach ($navs as $nav) {
        if (($nav->object_id)==$post_id) {

          if ($nav->menu_item_parent!=0) {
            //start recursive by menu parent
            grade_get_breadcrumbs_from_menu_by_menuid($nav->menu_item_parent,$breadcrumbs,$args);
          }

          if ($iscurrent) {
            array_push($breadcrumbs, sprintf($args['format']," class=\"".$args['current_class']."\"",$nav->title));
          } else {
            array_push($breadcrumbs, sprintf($args['format'],"", '<a href="'.esc_url($nav->url).'" title="'.esc_attr($nav->title).'">'.$nav->title .'</a>' ));
          }

          break;
        }
      }
    endif;
  }
}

function grade_get_breadcrumbs_from_menu_by_menuid($menu_id,&$breadcrumbs,$args) {
  $primary = get_nav_menu_locations();

  if (isset($primary['primary'])) {
    $navs = wp_get_nav_menu_items($primary['primary']);

    foreach ($navs as $nav) {
      if (($nav->ID)==$menu_id) {

        if ($nav->menu_item_parent!=0) {
          //recursive by menu parent
          grade_get_breadcrumbs_from_menu_by_menuid($nav->menu_item_parent,$breadcrumbs,$args);
        }

        array_push($breadcrumbs, sprintf($args['format'],"",'<a href="'.esc_url($nav->url).'" title="'.esc_attr($nav->title).'">'.$nav->title .'</a>'));

        break;
      }
    }
  }
}


function grade_remove_blog_slug( $wp_rewrite ) {
  if ( ! is_multisite() )
    return;
  // check multisite and main site
  if ( ! is_main_site() )
    return;

  // set checkup
  $rewrite = FALSE;

  // update_option
  $wp_rewrite->permalink_structure = preg_replace( '!^(/)?blog/!', '$1', $wp_rewrite->permalink_structure );
  update_option( 'permalink_structure', $wp_rewrite->permalink_structure );

  // update the rest of the rewrite setup
  $wp_rewrite->author_structure = preg_replace( '!^(/)?blog/!', '$1', $wp_rewrite->author_structure );
  $wp_rewrite->date_structure = preg_replace( '!^(/)?blog/!', '$1', $wp_rewrite->date_structure );
  $wp_rewrite->front = preg_replace( '!^(/)?blog/!', '$1', $wp_rewrite->front );

  // walk through the rules
  $new_rules = array();
  foreach ( $wp_rewrite->rules as $key => $rule )
    $new_rules[ preg_replace( '!^(/)?blog/!', '$1', $key ) ] = $rule;
  $wp_rewrite->rules = $new_rules;

  // walk through the extra_rules
  $new_rules = array();
  foreach ( $wp_rewrite->extra_rules as $key => $rule )
    $new_rules[ preg_replace( '!^(/)?blog/!', '$1', $key ) ] = $rule;
  $wp_rewrite->extra_rules = $new_rules;

  // walk through the extra_rules_top
  $new_rules = array();
  foreach ( $wp_rewrite->extra_rules_top as $key => $rule )
    $new_rules[ preg_replace( '!^(/)?blog/!', '$1', $key ) ] = $rule;
  $wp_rewrite->extra_rules_top = $new_rules;

  // walk through the extra_permastructs
  $new_structs = array();
  foreach ( $wp_rewrite->extra_permastructs as $extra_permastruct => $struct ) {
    $struct[ 'struct' ] = preg_replace( '!^(/)?blog/!', '$1', $struct[ 'struct' ] );
    $new_structs[ $extra_permastruct ] = $struct;
  }
  $wp_rewrite->extra_permastructs = $new_structs;
}

add_action( 'generate_rewrite_rules', 'grade_remove_blog_slug' );

if(grade_plugin_is_active('woocommerce/woocommerce.php')){

require_once( get_template_directory().'/lib/woocommerce.php');

}

 add_filter("dt_carousel_navigation_btn",'grade_dt_carousel_pagination');

 function grade_dt_carousel_pagination($pagination){
  return array('<span class="btn-owl prev page"><i class="icon-angle-left"></i></span>','<span class="page">/</span>','<span class="btn-owl next page"><i class="icon-angle-right"></i></span>');
 }


function grade_banner_height_setting($height){

  if(get_post_type()=='essential_grid'){
    return get_grade_option('dt-ess-banner-height');
  }

  return $height;
}

add_filter('detheme_banner_height','grade_banner_height_setting');

add_filter( 'wp_nav_menu_items','grade_add_search_box_to_menu', 10, 2 );

function grade_add_search_box_to_menu( $items, $args ) {
  if(get_grade_option('dt-header-type')=='middle') :

    if(get_grade_option('show-header-shoppingcart')):
      if( $args->theme_location == 'primary' ) :
        if ( grade_plugin_is_active('woocommerce/woocommerce.php') ) :

          if ( function_exists('WC') && sizeof( WC()->cart->get_cart() ) > 0 ) :
            $items .= '<li id="menu-item-888888" class="hidden-mobile bag menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-888888">
                      <a href="#">
                        <span><i class="icon-cart"></i><span class="item_count">'. WC()->cart->get_cart_contents_count() . '</span></span>
                      </a>

                      <label for="fof888888" class="toggle-sub" onclick="">&rsaquo;</label>
                      <input id="fof888888" class="sub-nav-check" type="checkbox">
                      <ul id="fof-sub-888888" class="sub-nav">
                        <li class="sub-heading">'.esc_html__('Shopping Cart','grade').' <label for="fof999999" class="toggle" onclick="" title="'.esc_attr__('Back','grade').'">&lsaquo; '.esc_html__('Back','grade').'</label></li>
                        <li>
                          <!-- popup -->
                          <div class="cart-popup"><div class="widget_shopping_cart_content"></div></div>
                          <!-- end popup -->
                        </li>
                      </ul>

                    </li>';
          else:
              $items .= '<li id="menu-item-888888" class="hidden-mobile bag menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-888888">
                        <a href="#">
                          <span><i class="icon-cart"></i> <span class="item_count">0</span></span>
                        </a>

                        <label for="fof888888" class="toggle-sub" onclick="">&rsaquo;</label>
                        <input id="fof888888" class="sub-nav-check" type="checkbox">
                        <ul id="fof-sub-888888" class="sub-nav">
                          <li class="sub-heading">'.esc_html__('Shopping Cart','grade').' <label for="fof888888" class="toggle" onclick="" title="'.esc_attr__('Back','grade').'">&lsaquo; '.esc_html__('Back','grade').'</label></li>
                          <li>
                            <!-- popup -->
                            <div class="cart-popup"><div class="widget_shopping_cart_content"></div></div>
                            <!-- end popup -->
                          </li>
                        </ul>

                      </li>';
          endif;
        endif;
      endif;
    endif;
  endif;
  return $items;
}

function grade_style_loader_tag($tag){

  return preg_replace('/\<link/','<link property', $tag);
}
add_filter('style_loader_tag','grade_style_loader_tag');

if ( ! function_exists( 'grade_hex2rgba' ) ) {
  function grade_hex2rgba( $hex, $alpha = '' ) {
      $hex = str_replace( "#", "", $hex );
      if ( strlen( $hex ) == 3 ) {
          $r = hexdec( substr( $hex, 0, 1 ) . substr( $hex, 0, 1 ) );
          $g = hexdec( substr( $hex, 1, 1 ) . substr( $hex, 1, 1 ) );
          $b = hexdec( substr( $hex, 2, 1 ) . substr( $hex, 2, 1 ) );
      } else {
          $r = hexdec( substr( $hex, 0, 2 ) );
          $g = hexdec( substr( $hex, 2, 2 ) );
          $b = hexdec( substr( $hex, 4, 2 ) );
      }
      $rgb = $r . ',' . $g . ',' . $b;

      if ( '' == $alpha ) {
          return 'rgba(' . $rgb . ',' . 0 . ')';
      } else {
          $alpha = floatval( $alpha );

          return 'rgba(' . $rgb . ',' . $alpha . ')';
      }
  }
}

function grade_set_blog_masonry_posts_per_page($query) {
    if (($query->is_posts_page==1) && (get_grade_option('blog_type')=='masonry')) {
      $query->set( 'posts_per_page', -1 );
      return;
    }
}

function grade_get_link_pages_args() {
  $detheme_link_pages_args = array(
    'before'           => '<div class="row"><div class="billio_link_page container">',
    'after'            => '</div></div>',
    'link_before'      => '<span class="page-numbers">',
    'link_after'       => '</span>',
    'next_or_number'   => 'number',
    'separator'        => ' ',
    'nextpagelink'     => esc_html__( 'Next page','grade' ),
    'previouspagelink' => esc_html__( 'Previous page','grade' ),
    'pagelink'         => '%',
    'echo'             => 1
  );

  return $detheme_link_pages_args;
}

function grade_set_global_more($value=0) {
  global $more;

  $more = $value;
}

function grade_set_global_more_post() {
  global $more;

  if (is_single()) {
    $more = 1;
  } else {
    $more = 0;
  }
}

function grade_set_global_style($value) {
  global $detheme_Style;

  $detheme_Style[] = $value;
}

function grade_get_global_var($varname) {
  if (isset($GLOBALS[$varname])) {
    return $GLOBALS[$varname];
  }
}

function grade_set_global_var($varname,$value) {
    $GLOBALS[$varname] = $value;
}

function grade_get_logo_content() {
  $logo             = get_grade_option('dt-logo-image');
  $logo_url         = isset($logo['url']) ? $logo['url'] : "";

  $logo_transparent = get_grade_option('dt-logo-image-transparent');
  $logo_transparent_url= isset($logo_transparent['url']) ? $logo_transparent['url'] : "";

  $logoContent = "";
  $logotext = get_grade_option('dt-logo-text','');

  if(!empty($logo_url)){

    $altimage = esc_attr(sanitize_title($logotext));
    $altwidth = ( $logowidth=get_grade_option('logo-width')) ? " width=\"".intval($logowidth)."\"" : "";


    $logoContent = sprintf('<a href="%s" style=""><img id="logomenu" src="%s" alt="%s" class="img-responsive halfsize" %s></a>',
      esc_url(home_url()),
      esc_url(grade_maybe_ssl_url($logo_url)),
      $altimage,
      $altwidth
    );

    $logoContent .= sprintf('<a href="%s" style=""><img id="logomenureveal" src="%s" alt="%s" class="img-responsive halfsize" %s></a>',
      esc_url(home_url()),
      esc_url(grade_maybe_ssl_url($logo_transparent_url)),
      $altimage,
      $altwidth
    );
  } else{
    if (!empty($logotext)) {
      $logoContent = sprintf('<div class="header-logo"><a class="navbar-brand-desktop" href="%s">%s</a></div>',
        esc_url(home_url()),
        $logotext
      );
    }
  }

  return $logoContent;
}

function grade_load_vc_nav_buttons(){


  $fields=get_grade_option('menu_icon_fields');

  if(!$fields || !is_array($fields)) return;

  print '<div class="navigation_button">';

  $i=0;

  foreach($fields as $field){

    print '<div class="navigation_button_item'.($i=='0' ? " navigation_active":"").'">';
    print (isset($field['icon']) && ($icon=$field['icon'])) ? "<i class=\"{$icon}\"></i>" : "";

    if((isset($field['label']) && $field['label']!='') || (isset($field['text']) && $field['text']!='')){

      print "<div class=\"text-box\">";
      print isset($field['label']) && $field['label']!='' ? "<div class=\"navigation-label\">".$field['label']."</div>": "";
      print isset($field['text']) && $field['text']!='' ? "<div class=\"navigation-text\">".$field['text']."</div>": "";
      print "</div>";


    }
    print '</div>';

    $i++;

  }
  print '</div>';
}

add_action('detheme_load_vc_nav_buttons','grade_load_vc_nav_buttons');

if ( ! is_admin() ) {
// ---------------------- FRONTPAGE -------------------
if ( defined('WC_VERSION') ) {
// ---------------------- WooCommerce active -------------------

    /**
   * Set Pagination for shortcodes custom loop on single-pages.
     * @uses $woocommerce_loop;
     */
    add_action( 'pre_get_posts', 'grade_wc_pre_get_posts_query' );
    function grade_wc_pre_get_posts_query( $query ) {
      global $woocommerce_loop;

      if ( $query->is_main_query() ){

        if ( isset($query->query['paged']) ){
          $woocommerce_loop['paged'] = $query->query['paged'];
        }
      }


      if ( ! $query->is_post_type_archive || empty($query->query['post_type']) || $query->query['post_type'] !== 'product' ){
        return;
      }

      $query->is_paged = true;
      $paged = (isset($woocommerce_loop['paged'])) ? $woocommerce_loop['paged'] : 1;
      $query->query['paged'] = $paged;
      $query->query_vars['paged'] = $paged;

    }

  /** Prepare Pagination data for shortcodes on pages
     * @uses $woocommerce_loop;
   */
  add_action( 'loop_end', 'grade_query_loop_end' );
  function grade_query_loop_end( $query ) {

    if ( ! $query->is_post_type_archive || empty($query->query['post_type']) || $query->query['post_type'] !== 'product' ){
      return;
    }

    // Cache data for pagination
    global $woocommerce_loop;
    $woocommerce_loop['pagination']['paged'] = 1;

    $paged = (isset($woocommerce_loop['paged'])) ? $woocommerce_loop['paged'] : 1;
    $woocommerce_loop['pagination']['paged'] = $paged;

    $woocommerce_loop['pagination']['found_posts'] = $query->found_posts;
    $woocommerce_loop['pagination']['max_num_pages'] = $query->max_num_pages;
    $woocommerce_loop['pagination']['post_count'] = $query->post_count;
    $woocommerce_loop['pagination']['current_post'] = $query->current_post;
  }
  /**
   * Pagination for shortcodes on single-pages
   * @uses $woocommerce_loop;
   */
  add_action( 'woocommerce_after_template_part', 'grade_wc_shortcode_pagination' );
  function grade_wc_shortcode_pagination( $template_name ) {
    if ( ! ( $template_name === 'loop/loop-end.php' && is_page() ) ){
      return;
    }
    global $wp_query, $woocommerce_loop;
    if ( ! isset( $woocommerce_loop['pagination'] ) ){
      return;
    }
    $wp_query->query_vars['paged'] = $woocommerce_loop['pagination']['paged'];
    $wp_query->query['paged'] = $woocommerce_loop['pagination']['paged'];
    $wp_query->max_num_pages = $woocommerce_loop['pagination']['max_num_pages'];
    $wp_query->found_posts = $woocommerce_loop['pagination']['found_posts'];
    $wp_query->post_count = $woocommerce_loop['pagination']['post_count'];
    $wp_query->current_post = $woocommerce_loop['pagination']['current_post'];

    // Custom pagination function or default woocommerce_pagination()
    grade_woocommerce_pagination();
  }
  /**
   * Custom pagination for WooCommerce instead the default woocommerce_pagination()
   * @uses plugin Prime Strategy Page Navi, but added is_singular() on #line16
   */
  remove_action('woocommerce_after_shop_loop', 'woocommerce_pagination', 10);
  add_action( 'woocommerce_after_shop_loop', 'grade_woocommerce_pagination', 10);
  function grade_woocommerce_pagination() {
    woocommerce_pagination();
  }
}// END WOOCOMMERCE
}// END FRONTPAGE


/* spectech plugin */

if(grade_plugin_is_active('detheme-spectech/spectech.php') && get_option( 'db_version' ) >= 34370 ){

  function grade_specification_category_form($tag, $taxonomy=""){

     $term_id= is_object($tag) && $tag->term_id ? $tag->term_id : 0;

     $show_excerpt=get_term_meta($term_id, '_show_excerpt', true);
     $specification_fields=(array)get_term_meta($term_id, '_show_specification_fields', true);
     $short_description=get_term_meta($term_id, '_short_description', true);
?>
<table class="form-table">
<tr class="form-field">
    <th scope="row">
        <label for="short-description"><?php esc_html_e('Short Description','grade') ?></label>
    </th>
    <td>
      <textarea id="short-description" name="short_description" cols="50" rows="5"><?php print esc_textarea($short_description);?></textarea>
      <p class="description"><?php esc_html_e('Short description will show if this category shown on parent category.','grade');?></p>

    </td>
</tr>
<tr class="form-field">
    <th scope="row">
        <label for="specification-excert-show"><?php esc_html_e('Product Teaser','grade') ?></label>
    </th>
    <td>
      <input type="checkbox" name="show_excerpt" value="1" <?php checked('1',$show_excerpt);?>/> <span><?php esc_html_e('show product teaser/excerpt','grade');?></span>
      <p class="description"><?php esc_html_e('Show product short description on category view (blog view).','grade');?></p>
    </td>
</tr>
<tr class="form-field">
    <th scope="row">
        <label for="specification-field-show"><?php esc_html_e('Product Specification Field','grade') ?></label>
    </th>
    <td>
      <p class="description"><?php esc_html_e('Select specification field will shown on category view (blog view).','grade');?></p>
<?php
$specifications=get_spectech_fields();

/* grouping */

$specifications_grouped=array();

foreach($specifications as $field){
  $specifications_grouped[$field->field_group][$field->field_id]=$field;
}

foreach ($specifications_grouped as $group_name => $grouped_fields) {
?>
<h2><?php print esc_html($group_name);?></h2>
<ul>
<?php

foreach ($grouped_fields as $field) {

  $metaname="_spectech_field_".sanitize_key($field->field_name);
  print "<li><input type=\"checkbox\" name=\"specification_fields[]\" value=\"".$metaname."\" ".checked(true, in_array($metaname,$specification_fields),false).">".$field->field_name."</li>";

}
?>
</ul>
<?php
}
?>

    </td>
</tr>
</table>

<?php

  }

  function grade_specification_category_save_form($term_id){

    $show_excerpt = isset($_POST['show_excerpt']) ? true : false;

    $old_show_excerpt=get_term_meta($term_id, '_show_excerpt', true);
    update_term_meta($term_id, '_show_excerpt', $show_excerpt, $old_show_excerpt);

    $specification_fields = isset($_POST['specification_fields']) ? (array) $_POST['specification_fields'] : array();

    $old_show_specifications=get_term_meta($term_id, '_show_specification_fields', true);
    update_term_meta($term_id, '_show_specification_fields', $specification_fields, $old_show_specifications);

    $short_description = isset($_POST['short_description']) ? esc_textarea($_POST['short_description']) : '';

    $old_description=get_term_meta($term_id, '_short_description', true);
    update_term_meta($term_id, '_short_description', $short_description, $old_description);
  }


  function grade_manage_category_columns($columns){
      $columns['shortdesc'] = esc_html__('Description', 'grade');

      unset($columns['description']);

      return $columns;
  }

  function grade_manage_category_columns_fields($deprecated, $column_name, $term_id){

    if($column_name=='shortdesc'){
        $description=get_term_meta($term_id, '_short_description', true);

        print $description;
    }
  }


  function grade_category_short_description(){

        add_action( 'spectech_cat_edit_form','grade_specification_category_form',10,2);
        add_action( 'spectech_cat_add_form', 'grade_specification_category_form',10,2);
        add_action( 'edit_term', "grade_specification_category_save_form", 10, 3 );
        add_action( 'create_term', "grade_specification_category_save_form", 10, 3 );

      if(get_option( 'db_version' ) >= 34370 ) {

        add_filter('manage_edit-spectech_cat_columns', 'grade_manage_category_columns',1);
        add_action('manage_spectech_cat_custom_column', 'grade_manage_category_columns_fields', 10, 3);

      }
  }

  add_action( 'admin_init', 'grade_category_short_description' );


}

/* post_title_ filter */

function grade_fix_post_title($title){
  return strip_tags($title, '<i><span>');
}

add_filter( 'the_title', 'grade_fix_post_title');

/* spectech plugin */

if(get_option( 'db_version' ) >= 34370 ){
  require_once( get_template_directory().'/lib/taxonomy-banner.php');
}
?>
