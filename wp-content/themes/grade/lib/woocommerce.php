<?php
defined('ABSPATH') or die();

function grade_wc_body_class( $classes ) {
  $classes = (array) $classes;

  $col="";

  if ( is_shop() || is_product_category() || is_product_tag() || is_product_taxonomy()) {

    $col=get_option('loop_shop_columns',4);
  }
  elseif(is_product()){

    $col=get_option('loop_related_columns',3);
  }
  elseif(is_cart()){

    $col=get_option('loop_cross_sells_columns',2);

  }

  if($col!='') {
    $classes[] = 'columns-'.$col;
  }

  return array_unique( $classes );
}

function grade_related_products_args($args){

  $col=get_option('loop_related_columns',3);

  if($related_per_page=get_option('loop_related_per_page')){
    $args['posts_per_page']=$related_per_page;  
  }

  $args['columns']=$col;
  return $args;

}

function grade_woocommerce_product_settings($settings=array()){

  if(is_array($settings) && count($settings)){

    $newsettings=array();

    foreach ($settings as $key => $setting) {

      array_push($newsettings, $setting);
      if(isset($setting['id']) && 'woocommerce_shop_page_id'==$setting['id']){

                array_push($newsettings,
                        array(
                          'title'    => esc_html__( 'Num Product Show', 'grade' ),
                          'desc'     => esc_html__( 'This controls num product show', 'grade' ),
                          'id'       => 'loop_per_page',
                          'default'  => get_option( 'posts_per_page'),
                          'type'     => 'number',
                          'css'      => 'width:50px;',
                          'custom_attributes' => array(
                            'min'  => 1,
                            'step' => 1,
                            'max' =>100
                          ),
                          'desc_tip' =>  true,
                        ),
                        array(
                        'title'    => esc_html__( 'Product Display Columns', 'grade' ),
                        'desc'     => esc_html__( 'This controls num column product display', 'grade' ),
                        'id'       => 'loop_shop_columns',
                        'class'    => 'wc-enhanced-select',
                        'css'      => 'min-width:300px;',
                        'default'  => '3',
                        'type'     => 'select',
                        'options'  => array(
                          '2' => esc_html__( 'Two Columns', 'grade' ),
                          '3' => esc_html__( 'Three Columns', 'grade' ),
                          '4' => esc_html__( 'Four Columns', 'grade' ),
                          '5' => esc_html__( 'Five Columns', 'grade' ),
                          '6' => esc_html__( 'Six Columns', 'grade' ),
                        ),
                        'desc_tip' =>  true,
                      ),
                        array(
                          'title'    => esc_html__( 'Num Related/Upsell Product Show', 'grade' ),
                          'desc'     => esc_html__( 'This controls num related/upsell product show', 'grade' ),
                          'id'       => 'loop_related_per_page',
                          'default'  => 4,
                          'type'     => 'number',
                          'css'      => 'width:50px;',
                          'custom_attributes' => array(
                            'min'  => 1,
                            'step' => 1,
                            'max' =>18
                          ),
                          'desc_tip' =>  true,
                        ),
                        array(
                        'title'    => esc_html__( 'Related/Upsell Product Display Columns', 'grade' ),
                        'desc'     => esc_html__( 'This controls num column related/upsell product display', 'grade' ),
                        'id'       => 'loop_related_columns',
                        'class'    => 'wc-enhanced-select',
                        'css'      => 'min-width:300px;',
                        'default'  => '3',
                        'type'     => 'select',
                        'options'  => array(
                          '2' => esc_html__( 'Two Columns', 'grade' ),
                          '3' => esc_html__( 'Three Columns', 'grade' ),
                          '4' => esc_html__( 'Four Columns', 'grade' ),
                          '5' => esc_html__( 'Five Columns', 'grade' ),
                          '6' => esc_html__( 'Six Columns', 'grade' ),
                        ),
                        'desc_tip' =>  true,
                      )
                );
      }


    }

    return $newsettings;

  }

  return $settings;
}

function grade_woocommerce_payment_gateways_settings($settings=array()){

  if(is_array($settings) && count($settings)){

    $newsettings=array();

    foreach ($settings as $key => $setting) {

      array_push($newsettings, $setting);

      if( isset($setting['id']) && 'woocommerce_checkout_page_id'== $setting['id'] ){

                array_push($newsettings,
                      
                        array(
                        'title'    => esc_html__( 'Cross Sell Display Columns', 'grade' ),
                        'desc'     => esc_html__( 'This controls num column cross sell display', 'grade' ),
                        'id'       => 'loop_cross_sells_columns',
                        'class'    => 'wc-enhanced-select',
                        'css'      => 'min-width:300px;',
                        'default'  => '3',
                        'type'     => 'select',
                        'options'  => array(
                          '2' => esc_html__( 'Two Columns', 'grade' ),
                          '3' => esc_html__( 'Three Columns', 'grade' ),
                          '4' => esc_html__( 'Four Columns', 'grade' ),
                          '5' => esc_html__( 'Five Columns', 'grade' ),
                          '6' => esc_html__( 'Six Columns', 'grade' ),
                        ),
                        'desc_tip' =>  true,
                      ),
                        array(
                        'title'    => esc_html__( 'Cross Sell Display Product', 'grade' ),
                        'desc'     => esc_html__( 'This controls num cross sell product display', 'grade' ),
                        'id'       => 'loop_cross_sells_total',
                        'default'  => '3',
                        'type'     => 'number',
                        'css'      => 'width:50px;',
                        'custom_attributes' => array(
                          'min'  => 1,
                          'step' => 1,
                          'max' =>12
                        ),
                        'desc_tip' =>  true,
                      )
                );
      
      }


    }

    return $newsettings;

  }

  return $settings;

}

function grade_woocommerce_order_button_html(){
  $order_button_text  = apply_filters( 'woocommerce_order_button_text', esc_html__( 'Place order', 'woocommerce' ));
  $button_html='<input type="submit" class="button btn btn-primary" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr( $order_button_text ) . '" data-value="' . esc_attr( $order_button_text ) . '" />';
  return $button_html;

}

function grade_woocommerce_product_description_heading($heading){

  return '';
}

function grade_woocommerce_product_review_comment_form_args($comment_form=array()){

  $comment_form['class_submit']='btn btn-primary';
  return $comment_form;
}

add_filter( 'woocommerce_output_related_products_args', 'grade_related_products_args' );
add_filter( 'body_class', 'grade_wc_body_class' );
add_filter( 'loop_shop_columns',create_function('$column','return ($col=get_option(\'loop_shop_columns\'))?$col:$column;'));
add_filter( 'woocommerce_product_settings','grade_woocommerce_product_settings');
add_filter( 'woocommerce_product_description_heading', 'grade_woocommerce_product_description_heading' );
add_filter( 'woocommerce_payment_gateways_settings','grade_woocommerce_payment_gateways_settings');
add_filter( 'woocommerce_cross_sells_columns', create_function('$column','return get_option(\'loop_cross_sells_columns\',$column);') );
add_filter( 'woocommerce_cross_sells_total', create_function('$post_per_page','return get_option(\'loop_cross_sells_total\', $post_per_page);') );
add_filter( 'loop_shop_per_page', create_function( '$post_per_page', 'return get_option(\'loop_per_page\', $post_per_page);' ), 20 );
add_filter( 'woocommerce_order_button_html', 'grade_woocommerce_order_button_html');
add_filter( 'woocommerce_product_review_comment_form_args', 'grade_woocommerce_product_review_comment_form_args' );
?>