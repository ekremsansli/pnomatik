<?php
defined('ABSPATH') or die();
/**
 * The default template for displaying content quote on masonry layout
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Grade
 * @since Grade 1.0
 */
?>

<?php 
	$featured_image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full',false); 
	$bg_url = '';
	if (isset($featured_image[0])) {
		$bg_url = $featured_image[0];
	} 
?>		

		<div class="container-fluid">
			<div class="row">
					<div class="postcontent postcontent-quote primary_color_bg" style="background-image: url('<?php echo esc_url($bg_url)?>');">
						<?php the_content(); ?>
					</div>
			</div>
			<div class="row">
				<?php if(is_rtl()):?>
					<div class="col-xs-12 text-left blog_info_share">
						<?php get_template_part('pagetemplates/ms_social_share'); ?>
					</div>
				<?php else:?>
					<div class="col-xs-12 text-right blog_info_share">
						<?php get_template_part('pagetemplates/ms_social_share'); ?>
					</div>
				<?php endif;?>

			</div>
		</div>