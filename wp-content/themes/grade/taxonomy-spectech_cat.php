<?php
defined('ABSPATH') or die();
/**
 * The single spectech category
 *
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Grade
 * @since Grade 1.0
 */

get_header();

get_template_part('pagetemplates/scrollingsidebar');

$sidebar=is_active_sidebar( 'detheme-sidebar' )?'detheme-sidebar':false;
$sidebar_position = "nosidebar";

set_query_var('sidebar',$sidebar);
$class_sidebar = $sidebar_position;
$vertical_menu_container_class = (get_grade_option('dt-header-type')=='leftbar')?" vertical_menu_container":"";

$category = get_queried_object();
$description = get_term_field( 'description', $category->term_id, "spectech_cat" );
$category_childrens = get_terms( array("taxonomy"=>"spectech_cat","hide_empty"=>false, "parent"=>$category->term_id) );

$is_parent_category=($category_childrens && count($category_childrens));
?>
<div class="content <?php print ($is_parent_category? "main-category ":"").sanitize_html_class($class_sidebar)." ".sanitize_html_class($vertical_menu_container_class); ?>">
<?php if(!empty($description) && $is_parent_category)	:?>
<div class="box-container vc_row wpb_row vc_row-fluid parent-category">
	<div class="container dt-container">
		<div class="archive-meta"><?php echo do_shortcode($description); ?></div>
	</div>
</div>
<?php endif;?>
	<div class="container">
		<div class="row">

		<?php if(get_grade_option('dt-show-title-page')):?>
		<h1 class="page-title"><?php print get_grade_option('page-title');?></h1>
		<?php endif;?>


<?php if ($sidebar_position=='nosidebar') { ?>
			<div class="col-sm-12">

<?php	} else { ?>
			<div class="col-sm-8 <?php print ($sidebar_position=='sidebar-left')?" col-sm-push-4":"";?> col-md-9 <?php print ($sidebar_position=='sidebar-left')?" col-md-push-3":"";?>">
<?php	} ?>

			<?php if ( !empty($description) && !$is_parent_category) : ?>
			<div class="archive-meta"><?php echo do_shortcode($description); ?></div>
			<?php endif; ?>



		</div>
<?php if ('sidebar-right'==$sidebar_position) { ?>
			<div class="col-sm-4 col-md-3 sidebar">
				<?php get_sidebar(); ?>
			</div>
<?php }
	elseif ($sidebar_position=='sidebar-left') { ?>
			<div class="col-sm-4 col-md-3 sidebar col-sm-pull-8 col-md-pull-9">
				<?php get_sidebar(); ?>
			</div>
<?php }?>

		</div>
	</div>
</div>
<?php echo do_shortcode('[vc_row expanded="2" css=".vc_custom_1469170203479{margin-top:-120px!important;background-color: #fec56b !important;}"][vc_column][vc_row_inner css=".vc_custom_1469171164271{margin-top: 50px !important;margin-bottom: 30px !important;}"][vc_column_inner width="7/12"][section_header main_heading=" Sipariş ve Detaylı Bilgi için <b>Bize Ulaşın</b>" text_align="left" font_size="small" use_multi_color="1" multi_color="#4a4a4a | #4a4a4a | #4a4a4a | #4a4a4a | #202020 | #202020"][/vc_column_inner][vc_column_inner width="2/12" css=".vc_custom_1469171062885{margin-top: 8px !important;}"][vc_btn title="Bize Ulaşın" style="outline-custom" outline_custom_color="#4a4a4a" outline_custom_hover_background="#4a4a4a" outline_custom_hover_text="#ffffff" align="right" link="url:%23|||"][/vc_column_inner][vc_column_inner width="2/12" css=".vc_custom_1469171069297{margin-top: 8px !important;}"][vc_btn title="Ekipmanlar" style="custom" custom_background="#696969" custom_text="#ffffff" align="left" link="url:%23|||"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]');?>
<?php get_footer(); ?>
