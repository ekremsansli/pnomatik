<?php 
defined('ABSPATH') or die();
/** 
 * @version: 1.0.0
 * @since: 1.0.0
 */


$category=grade_get_global_var('category_child');
$description=$category->description;
$category_short_description=get_term_meta($category->term_id, '_short_description', true);
$short_description=!empty($category_short_description) ? $category_short_description : $description;

$category_image=get_term_meta($category->term_id, '_category_image', true);
$img_url="";

$category_img_url = wp_get_attachment_url($category_image);
if(($newsize=grade_aq_resize($category_img_url,640,500,true,false))){

	$img_url=$newsize[0];
}
$alt_image = get_post_meta($category_image, '_wp_attachment_image_alt', true);
$category_link=get_term_link( $category);
?>
<div class="spectech-child-category" >
	<figure class="spectech-child-category-container">
		<?php if(!empty($img_url)):?>
		<div class="spectech-child-category-image">
			<img class="img-responsive" src="<?php print esc_url($img_url);?>" alt="<?php print esc_attr($alt_image);?>"/>
		</div>
		<?php endif;?>
		<figcaption class="spectech-child-category-description">
<div class="figcaption-container">
	<div class="figcaption-inner">
			<h2 class="spectech-child-category-title"><?php print $category->name;?></h2>
			<?php if ( !empty($short_description) ) : ?>
			<div class="child-archive-meta"><?php echo wp_trim_words($short_description,20,""); ?></div>
			<?php endif; ?>
			<a class="btn btn-primary" href="<?php print esc_url($category_link);?>"><?php esc_html_e('learn more','grade');?></a>
	</div>
</div>
		</figcaption>
	</figure>
</div>