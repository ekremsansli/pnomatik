<?php
defined('ABSPATH') or die();
/**
 * @package WordPress
 * @subpackage Grade
 * @since Grade 1.0
 */

$imageId=get_post_thumbnail_id(get_the_ID());

$thumb_id = get_post_thumbnail_id(get_the_ID());
$imageurl="";

$post_img_url = wp_get_attachment_url($thumb_id);

if(($newsize=grade_aq_resize($post_img_url,640,500,true,false))){
	$imageurl=$newsize[0];
}

$alt_image = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
$link=get_the_permalink();

?>
<div class="col-xs-12 col-lg-4">
	<a href="<?php print esc_url($link); ?>">
	<figure>
		<?php if($post_img_url):?>
		<div class="top-image">
			<img class="img-responsive" alt="<?php print esc_attr($alt_image);?>" src="<?php print esc_url($imageurl);?>" title=""/>
		</div>
	<?php endif;?>
	<figcaption>
		<h3 class="related-title"><?php the_title();?></h3>
	</figcaption>
	</figure>
	</a>
</div>