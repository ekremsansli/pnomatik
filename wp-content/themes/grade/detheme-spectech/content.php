<?php
defined('ABSPATH') or die();
/**
 *
 * @package WordPress
 * @subpackage Grade
 * @since Grade 1.0
 */
?>
<?php if(is_single()):

/* single content */

?>
<div class="row">
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="col-xs-12">
		<div class="postcontent">
			<div class="row">
				<div class="col-xs-12">
				<?php if(get_grade_option('dt-show-title-page') && (bool)get_grade_option('essential_grid-title')):?>
					<h2 class="blog-post-title"><a href="<?php echo esc_url(get_permalink()); ?>"><?php the_title();?></a></h2>
	    		<?php endif;?>
	    		<?php
					the_content();
				?>
				<?php
/* related product */
	$related_args=array(
		'posts_per_page' => 3,
		'post_type' => 'specification',
		'no_found_rows' => false,
		'meta_key' => '_thumbnail_id',
		'post_status' => 'publish',
		'orderby' => 'rand',
		'post__not_in'=>array(get_the_ID())
	);
	$related = new WP_Query($related_args);

	if($related->have_posts()):
?>

				<div class="spectech-related">
				<h2><?php esc_html_e('Benzer Ekipmanlar','grade');?></h2>
<div class="spectech-related-container">
	<div class="row">
					<?php
					while ( $related->have_posts() ) :
						$related->the_post();
						get_template_part('detheme-spectech/related-specification');
					?>
					<?php endwhile;
?>
	</div>
</div>
				</div>
<?php endif;?>
				</div>
			</div>
		</div>
	</div>
</article>
</div>
<?php

else:

/* teaser content */

$thumb_id = get_post_thumbnail_id($post->ID);
$imageurl="";

$post_img_url = wp_get_attachment_url($thumb_id);

if(($newsize=grade_aq_resize($post_img_url,640,500,true,false))){
	$imageurl=$newsize[0];
}

$alt_image = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
$category= grade_get_global_var('spectech_category');
$specification_fields= $category->specification_fields;
?>
<div class="row">
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="col-xs-12">
		<div class="spectech-specification-post-list">
			<div class="row">
<?php if(!empty($imageurl)):?>
				<div class="col-xs-12 col-lg-6">
					<div class="spectech-list-image">
					<a href="<?php print get_the_permalink($post->ID);?>">
						<img src="<?php print esc_url($imageurl);?>" alt="<?php print esc_attr($alt_image);?>" />
					</a>
					</div>
				</div>
<?php endif;?>
				<div class="col-xs-12 <?php print (!empty($imageurl))? "col-lg-6":"col-lg-12";?>">
					<div class="spectech-list-content">
						<a href="<?php print get_the_permalink($post->ID);?>">
						<h2 class="list-content-title"><?php the_title();?></h2>
						</a>
						<?php if(get_term_meta($category->term_id, '_show_excerpt', true)):?>
						<div class="list-content-excerpt">
						<?php print get_the_excerpt();?>
						</div>
						<?php endif;?>
						<?php if(isset($category->show_specifications) &&  ($show_specifications=$category->show_specifications)){?>
							<ul class="spectech-specifications-list">
								<?php
									foreach ($specification_fields as $specification_field) {

										$metaname="_spectech_field_".sanitize_key($specification_field->field_name);

										if(!in_array($metaname,$show_specifications)) continue;

										$value=get_post_meta( $post->ID, $metaname, true );

										if(empty($value)) continue;

										$unit_id=isset($specification_field->unit_id) ? $specification_field->unit_id : 0;
										$field_value=spectech_get_field_display($value , $unit_id);

										print "<li><label>".$specification_field->field_name."</label> ".$field_value."</li>";

									}
								?>
							</ul>


<?php						}?>
						<a class="btn btn-secondary-color" href="<?php print get_the_permalink($post->ID);?>"><?php esc_html_e('read more','grade');?></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
</div>

<?php endif;?>
