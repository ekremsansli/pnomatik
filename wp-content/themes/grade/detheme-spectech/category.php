<?php 
defined('ABSPATH') or die();
/** 
 * @version: 1.0.0
 * @since: 1.0.0
 */


$category=grade_get_global_var('spectech_category');
$description=$category->description;

$category_childrens = get_terms( array("taxonomy"=>"spectech_cat","hide_empty"=>true, "parent"=>$category->term_id) );
?>
<div class="spectech-sub-category">
	<h2 class="spectech-sub-category-title"><?php print $category->name;?></h2>
	<?php if ( !empty($description) ) : ?>
	<div class="archive-meta"><?php echo do_shortcode($description); ?></div>
	<?php endif; ?>
	<?php if($category_childrens && count($category_childrens)):?>
	<div class="spectech-sub-category-wrap">
		<div class="row">
		<?php 
		foreach ($category_childrens as $sub_category) {?>
		<div class="col-lg-4 col-xs-12">
		<?php
			grade_set_global_var("category_child" , $sub_category);
			get_template_part('detheme-spectech/category',"sub");
			?>
		</div>
		<?php
		}
		?>
		</div>
	</div>	
	<?php else:

	$category_link=get_term_link( $category);

?>
	<a class="spectech-sub-category-link" href="<?php print esc_url($category_link);?>"><?php esc_html_e('read more','grade');?></a>
<?php
	endif;?>

</div>