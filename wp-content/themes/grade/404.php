<?php
defined('ABSPATH') or die();
/**
 *
 *
 * @package WordPress
 * @subpackage Grade
 * @since Grade 1.0
 */
get_header();
?>
<div clss='content'>
<div class="nosidebar">
	<div class="container">
		<?php if (get_grade_option('dt-404-page') && $post = get_post(get_grade_option('dt-404-page'))) :
		if(get_grade_option('dt-show-title-page')):?>
		<h1 class="page-title"><?php print get_grade_option('page-title','');?></h1>
		<?php endif;?>
		<div class="row">
			<div class="col-xs-12">
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="row">
						<div class="col-xs-12 col-sm-12">
							<div class="postcontent">
<?php
	print do_shortcode($post->post_content);	
?>
							</div>
						</div>
					</div>
					</article>
		</div>
		<?php else:?>
<div class="page-404">
<div class="page-404-heading1">404</div>
   <div class="page-404-subheading"><?php esc_html_e('OOPS! SOMETHING GOES WRONG','grade');?></div>
    <p><?php esc_html_e('This is not the page you are looking for.','grade');?></p>
    <div class="page-404-button"><a href="<?php print esc_url(home_url('/'));?>" class="btn btn-ghost skin-dark"><?php esc_html_e('back to homepage','grade');?></a></div>
</div>
		<?php endif;?>

	</div><!-- .container -->
</div>
</div><!-- .page -->
</div>

<?php wp_footer(); ?>
</body>
</html>
