<?php
/**
 * @package WordPress
 * @subpackage Grade
 * @since Grade 1.0
 */

defined('ABSPATH') or die();
 
$sidebar=get_query_var('sidebar');
dynamic_sidebar($sidebar);
?>