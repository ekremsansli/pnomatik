# Copyright (C) 2016 the detheme WordPress team
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Grade 1.0\n"
"Report-Msgid-Bugs-To: support@detheme.com\n"
"POT-Creation-Date: 2016-10-25 05:25:07+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2016-MO-DA HO:MI+ZONE\n"
"Last-Translator: Atawai <support@detheme.com>\n"
"Language-Team: LANGUAGE <support@detheme.com>\n"

## metaboxes.php

msgid "Page Attributes"
msgstr ""

msgid "(no parent)"
msgstr ""

msgid "Parent"
msgstr ""

msgid "Hide title"
msgstr ""

msgid "Hide banner"
msgstr ""

msgid "Disable Page Loader"
msgstr ""

msgid "Left"
msgstr ""

msgid "Right"
msgstr ""

msgid "No Sidebar"
msgstr ""

msgid "Template"
msgstr ""

msgid "Page Template"
msgstr ""

msgid "Default Template"
msgstr ""

msgid "Sidebar Position"
msgstr ""

msgid "Default"
msgstr ""

msgid "Order"
msgstr ""

msgid "Need help? Use the Help tab in the upper right of your screen."
msgstr ""

msgid "Cover"
msgstr ""

msgid "Cover All"
msgstr ""

msgid "ontain"
msgstr ""

msgid "No Repeat"
msgstr ""

msgid "Repeat"
msgstr ""

msgid "Parallax"
msgstr ""

msgid "Parallax All"
msgstr ""

msgid "Fixed"
msgstr ""

msgid "Background"
msgstr ""

msgid "Color"
msgstr ""

msgid "Image"
msgstr ""

msgid "Set background image"
msgstr ""

msgid "Remove background image"
msgstr ""

msgid "Background Style"
msgstr ""

msgid "Banner"
msgstr ""

msgid "Set Page Banner"
msgstr ""

msgid "Remove Page Banner"
msgstr ""

msgid "Select Banner"
msgstr ""

msgid "Insert Banner"
msgstr ""

## page-options.php

msgid "Category : %s"
msgstr ""

msgid "Tag : %s"
msgstr ""

msgid "%s : Archive"
msgstr ""

msgid "Archive : %s"
msgstr ""

## shortcode_panel.php

msgid "Search Icon"
msgstr ""

msgid "Primary"
msgstr ""

msgid "Secondary"
msgstr ""

msgid "Ghost Button"
msgstr ""

msgid "Link"
msgstr ""

msgid "Underlined"
msgstr ""

msgid "Rounded"
msgstr ""

msgid "Large"
msgstr ""

msgid "Extra small"
msgstr ""

msgid "Button URL"
msgstr ""

msgid "Add the button's url eg http://example.com"
msgstr ""

msgid "utton skin"
msgstr ""

msgid "Dark (default)"
msgstr ""

msgid "Light"
msgstr ""

msgid "Select the button's skin"
msgstr ""

msgid "Button style"
msgstr ""

msgid "Select the button's style, ie the button's colour"
msgstr ""

msgid "Button size"
msgstr ""

msgid "Button Target"
msgstr ""

msgid "Self"
msgstr ""

msgid "Blank"
msgstr ""

msgid "Button Text"
msgstr ""

msgid "Select the button's text"
msgstr ""

msgid "Number"
msgstr ""

msgid "The value must be numeric"
msgstr ""

msgid "Separator Color"
msgstr ""

msgid "Medium"
msgstr ""

msgid "Icon Size"
msgstr ""

msgid "Select the button's size"
msgstr ""

msgid "Icon Color"
msgstr ""

msgid "Select the button's color"
msgstr ""

msgid "Icon Style"
msgstr ""

msgid "Select the button's style"
msgstr ""

msgid "Circle"
msgstr ""

msgid "Square"
msgstr ""

msgid "Ghost"
msgstr ""

msgid "Icon"
msgstr ""

msgid "Insert Shortcode"
msgstr ""

## custom_function.php

msgid "Bigger"
msgstr ""

msgid "Big"
msgstr ""

msgid "Small"
msgstr ""

msgid "Smaller"
msgstr ""

## widgets.php

msgid "Display popular posts, recent posts, and recent comments in Tabulation."
msgstr ""

msgid "DT Tabs"
msgstr ""

msgid "Recent Posts"
msgstr ""

msgid "Popular"
msgstr ""

msgid "Recent"
msgstr ""

msgid "Title:"
msgstr ""

msgid "Number of posts/comments to show:"
msgstr ""

msgid "View all posts filed under %s"
msgstr ""

msgid "Feed for all posts filed under %s"
msgstr ""

msgid "Display information in accordion style."
msgstr ""

msgid "DT Accordion"
msgstr ""

msgid "Accordion Title 1:"
msgstr ""

msgid "Accordion Description 1:"
msgstr ""

msgid "Accordion Title 2:"
msgstr ""

msgid "Accordion Description 2:"
msgstr ""

msgid "Accordion Title 3:"
msgstr ""

msgid "Accordion Description 3:"
msgstr ""

msgid "Accordion Title 4:"
msgstr ""

msgid "Accordion Description 4:"
msgstr ""

msgid "Sidebar Widget Area"
msgstr ""

msgid "Bottom Widget Area"
msgstr ""

msgid "Sticky Widget Area"
msgstr ""

msgid "Shop Sidebar Widget Area"
msgstr ""

msgid "Sidebar will display on woocommerce page only"
msgstr ""

## woocommerce.php

msgid "Num Product Show"
msgstr ""

msgid "This controls num product show"
msgstr ""

msgid "Product Display Columns"
msgstr ""

msgid "This controls num column product display"
msgstr ""

msgid "Two Columns"
msgstr ""

msgid "Three Columns"
msgstr ""

msgid "Four Columns"
msgstr ""

msgid "Five Columns"
msgstr ""

msgid "Six Columns"
msgstr ""

msgid "Num Related/Upsell Product Show"
msgstr ""

msgid "This controls num related/upsell product show"
msgstr ""

msgid "Related/Upsell Product Display Columns"
msgstr ""

msgid "This controls num column related/upsell product display"
msgstr ""

msgid "Cross Sell Display Columns"
msgstr ""

msgid "This controls num column cross sell display"
msgstr ""

msgid "Cross Sell Display Product"
msgstr ""

msgid "This controls num cross sell product display"
msgstr ""

## custom_functions.php

msgid "Page Atribute"
msgstr ""

msgid "Back"
msgstr ""

msgid "full name"
msgstr ""

msgid "email address"
msgstr ""

msgid "website"
msgstr ""

msgid "Required fields are marked %s"
msgstr ""

msgid "our message"
msgstr ""

msgid "Leave a Comment"
msgstr ""

msgid "Leave a Comment to %s"
msgstr ""

msgid "Cancel reply"
msgstr ""

msgid "Submit"
msgstr ""

msgid "You must be <a href=\"%s\">logged in</a> to post a comment."
msgstr ""

msgid "Logged in as <a href=\"%1$s\">%2$s</a>. <a href=\"%3$s\" title=\"Log out of this account\">Log out?</a>"
msgstr ""

msgid "Your email address will not be published."
msgstr ""

msgid "Reply"
msgstr ""

msgid "Log in to Reply"
msgstr ""

msgid "Edit This"
msgstr ""

msgid "Pingback:"
msgstr ""

msgid "(Edit)"
msgstr ""

msgid "%d View"
msgstr ""

msgid "% Views"
msgstr ""

msgid "Success"
msgstr ""

msgid "Info"
msgstr ""

msgid "Warning"
msgstr ""

msgid "Danger"
msgstr ""

msgid "Size"
msgstr ""

msgid "Expand section width"
msgstr ""

msgid "Expand Column"
msgstr ""

msgid "Expand Background"
msgstr ""

msgid "Make section \"out of the box\"."
msgstr ""

msgid "Extended options"
msgstr ""

msgid "Background Type"
msgstr ""

msgid "Video"
msgstr ""

msgid "Video Source"
msgstr ""

msgid "Local Server"
msgstr ""

msgid "Youtube/Vimeo"
msgstr ""

msgid "Background Video (mp4)"
msgstr ""

msgid "Background Video (webm)"
msgstr ""

msgid "Background Image"
msgstr ""

msgid "Video link"
msgstr ""

msgid "Add YouTube/Vimeo link"
msgstr ""

msgid "Extra id"
msgstr ""

msgid "If you wish to add anchor id to this row. Anchor id may used as link like href=\"#yourid\""
msgstr ""

msgid "Image Hover Option"
msgstr ""

msgid "None"
msgstr ""

msgid "Text"
msgstr ""

msgid "Animation Style"
msgstr ""

msgid "From Left"
msgstr ""

msgid "From Right"
msgstr ""

msgid "From Top"
msgstr ""

msgid "From Bottom"
msgstr ""

msgid "Border"
msgstr ""

msgid "Outline"
msgstr ""

msgid "Shadow"
msgstr ""

msgid "Bordered shadow"
msgstr ""

msgid "3D Shadow"
msgstr ""

msgid "Round"
msgstr ""

msgid "Round Border"
msgstr ""

msgid "Round Outline"
msgstr ""

msgid "Round Shadow"
msgstr ""

msgid "Round Border Shadow"
msgstr ""

msgid "Circle Border"
msgstr ""

msgid "Circle Outline"
msgstr ""

msgid "Circle Shadow"
msgstr ""

msgid "Circle Border Shadow"
msgstr ""

msgid "Diamond"
msgstr ""

msgid "Pre Title"
msgstr ""

msgid "Title"
msgstr ""

msgid "Spectech Measurement Switcher"
msgstr ""

msgid "Show measurement switcher (metric/imperial)"
msgstr ""

msgid "Label"
msgstr ""

msgid "Align"
msgstr ""

msgid "Center"
msgstr ""

msgid "Description"
msgstr ""

msgid "Specification Box"
msgstr ""

msgid "Spesification Field"
msgstr ""

msgid "Product ID"
msgstr ""

msgid "Type product ID, leave blank if use current post ID"
msgstr ""

msgid "Download Specification"
msgstr ""

msgid "Show selected specification of product"
msgstr ""

msgid "Full Width"
msgstr ""

msgid "Download"
msgstr ""

msgid "Category Specification"
msgstr ""

msgid "Show category specification"
msgstr ""

msgid "Category"
msgstr ""

msgid "Select category"
msgstr ""

msgid "Description Word Length"
msgstr ""

msgid "Layout"
msgstr ""

msgid "Layout 1: Image on top"
msgstr ""

msgid "Layout 2: Image on left"
msgstr ""

msgid "Single Product Specification"
msgstr ""

msgid "Show single product specification"
msgstr ""

msgid "Specification Item"
msgstr ""

msgid "Specification Label"
msgstr ""

msgid "Value"
msgstr ""

msgid "Specification Table"
msgstr ""

msgid "Shows specification table"
msgstr ""

msgid "Block Title"
msgstr ""

msgid "Block Table"
msgstr ""

msgid "Job position not found or closed"
msgstr ""

msgid "Attachment size exceed allowed"
msgstr ""

msgid "%s not allowed"
msgstr ""

msgid "Attachment type not allowed"
msgstr ""

msgid "Apply Job for Position %s"
msgstr ""

msgid "This application for job position %s"
msgstr ""

msgid "Could not instantiate mail function"
msgstr ""

msgid "%s tell you about job for Position %s"
msgstr ""

msgid "Hi,\nYour friend %s about job position %s.\n"
msgstr ""

msgid "Ess. Grid Posts"
msgstr ""

msgid "Ess. Grid Post"
msgstr ""

msgid "%s Settings"
msgstr ""

msgid "Label Name"
msgstr ""

msgid "Singular Name"
msgstr ""

msgid "Rewrite Slug"
msgstr ""

msgid "Save Changes"
msgstr ""

msgid "Portfolio Settings"
msgstr ""

msgid "Home"
msgstr ""

msgid "Shopping Cart"
msgstr ""

msgid "Short Description"
msgstr ""

msgid "Short description will show if this category shown on parent category."
msgstr ""

msgid "Product Teaser"
msgstr ""

msgid "show product teaser/excerpt"
msgstr ""

msgid "Show product short description on category view (blog view)."
msgstr ""

msgid "Product Specification Field"
msgstr ""

msgid "Select specification field will shown on category view (blog view)."
msgstr ""

## theme part
msgid "OOPS! SOMETHING GOES WRONG"
msgstr ""

msgid "This is not the page you are looking for."
msgstr ""

msgid "back to homepage"
msgstr ""

msgid "Comments are closed."
msgstr ""

msgid "Ready to publish your first post? <a href=\"%1$s\">Get started here</a>."
msgstr ""

msgid "Sorry, but nothing matched your search terms. Please try again with some different keywords."
msgstr ""

msgid "It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help."
msgstr ""

msgid "No Comments"
msgstr ""

msgid "1 Comment"
msgstr ""

msgid "% Comments"
msgstr ""

msgid "By "
msgstr ""

msgid "Read More"
msgstr ""

msgid "read more"
msgstr ""

msgid "By %s"
msgstr ""

msgid "Search for:"
msgstr ""

msgid "Search"
msgstr ""

msgid "search"
msgstr ""

msgid "About %d result found for %s"
msgstr ""

msgid "No result have been found for your search query"
msgstr ""

msgid "Please try again or navigate another page"
msgstr ""

msgid "Metric"
msgstr ""

msgid "Imperial"
msgstr ""

msgid "WPBakery Visual Composer"
msgstr ""

msgid "Grade Visual Composer Add On"
msgstr ""

msgid "Contact Form 7"
msgstr ""

msgid "WooCommerce - excelling eCommerce"
msgstr ""

msgid "Revolution Slider"
msgstr ""

msgid "Grade Icon Font - Add-on"
msgstr ""

msgid "Detheme Megamenu Plugin"
msgstr ""

msgid "WP Spechtech Plugin"
msgstr ""

msgid "Detheme Custom Post"
msgstr ""

msgid "Essential Grid"
msgstr ""

msgid "DT Recruitment"
msgstr ""

msgid "Grade Demo Installer"
msgstr ""

msgid "Top Navigation"
msgstr ""

msgid "Apply Now"
msgstr ""

msgid "Full Name"
msgstr ""

msgid "e.g. John Smith"
msgstr ""

msgid "Email"
msgstr ""

msgid "e.g. john.smith@hotmail.com"
msgstr ""

msgid "Covering Note"
msgstr ""

msgid "Upload CV"
msgstr ""

msgid "Maximum file size %.2fMb"
msgstr ""

msgid "Email To a Friend"
msgstr ""

msgid "Friend Email"
msgstr ""

msgid "e.g. alice@hotmail.com"
msgstr ""

msgid "Quick Note"
msgstr ""

msgid "Type a quick message directed to your friend. Please avoid content that could be considered as spam as this could result in a ban from the site."
msgstr ""

msgid "Send Message"
msgstr ""

msgid "learn more"
msgstr ""

msgid "Related Product"
msgstr ""

msgid "Close"
msgstr ""

msgid "Menu"
msgstr ""

msgid "previous page"
msgstr ""

msgid "next page"
msgstr ""

msgid "Next page"
msgstr ""

msgid "Previous page"
msgstr ""

msgid "About %s"
msgstr ""

msgid "Share"
msgstr ""

msgid "Product Name"
msgstr ""

msgid "You may also be interested in the following product(s)"
msgstr ""

msgid "Here’s a selection of items which we think might go well with your choice"
msgstr ""