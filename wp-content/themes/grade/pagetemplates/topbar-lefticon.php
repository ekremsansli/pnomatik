<?php
defined('ABSPATH') or die();
/**
 * left top-bar type icon 
 *
 *
 * @package WordPress
 * @subpackage Grade
 * @since Grade 1.0
 */

$menu=get_grade_option('dt-left-top-bar-menu');

if(!empty($menu)):

	$menuParams=array(
            	'menu'=>$menu,
            	'echo' => false,
            	'container_class'=>'left-menu',
            	'menu_class'=>'nav navbar-nav topbar-icon',
            	'container'=>'div',
                  'theme_location'=>'left-menu',
			'before' => '',
            	'after' => '',
            	'fallback_cb'=>false,
                  'walker' => new grade_iconmenu_walker()
			);

	$menu=wp_nav_menu($menuParams);

	print ($menu)?$menu:"";
?>
<?php endif;?>
