<?php
defined('ABSPATH') or die();
/**
 *
 * @package WordPress
 * @subpackage Grade
 * @since Grade 1.0
 */

$menu=get_grade_option('dt-right-top-bar-menu');

if(!empty($menu)):

	$menuParams=array(
            	'menu'=>$menu,
            	'echo' => false,
            	'container_class'=>'right-menu',
            	'menu_class'=>'nav navbar-nav topbar-icon',
            	'container'=>'div',
			'before' => '',
            	'after' => '',
            	'fallback_cb'=>false,
                  'theme_location'=>'right-menu',
                  'walker' => new grade_iconmenu_walker()
			);

	$menu=wp_nav_menu($menuParams);

      if($menu){
            print $menu;
      }
?>
<?php endif;?>
