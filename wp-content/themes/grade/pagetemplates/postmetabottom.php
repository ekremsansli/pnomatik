<?php
defined('ABSPATH') or die();
?>
					<div class="postmetabottom">
						<div class="row">
						<?php if(is_rtl()):?>
							<div class="col-xs-6 text-left blog_info_share">
								<?php get_template_part('pagetemplates/ms_social_share','share'); ?>
							</div>
							<div class="col-xs-6">
								<a target="_self" class="btn-readmore btn btn-primary" href="<?php echo esc_url(get_permalink()); ?>"><?php esc_html_e('Read More','grade') ?></a>
							</div>
						<?php else:?>
							<div class="col-xs-6">
								<a target="_self" class="btn-readmore  btn btn-primary" href="<?php echo esc_url(get_permalink()); ?>"><?php esc_html_e('Read More','grade') ?></a>
							</div>
							<div class="col-xs-6 text-right blog_info_share">
								<?php get_template_part('pagetemplates/ms_social_share','share'); ?>
							</div>
						<?php endif;?>
						</div>

						<div class="postborder"></div>
					</div>

