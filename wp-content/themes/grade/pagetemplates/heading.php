<?php
defined('ABSPATH') or die();
/**
 * heading section
 *
 *
 * @package WordPress
 * @subpackage Grade
 * @since Grade 1.0
 */

$stretched_menu_class = get_grade_option("dt-is-stretched-menu") ? "stretched_menu":"";
$menu_type_class = get_grade_option("dt-header-type","");
?>

<div id="head-page" class="head-page<?php  print is_admin_bar_showing()?" adminbar-is-here":" adminbar-not-here";?> notopbar <?php print sanitize_html_class($stretched_menu_class); ?>  <?php print sanitize_html_class($menu_type_class); ?> menu_background_color">
<?php

  get_template_part('lib/class/class.grade_mainmenu_walker');

  $headerType=get_grade_option('dt-header-type','');


  $logo_img = get_grade_option('dt-logo-image');

  $logo = isset($logo_img['url']) ? $logo_img['url'] : "";

  $logo_overlay_img = get_grade_option('dt-logo-image-overlay');
  $logo_overlay = isset($logo_overlay_img['url']) ? $logo_overlay_img['url'] : "";

  $logo_transparent_img = get_grade_option('dt-logo-image-transparent');
  $logo_transparent = isset($logo_transparent_img['url']) ? $logo_transparent_img['url'] : "";


  $logoContent=$searchContent=$shoppingCart="";
  $logo_width_style = $headerType=='pagebar' &&  get_grade_option('logo-width') ? " width:".(int)get_grade_option('logo-width')."px;": "";

  if($headerType!='middle'):
    if(!empty($logo)){
      $alt_image = isset($logo_img['id']) ? get_post_meta($logo_img['id'], '_wp_attachment_image_alt', true) : "";
      $mobilealt_image = isset($logo_transparent_img['id']) ? get_post_meta($logo_transparent_img['id'], '_wp_attachment_image_alt', true) : "";

      $logoContent='<a href="'.esc_url(home_url('/')).'" style="'.$logo_width_style.'"><img id="logomenu" src="'.esc_url(grade_maybe_ssl_url($logo)).'" alt="'.esc_attr($alt_image).'" class="img-responsive halfsize" '.(get_grade_option('logo-width') ? " width=\"".(int)get_grade_option('logo-width')."\"":"").'><img id="logomenureveal" src="'.esc_url($logo_transparent).'" alt="'.esc_attr($mobilealt_image).'" class="img-responsive halfsize" '.(get_grade_option('logo-width') ? " width=\"".(int)get_grade_option('logo-width')."\"":"").'></a>';
    } else{
       $logoContent=get_grade_option('dt-logo-text') ?'<div class="header-logo><a class="navbar-brand-desktop" href="'.esc_url(home_url('/')).'">'.get_grade_option('dt-logo-text').'</a></div>':"";
    }

    if($logoContent!=''){
      $logoContent = '<li class="logo-desktop"'.(get_grade_option('logo-width')?" style=\"width:".(int)get_grade_option('logo-width')."px;\"":"").'>'.$logoContent.'</li>';
    }


    if(get_grade_option('show-header-shoppingcart') && grade_plugin_is_active('woocommerce/woocommerce.php')):

      if ( function_exists('WC') && sizeof( WC()->cart->get_cart() ) > 0 ) :

        $shoppingCart= '';
      else:
          $shoppingCart= '';
      endif;
    endif;
endif;

if(get_grade_option('show-header-searchmenu')):
    $searchContent= '';
endif;

$logoContent="";
$logoContentMobile="";

if(!empty($logo)){
  $alt_image = isset($logo_img['id']) ? get_post_meta($logo_img['id'], '_wp_attachment_image_alt', true) : "";
  $alt_image = empty($alt_image) ? get_bloginfo('name') : $alt_image;

  $mobilealt_image = "";
  $mobilealt_image = isset($logo_transparent_img['id']) ? get_post_meta($logo_transparent_img['id'], '_wp_attachment_image_alt', true) : "";

  $mobilealt_image = empty($mobilealt_image) ? get_bloginfo('name') : $mobilealt_image;

  $logoContent='<a href="'.esc_url(home_url('/')).'" style="" class="logolink">
  <img id="logomenudesktop" src="'.esc_url(grade_maybe_ssl_url($logo)).'" alt="'.esc_attr($alt_image).'" class="img-responsive halfsize" '.(get_grade_option('logo-width') ? " width=\"".(int)get_grade_option('logo-width')."\"":" width=\"auto\"").'>
  <img id="logomenurevealdesktop" src="'.esc_url(grade_maybe_ssl_url($logo_transparent)).'" alt="'.esc_attr($mobilealt_image).'" class="img-responsive halfsize" '.(get_grade_option('logo-width')?" width=\"".(int)get_grade_option('logo-width')."\"":" width=\"auto\"").'>
  <img id="logomenuoverlay" src="'.esc_url(grade_maybe_ssl_url($logo_overlay)).'" alt="'.esc_attr($mobilealt_image).'" class="img-responsive halfsize" '.(get_grade_option('logo-width')?" width=\"".(int)get_grade_option('logo-width')."\"":" width=\"auto\"").'></a>';

  $logoContentMobile='<a href="'.esc_url(home_url('/')).'" style="">
  <img id="logomenumobile" src="'.esc_url(grade_maybe_ssl_url($logo)).'" alt="'.esc_attr($alt_image).'" class="img-responsive halfsize" '.(get_grade_option('logo-width') ? " width=\"".(int)get_grade_option('logo-width')."\"":" width=\"auto\"").'>
  <img id="logomenurevealmobile" src="'.esc_url(grade_maybe_ssl_url($logo_transparent)).'" alt="'.esc_attr($mobilealt_image).'" class="img-responsive halfsize" '.(get_grade_option('logo-width')?" width=\"".(int)get_grade_option('logo-width')."\"":" width=\"auto\"").'></a>';

} else{
  $logoContent=get_grade_option('dt-logo-text') ? '<a class="navbar-brand-desktop" href="'.esc_url(home_url('/')).'">'.get_grade_option('dt-logo-text').'</a>':"";
}

  switch ($headerType) {
      case 'left':
        $classmenu = 'dt-menu-right';
          break;
      case 'right' :
          $classmenu = 'dt-menu-left';
          break;
      case 'leftbar' :
          $classmenu = 'dt-menu-leftbar';
          break;
      case 'pagebar' :
          $classmenu = 'dt-menu-pagebar';
          break;
      case 'middle' :
          $classmenu = 'dt-menu-middle';
          break;
      case "sidemenuoverlay":
          $classmenu = 'dt-menu-sidemenuoverlay';
          break;
      case "pushmenuoverlay":
          $classmenu = 'dt-menu-pushmenuoverlay';
          break;
      case "stackmenuoverlay":
          $classmenu = 'dt-menu-stackmenuoverlay';
          break;
      case "centermenuoverlay":
          $classmenu = 'dt-menu-centermenuoverlay';
          break;
      case 'leftvc' :
          $classmenu = 'dt-menu-leftvc';
          $items_wrap = null;
          break;
      default:
          $classmenu = 'dt-menu-center';
  }

  $menuParams = array(
        'theme_location' => 'primary',
        'echo' => false,
        'container_class'=>$classmenu,
        'container_id'=>'dt-menu',
        'menu_class'=>'',
        'container'=>'div',
        'before' => '',
        'after' => '',
        'fallback_cb'=>false,
        'nav_menu_item_id'=>'main-menu-item-',
        'walker'  => new grade_mainmenu_walker()
  );

  switch ($headerType) {
      case 'leftvc' :
          $menuParams['items_wrap'] = '<label for="main-nav-check" class="toggle" onclick="" title="'.esc_attr__('Close','grade').'"><i class="icon-cancel-1"></i></label><ul id="%1$s" class="%2$s">%3$s'.$shoppingCart.$searchContent.'</ul><label class="toggle close-all" onclick="uncheckboxes(&#39;nav&#39;)"><i class="icon-cancel-1"></i></label>';
          break;
      default:
          $menuParams['items_wrap'] = '<label for="main-nav-check" class="toggle" onclick="" title="'.esc_attr__('Close','grade').'"><i class="icon-cancel-1"></i></label>'.$logoContent.'<ul id="%1$s" class="%2$s">%3$s'.$searchContent.$shoppingCart.'</ul><label class="toggle close-all" onclick="uncheckboxes(&#39;nav&#39;)"><i class="icon-cancel-1"></i></label>';
  }

  $menu = wp_nav_menu($menuParams);
?>

<?php       if (!empty($menu)) :

              switch ($headerType) {
                case 'leftvc': //switch ($headerType)
?>
                <div class="container flex-container nopadding <?php echo get_grade_option('dt-sticky-menu') ? 'stickyonscrollup': ''; ?>">
                    <div class="flex-item col-sm-12 col-md-3 nopadding logo-container logo_bgcolor"><?php print grade_get_logo_content(); ?></div>
                    <div class="flex-item col-sm-12 col-md-9 nopadding">
                        <div class="col-xs-12 nav_buttons nav_buttons_bgcolor"><?php do_action('detheme_load_vc_nav_buttons'); ?></div>
                        <div class="col-xs-12 nav_bgcolor nopadding">
<?php                     print $menu; ?>
                        </div>
                    </div>
                </div>

                <div class="nav_mobile nav_bgcolor hidden-sm-max">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div id="mobile-header">
                                    <label for="main-nav-check" class="toggle" onclick="" title="<?php esc_html_e('Menu','grade');?>"><i class="icon-menu"></i></label>
                                    <?php echo $logoContentMobile ?>
                                </div><!-- closing "#header" -->
                            </div>
                        </div>
                    </div>
                </div>

<?php
                  break;
                default: //switch ($headerType)
?>
                <div class="container <?php echo get_grade_option('dt-sticky-menu') ? 'stickyonscrollup': ''; ?> menu_background_color">
<?php             print $menu; ?>
                  <div id="mobile-header" class="hidden-sm-max col-sm-12">
                    <label for="main-nav-check" class="toggle" onclick="" title="Menu"><i class="menu-icon"></i></label>
                    <?php echo $logoContentMobile ?>
                  </div><!-- closing "#mobile-header" -->
                </div><!-- closing ".container" -->
<?php
              } //switch ($headerType)

            else : ?>
              <div class="container <?php echo get_grade_option('dt-sticky-menu') ? 'stickyonscrollup': ''; ?> menu_background_color">
                <div id="dt-nomenu" class="<?php echo sanitize_html_class($menuParams['container_class']); ?>">
<?php
              $logoContent="";
              $logoContentMobile="";

              if(!empty($logo)){
                $logoContent='<a href="'.esc_url(home_url('/')).'" style="" class="logolink visible-md visible-lg">
                    <img id="logomenudesktop" src="'.esc_url(grade_maybe_ssl_url($logo)).'" alt="'.esc_attr($alt_image).'" class="img-responsive halfsize">
                    <img id="logomenurevealdesktop" src="'.esc_url(grade_maybe_ssl_url($logo_transparent)).'" alt="'.esc_attr($mobilealt_image).'" class="img-responsive halfsize">
                    <img id="logomenuoverlay" src="'.esc_url(grade_maybe_ssl_url($logo_overlay)).'" alt="'.esc_attr($mobilealt_image).'" class="img-responsive halfsize"></a>';

                $logoContentMobile='<a href="'.esc_url(home_url('/')).'" style="">
                    <img id="logomenumobile" src="'.esc_url(grade_maybe_ssl_url($logo)).'" alt="'.esc_attr($alt_image).'" class="img-responsive halfsize">
                    <img id="logomenurevealmobile" src="'.esc_url(grade_maybe_ssl_url($logo_transparent)).'" alt="'.esc_attr($mobilealt_image).'" class="img-responsive halfsize"></a>';
              }

                echo $logoContent;
?>
                <div id="mobile-header" class="hidden-sm-max col-sm-12">
<?php
                  echo $logoContentMobile
?>
                </div><!-- closing "#header" -->
                </div><!-- closing "#dt-nomenu" -->
              </div><!-- closing ".container" -->
<?php       endif;  ?>
</div> <!-- #head-page -->
<!--- mobile menu -->
<?php
    if ($headerType=='leftvc') :

        if ($menu):


        if(get_grade_option('show-header-shoppingcart') && grade_plugin_is_active('woocommerce/woocommerce.php')):

              if ( function_exists('WC') && sizeof( WC()->cart->get_cart() ) > 0 ) :

                $shoppingCart= '<li id="menu-item-999998" class="hidden-mobile bag menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-999998">
                          <a href="#">
                            <span><i class="icon-cart"></i> <span class="item_count">'. WC()->cart->get_cart_contents_count() . '</span></span>
                          </a>
                          <ul id="fof-sub-999998" class="sub-nav">
                            <li>
                              <!-- popup -->
                              <div class="cart-popup"><div class="widget_shopping_cart_content"></div></div>
                              <!-- end popup -->
                            </li>
                         </ul>
                        </li>';
              else:
                  $shoppingCart= '<li id="menu-item-999998" class="hidden-mobile bag menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-999998">
                            <a href="#">
                              <span><i class="icon-cart"></i> <span class="item_count">0</span></span>
                            </a>
                            <ul id="fof-sub-999998" class="sub-nav">
                                <li>
                                <!-- popup -->
                                <div class="cart-popup"><div class="widget_shopping_cart_content"></div></div>
                                <!-- end popup -->
                              </li>
                            </ul>
                          </li>';
              endif;
            endif;

            $menuParams['container_class'] = $classmenu . ' dt-menu-mobile';
            $menuParams['container_id'] = 'dt-menu-mobile';
            $menuParams['items_wrap'] = '<label for="main-nav-check" class="toggle" onclick="" title="'.esc_attr__('Close','grade').'"><i class="icon-cancel-1"></i></label><ul id="%1$s" class="%2$s">%3$s'.$searchContent.$shoppingCart.'</ul><label class="toggle close-all" onclick="uncheckboxes(&#39;nav&#39;)"><i class="icon-cancel-1"></i></label>';

            $menuParams['nav_menu_item_id']='mobile-menu-item-';


            $menu=wp_nav_menu($menuParams);
            print $menu;
        endif;
    endif;
?>
