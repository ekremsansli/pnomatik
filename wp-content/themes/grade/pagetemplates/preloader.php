<?php 
defined('ABSPATH') or die();
/**
 * pre-loader
 *
 *
 * @package WordPress
 * @subpackage Grade
 * @since Grade 1.0
 */

if(get_grade_option('page_loader')):?>
<div class="modal_preloader">
	<div class="fond">
	  <div class="contener_general">
	      <div class="contener_mixte"><div class="ballcolor ball_1">&nbsp;</div></div>
	      <div class="contener_mixte"><div class="ballcolor ball_2">&nbsp;</div></div>
	      <div class="contener_mixte"><div class="ballcolor ball_3">&nbsp;</div></div>
	      <div class="contener_mixte"><div class="ballcolor ball_4">&nbsp;</div></div>
	  </div>
	</div>
</div>	
<?php endif;?>
