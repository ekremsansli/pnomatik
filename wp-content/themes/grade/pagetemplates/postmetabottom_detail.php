<?php
defined('ABSPATH') or die();
?>
					<div class="postmetabottom">
						<div class="row">
							<?php if(is_rtl()):?>
							<div class="col-xs-12 text-left blog_info_share">
								<?php get_template_part('pagetemplates/social','share'); ?>
							</div>
							<?php else:?>
							<div class="col-xs-12 text-right blog_info_share">
								<?php get_template_part('pagetemplates/social','share'); ?>
							</div>
							<?php endif;?>
						</div>

					</div>


