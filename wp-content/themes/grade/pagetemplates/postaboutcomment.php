<?php
/**
 * @package WordPress
 * @subpackage Grade
 * @since Grade 1.0
 */
defined('ABSPATH') or die();

if (get_grade_option('show-author') && get_the_author_meta( 'description' )!='') :
?>
							<div class="about-author">
								<div class="media">
									<div class="pull-<?php print is_rtl()?"right":"left";?> text-center">
										<?php 
											$avatar_url = grade_get_avatar_url(get_avatar( get_the_author_meta( 'ID' ), 130 )); 
											if (isset($avatar_url)) {
										?>					
										<img src="<?php echo esc_url($avatar_url); ?>" class="author-avatar img-responsive img-circle" alt="<?php echo esc_attr(get_the_author_meta( 'nickname' )); ?>">
										<?php 
											} 
										?>											
									</div>
									<div class="media-body">
										<h4><?php echo sprintf(esc_html__('About %s','grade'),get_the_author_meta( 'nickname' )); ?></h4>
										<?php echo get_the_author_meta( 'description' ); ?>
									</div>
								</div>
							</div>

<?php 
endif; 

if(comments_open()):?>
	<div class="comment-count">
		<h3><?php comments_number(esc_html__('No Comments','grade'),esc_html__('1 Comment','grade'),esc_html__('% Comments','grade')); ?></h3>
	</div>

	<div class="section-comment">
		<?php comments_template('/comments.php', true); ?>
	</div><!-- Section Comment -->
<?php endif;?>
