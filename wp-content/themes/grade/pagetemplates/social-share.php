<?php defined('ABSPATH') or die();?>
<?php if(is_rtl()):?>
  <a href="<?php print esc_url('https://plus.google.com/share');?>" data-url="<?php echo esc_url(get_permalink()); ?>"><span class="post-share icon-social-google109"></span></a>
  <a href="<?php print esc_url('https://twitter.com/intent/tweet');?>" data-url="<?php echo esc_url(get_permalink()); ?>"><span class="post-share icon-social-twitter35"></span></a>
  <a href="<?php print esc_url('https://www.facebook.com/sharer/sharer.php');?>" data-url="<?php echo esc_url(get_permalink()); ?>"><span class="post-share icon-social-facebook43"></span></a>
  <span class="share-label"><?php esc_html_e('Share','grade');?></span>
<?php else:?>
  <span class="share-label"><?php esc_html_e('Share','grade');?></span>
  <a href="<?php print esc_url('https://www.facebook.com/sharer/sharer.php');?>" data-url="<?php echo esc_url(get_permalink()); ?>"><span class="post-share icon-social-facebook43"></span></a>
  <a href="<?php print esc_url('https://twitter.com/intent/tweet');?>" data-url="<?php echo esc_url(get_permalink()); ?>"><span class="post-share icon-social-twitter35"></span></a>
  <a href="<?php print esc_url('https://plus.google.com/share');?>" data-url="<?php echo esc_url(get_permalink()); ?>"><span class="post-share icon-social-google109"></span></a>
<?php endif;?>