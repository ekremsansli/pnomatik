<?php
defined('ABSPATH') or die();
/**
 *
 * @package WordPress
 * @subpackage Grade
 * @since Grade 1.0
 */

$menu=get_grade_option('dt-right-top-bar-menu');

if(!empty($menu)):

	$menuParams=array(
            	'menu'=>$menu,
            	'echo' => false,
                  'container_id'=>'dt-topbar-menu-right',
            	'menu_class'=>'topbar-menu',
            	'container'=>'div',
			'before' => '',
            	'after' => '',
                  'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul></ul>',
            	'fallback_cb'=>false,
                  'theme_location'=>'right-menu',
                  'walker'  => new grade_topbarmenuright_walker()
			);

      $menuParamsNoSubmenu=array(
                  'menu'=>$menu,
                  'echo' => false,
                  'container_id'=>'dt-topbar-menu-nosub',
                  'menu_class'=>'nav navbar-nav topbar-menu-nosub',
                  'container'=>'div',
                  'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul></ul>',
                  'before' => '',
                  'after' => '',
                  'theme_location'=>'right-menu',
                  'fallback_cb'=>false
                  );

	$menu=wp_nav_menu($menuParams);

      $found = preg_match_all('/menu\-item\-has\-children/s', $menu, $matches);
      // if one of menu items has children
      if ($found>0) {
?>
      <div class="right-menu">
            <input type="checkbox" name="nav-top-right" id="main-nav-check-top-right">
            <?php print ($menu)?$menu:"";?>
            <div id="mobile-header-top-right" class="visible-sm-min visible-xs">
                  <label for="main-nav-check-top-right" class="toggle" onclick="" title="<?php echo esc_attr__('Menu','grade');?>"><i class="icon-menu"></i></label>
            </div><!-- closing "#header" -->
      </div>
<?php
      } else {
            $menu=wp_nav_menu($menuParamsNoSubmenu);
?>
      <div class="right-menu"><?php print ($menu)?$menu:"";?></div>
<?php
      }
	
?>

<?php endif;?>
