<?php
defined('ABSPATH') or die();
/**
 * sticky sidebar
 *
 *
 * @package WordPress
 * @subpackage Grade
 * @since Grade 1.0
 */

if (get_grade_option('dt_scrollingsidebar_on')) :
	$dt_scrollingsidebar_margin = intval(get_grade_option('dt_scrollingsidebar_margin',0));
	$dt_scrollingsidebar_top_margin = intval(get_grade_option('dt_scrollingsidebar_top_margin',200));
	$dt_scrollingsidebar_position = get_grade_option('dt_scrollingsidebar_position','right');
	$dt_scrollingsidebar_bg_color = get_grade_option('dt_scrollingsidebar_bg_type') ? get_grade_option('dt_scrollingsidebar_bg_color','#ecf0f1') : 'transparent';
?>
<div id="floatMenu" data-top-margin="<?php echo esc_attr($dt_scrollingsidebar_top_margin);?>" data-bg-color="<?php echo esc_attr($dt_scrollingsidebar_bg_color);?>" data-position="<?php echo esc_attr($dt_scrollingsidebar_position); ?>" data-margin="<?php echo esc_attr($dt_scrollingsidebar_margin); ?>">
  <?php 
    dynamic_sidebar('detheme-scrolling-sidebar');
    do_action('detheme-scrolling-sidebar');
  ?>
</div>
<?php endif; ?>