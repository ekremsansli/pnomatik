<?php
defined('ABSPATH') or die();
?>
						<ul class="author_date_tags">
							<li class="blog_info_author"><?php the_author_link(); ?></li>
						<?php 
							$date = get_the_date();
							if (!empty($date)) :
								$title=get_the_title();
								$date = (!empty($title)) ? $date : '<a href="'.esc_url(get_permalink()).'">'.$date.'</a>';
						?>
							<li class="blog_info_date"><?php print $date;?></li>
						<?php
							endif;
						?>
							
						<?php 
							$tags = get_the_tag_list(' ',', ','');
							if (!empty($tags)) :
						?>
							<li class="blog_info_tags"><?php echo $tags; ?></li>
						<?php
							endif;
						?>

						</ul>
