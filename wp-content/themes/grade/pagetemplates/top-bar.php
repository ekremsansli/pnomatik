<?php
defined('ABSPATH') or die();
/**
 * top bar section
 *
 * @package WordPress
 * @subpackage Grade
 * @since Grade 1.0
 */

ob_start();
get_template_part( 'pagetemplates/topbar', "left".get_grade_option('dt-left-top-bar','text'));
$left_topbar=ob_get_clean();

ob_start();
get_template_part( 'pagetemplates/topbar', "right".get_grade_option('dt-right-top-bar','text'));
$right_topbar=ob_get_clean();

if(!empty($left_topbar) || !empty($right_topbar)) :
?>
<div id="top-bar" class="<?php echo get_grade_option('dt-header-type') == "leftbar" ? "vertical_menu_container":""; ?>">
	<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<?php print $left_topbar;?>
			<?php print $right_topbar;?>
		</div>
	</div>
</div>
</div>
<?php endif;?>