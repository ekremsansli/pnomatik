<?php
defined('ABSPATH') or die();
/**
 *
 * @package WordPress
 * @subpackage Grade
 * @since Grade 1.0
 */

$text=function_exists('icl_t') ? icl_t('hnd', 'right-top-bar-text', get_grade_option('dt-right-top-bar-text','')):get_grade_option('dt-right-top-bar-text','');
if(!empty($text)):
?>
<div class="right-menu"><div class="topbar-text"><?php print (!empty($text))? do_shortcode($text):"";?></div></div>
<?php endif;?>