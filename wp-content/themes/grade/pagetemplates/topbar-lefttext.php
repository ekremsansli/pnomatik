<?php
defined('ABSPATH') or die();
/**
 *
 * @package WordPress
 * @subpackage Grade
 * @since Grade 1.0
 */

$text=function_exists('icl_t') ? icl_t('grade', 'left-top-bar-text', get_grade_option('dt-left-top-bar-text','')):get_grade_option('dt-left-top-bar-text','');
if(!empty($text)):
?>
<div class="left-menu"><div class="topbar-text"><?php print (!empty($text))? do_shortcode($text):"";?></div></div>
<?php endif;?>
