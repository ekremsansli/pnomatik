<?php
defined('ABSPATH') or die();
/**
 * The template for displaying Search page
 *
 * @package WordPress
 * @subpackage Grade
 * @since Grade 1.0
 */

get_header();

get_template_part('pagetemplates/scrollingsidebar');


$sidebar_position=get_grade_sidebar_position();
$sidebar= $sidebar_position=='fullwidth' ? "fullwidth": (is_active_sidebar( 'detheme-sidebar' )?'detheme-sidebar':false);

if(!$sidebar){
	$sidebar_position = "nosidebar";
}

set_query_var('sidebar',$sidebar);
?>

<div class="content">
<div class="<?php echo sanitize_html_class($sidebar_position);?>">
	<div class="container">
		<div class="row">
<?php if ($sidebar_position=='nosidebar') { ?>
			<div class="col-sm-12">
<?php	} else { ?>
			<div class="col-sm-8 <?php print ($sidebar_position=='sidebar-left')?" col-sm-push-4":"";?> col-md-9 <?php print ($sidebar_position=='sidebar-left')?" col-md-push-3":"";?>">
<?php	} ?>
<div class="row">
	<div class="col-sm-12">

	<form method="get" class="search-form-onpage" action="<?php print esc_url( home_url( '/' ) );?>">
	        <div class="form-group">
	          <input type="search" class="form-control search-field" placeholder="<?php esc_attr_e('search','grade');?>" value="<?php print get_search_query();?>" name="s" title="<?php print esc_attr_x( 'Search for:', 'label','grade' );?>" />
	          <i class="icon-search-6"></i>
	        </div>
	        <input type="hidden" class="searchsubmit" value="<?php print esc_attr_x( 'Search', 'submit button', 'grade' );?>" />
	 </form>

	</div>
</div>
<?php
				if ( have_posts() ) :

				?>
			<div class="search-result-container">
				<header class="search-header">
				<h2 class="search-title"><?php printf( esc_html__( 'About %d result found for %s', 'grade' ), $wp_query->found_posts, "<span class=\"search-keyword\">\"".get_search_query()."\"</span>" ); ?></h2>
				</header>
<?php				
						while ( have_posts() ) : the_post();

						/*
						 * Include the post type template for search result. If you want to
						 * use this in a child theme, then include a file called called content-search-___.php
						 * (where ___ is the post type) and that will be used instead.
						 */?>
						 <div class="row">
						 	<div class="col-xs-12">
						 <?php
							get_template_part( 'content-search', 'post'==get_post_type()?get_post_format():get_post_type());
					?>
							</div>
						</div>
					<?php
					endwhile;
					?>
			
<?php
				else :?>
			<div class="search-result-container no-result">
				<header class="search-header">
				<h2 class="search-title"><?php esc_html_e( 'No result have been found for your search query', 'grade' ); ?></h2>
				</header>
<?php
					esc_html_e('Please try again or navigate another page','grade');
				endif;
?>
			</div>
				<!-- Pagination -->
				<div class="row">
					<?php get_template_part('pagetemplates/pagination'); ?>
				</div>
				<!-- /Pagination -->
				
			</div>


<?php if ('sidebar-right'==$sidebar_position) { ?>
			<div class="col-sm-4 col-md-3 sidebar">
				<?php get_sidebar(); ?>
			</div>
<?php }
	elseif ($sidebar_position=='sidebar-left') { ?>
			<div class="col-sm-4 col-md-3 sidebar col-sm-pull-8 col-md-pull-9">
				<?php get_sidebar(); ?>
			</div>
<?php }?>
		
		</div>			
	</div>
</div>
</div>
<?php
get_footer();
?>