<?php
defined('ABSPATH') or die();
/**
 *
 * @package WordPress
 * @subpackage Grade
 * @since Grade 1.0
 */

$footertext=function_exists('icl_t') ? icl_t('grade', 'footer-text', get_grade_option('footer-text','')):get_grade_option('footer-text','');


?>
<footer id="footer" class="tertier_color_bg <?php print get_grade_option('dt-header-type')=='leftbar' ? " vertical_menu_container":"";?>">
<div class="container footer-section">
		<?php if((!empty($footertext) || strlen(strip_tags($footertext)) > 1) && get_grade_option('showfooterwidget') && is_active_sidebar( 'detheme-bottom' )){?> 
		<div class="col-md-9<?php print get_grade_option('dt-footer-position')=='right' ? "":" col-md-push-3";?> col-sm-12 col-xs-12 footer-right">
			<div id="footer-right">
				<?php 
				dynamic_sidebar('detheme-bottom');
				do_action('dynamic_sidebar_detheme-bottom');
				 ?>
			</div>
		</div>			
		<div class="col-md-3<?php print get_grade_option('dt-footer-position')=='right' ? "":" col-md-pull-9";?> col-sm-12 col-xs-12 footer-left">
			<div id="footer-left">
				<?php echo do_shortcode($footertext); ?>
			</div>
		</div>
		<?php }
		elseif((!empty($footertext) || strlen(strip_tags($footertext)) > 1) && (!get_grade_option('showfooterwidget') || !is_active_sidebar( 'detheme-bottom' )))
		{
		?> 
		<div class="col-md-12 footer-left equal-height">
			<div id="footer-left">
				<?php echo do_shortcode($footertext); ?>
			</div>
		</div>	
		<?php 
		}
		elseif(get_grade_option('showfooterwidget') && is_active_sidebar( 'detheme-bottom' )){
			?>
		<div class="col-md-12 footer-right equal-height">
			<div id="footer-right">
				<?php dynamic_sidebar('detheme-bottom');
				do_action('dynamic_sidebar_detheme-bottom');
				 ?>
			</div>
		</div>	
		<?php
		 }
		?>
</div>
</footer>