(function() {
  "use strict";
  var $=jQuery.noConflict();
    $('select:not(.no-select,#rating)').each(function(i,el) {
      new Select({
        el: el,
        alignToHighlighted: 'always'
      });
    });
}).call(this);
