<?php
defined('ABSPATH') or die();
/**
 *
 * Used for display blog masonry.
 *
 * @package WordPress
 * @subpackage Grade
 * @since Grade 1.0
 */
?>

<?php 
	$imageurl = "";

	/* Get Image from featured image */
	if (isset($post->ID)) {
		$thumb_id = get_post_thumbnail_id($post->ID);
		$featured_image = wp_get_attachment_image_src($thumb_id,'full',false); 
		if (isset($featured_image[0])) {
			$imageurl = $featured_image[0];
		}

		$alt_image = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
	}
	
?>

<?php 
	if (!is_single()) : ?>

		<div class="container-fluid">
			<div class="row">
<?php
		if ($imageurl!="") {
?>		<div class="col-xs-12">	
			<a href="<?php echo esc_url(get_permalink()); ?>" title="<?php print esc_attr(get_the_title());?>"><img class="img-responsive blog_image" alt="<?php echo esc_attr($alt_image); ?>" src="<?php echo esc_url($imageurl); ?>"></a>
		</div>
<?php 	}  ?>											
				<div class="col-xs-12">
					<?php get_template_part('pagetemplates/ms_categories'); ?>
				</div>

				<div class="col-xs-12">
					<h2 class="blog-post-title"><a href="<?php echo esc_url(get_permalink()); ?>"><?php the_title();?></a></h2>
				</div>

				<div class="col-xs-12 blog_info_author">
					<?php esc_html_e('By ', 'grade'); ?><?php the_author_link(); ?>/<?php print get_the_date();?>
				</div>

				<div class="col-xs-12">
					<?php 
						print get_the_excerpt();
					?>
				</div>

				<?php if(is_rtl()):?>
					<div class="col-xs-6 text-left blog_info_share">
						<?php get_template_part('pagetemplates/ms_social_share'); ?>
					</div>
					<div class="col-xs-6 masonry_readmore">
						<a target="_self" class="btn btn-primary btn-readmore" href="<?php echo esc_url(get_permalink()); ?>"><?php esc_html_e('Read More','grade') ?></a>
					</div>
				<?php else:?>
					<div class="col-xs-6 masonry_readmore">
						<a target="_self" class="btn btn-primary btn-readmore" href="<?php echo esc_url(get_permalink()); ?>"><?php esc_html_e('Read More','grade') ?></a>
					</div>
					<div class="col-xs-6 text-right blog_info_share">
						<?php get_template_part('pagetemplates/ms_social_share'); ?>
					</div>
				<?php endif;?>

			</div>
		</div>
<?php 
	endif; 
?>
