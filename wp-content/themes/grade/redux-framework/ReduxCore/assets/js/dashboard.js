jQuery(document).ready(function($){
	'use strict';

  var 
  $select = $('select#dt-left-top-bar-select'),
  $menusource = $('#grade_config-dt-left-top-bar-menu').closest('.form-table tr'),
  $textsource = $('#grade_config-dt-left-top-bar-text').closest('.form-table tr'),
  $devider1 = $('#grade_config-devider-1').closest('.form-table tr'),
  $devider2 = $('#grade_config-devider-2').closest('.form-table tr'),
  $rselect = $('select#dt-right-top-bar-select'),
  $rmenusource = $('#grade_config-dt-right-top-bar-menu').closest('.form-table tr'),
  $rtextsource = $('#grade_config-dt-right-top-bar-text').closest('.form-table tr'),
  $showtopbar=$('#grade_config-showtopbar .cb-enable,#grade_config-showtopbar .cb-disable'),
  $backgroundImage = $('#grade_config-dt-banner-image').closest('.form-table tr'),
  $backgroundVideo = $('#grade_config-dt-banner-video').closest('.form-table tr'),
  $backgroundColor = $('#grade_config-banner-color').closest('.form-table tr'),
  $background=$('select#dt-show-banner-page-select'),
  $homebackground=$('#grade_config-homepage-background-type .cb-enable,#grade_config-homepage-background-type .cb-disable'),
  $homebackgroundColor = $('#grade_config-homepage-header-color').closest('.form-table tr'),
  $scrollsidebgcolor = $('#grade_config-dt_scrollingsidebar_bg_color').closest('.form-table tr'),
  $pagebackground=$('#grade_config-header-background-type .cb-enable,#grade_config-header-background-type .cb-disable'),
  $pagebackgroundColor = $('#grade_config-header-color').closest('.form-table tr'),
  $shopbackgroundColor = $('#grade_config-dt-shop-banner-image').closest('.form-table tr'),
  $showfooterwidget=$('#grade_config-showfooterwidget .cb-enable,#grade_config-showfooterwidget .cb-disable'),
  $footerwidget=$('#grade_config-dt-footer-widget-column').closest('.form-table tr'),
  $showfooterpage=$('#grade_config-showfooterpage .cb-enable,#grade_config-showfooterpage .cb-disable'),
  $footerpage=$('#grade_config-footerpage').closest('.form-table tr'),
  $footertext=$('#grade_config-footer-text').closest('.form-table tr'),
  $footertextposition=$('#grade_config-dt-footer-position').closest('.form-table tr'),
  $footerwidget=$('#grade_config-dt-footer-widget-column').closest('.form-table tr'),
  $showfooterwidgetcountainer=$('#grade_config-showfooterwidget').closest('.form-table tr'),
  $footercolor=$('#grade_config-footer-color').closest('.form-table tr'),
  $footerfontcolor=$('#grade_config-footer-font-color').closest('.form-table tr'),
  $showfooterarea=$('#grade_config-showfooterarea .cb-enable,#grade_config-showfooterarea .cb-disable'),
  $postfooterpage=$('#grade_config-postfooterpage').closest('.form-table tr');

  var $page_loader =$('#grade_config-page_loader .cb-enable,#grade_config-page_loader .cb-disable'),
  $page_loader_background=$('#grade_config-page_loader_background').closest('.form-table tr'),
  $page_loader_ball_1=$('#grade_config-page_loader_ball_1').closest('.form-table tr'),
  $page_loader_ball_2=$('#grade_config-page_loader_ball_2').closest('.form-table tr'),
  $page_loader_ball_3=$('#grade_config-page_loader_ball_3').closest('.form-table tr'),
  $page_loader_ball_4=$('#grade_config-page_loader_ball_4').closest('.form-table tr');

  var $scrollingsidebar =$('#grade_config-dt_scrollingsidebar_on .cb-enable,#grade_config-dt_scrollingsidebar_on .cb-disable'),
  $scrollsidebgtype = $('#grade_config-dt_scrollingsidebar_bg_type .cb-enable,#grade_config-dt_scrollingsidebar_bg_type .cb-disable'),
  $scrollingsidebar_bg_type=$('#grade_config-dt_scrollingsidebar_bg_type').closest('.form-table tr'),
  $scrollingsidebar_bg_color=$('#grade_config-dt_scrollingsidebar_bg_color').closest('.form-table tr'),
  $scrollingsidebar_top_margin=$('#grade_config-dt_scrollingsidebar_top_margin').closest('.form-table tr'),
  $scrollingsidebar_position=$('#grade_config-dt_scrollingsidebar_position').closest('.form-table tr'),
  $scrollingsidebar_margin=$('#grade_config-dt_scrollingsidebar_margin').closest('.form-table tr');

  var $boxed_layout =$('#grade_config-boxed_layout_activate .cb-enable,#grade_config-boxed_layout_activate .cb-disable'),
  $boxed_layout_boxed_background_image=$('#grade_config-boxed_layout_boxed_background_image').closest('.form-table tr'),
  $boxed_layout_boxed_background_color=$('#grade_config-boxed_layout_boxed_background_color').closest('.form-table tr'),
  $boxed_layout_stretched=$('#grade_config-boxed_layout_stretched').closest('.form-table tr');

  var $dtshowheader =$('#grade_config-dt-show-header .cb-enable,#grade_config-dt-show-header .cb-disable'),
  $dtshowheader_child=$('#grade_config-dt-show-header').closest('.form-table tr');

  var $showbannerarea =$('#grade_config-show-banner-area .cb-enable,#grade_config-show-banner-area .cb-disable'),
  $showbannerarea_child=$('#grade_config-show-banner-area').closest('.form-table tr');


  var $headerlayout=$('input[name="grade_config[dt-header-type]"]'),
  $bgmenuimage=$('#grade_config-dt-menu-image').closest('.form-table tr'),
  $bgmenuimagehorizontal=$('#grade_config-dt-menu-image-horizontal').closest('.form-table tr'),
  $bgmenuimagesize=$('#grade_config-dt-menu-image-size').closest('.form-table tr'),
  $bgmenuimagevertical=$('#grade_config-dt-menu-image-vertical').closest('.form-table tr'),
  $stickylogo=$('#grade_config-dt-logo-image-transparent').closest('.form-table tr'),
  $homestickylogo=$('#grade_config-homepage-dt-logo-image-transparent').closest('.form-table tr'),
  $stickylogomargin=$('#grade_config-dt-logo-top-margin-reveal').closest('.form-table tr'),
  $headertype=$('input[name="grade_config[dt-header-type]"]'),
  $logotoppadding=$('#grade_config-dt-logo-top-padding').closest('.form-table tr');

  var $pagebackgroundsticky=$('#grade_config-header-background-transparent-active .cb-enable,#grade_config-header-background-transparent-active .cb-disable'),
  $pagebackgroundstickyColor = $('#grade_config-header-color-transparent').closest('.form-table tr'),
  $pagebackgroundstickyrow = $('#grade_config-header-background-transparent-active').closest('.form-table tr'),
  $homebackgroundsticky=$('#grade_config-homepage-header-color-transparent-active .cb-enable,#grade_config-homepage-header-color-transparent-active .cb-disable'),
  $homebackgroundstickyColor = $('#grade_config-homepage-header-color-transparent').closest('.form-table tr'),
  $homebackgroundstickyrow = $('#grade_config-homepage-header-color-transparent-active').closest('.form-table tr');


  $headerlayout.live('change',function(){

    if ($('input[name="grade_config[dt-header-type]"]:checked').val()=="leftbar") {
      $bgmenuimage.fadeIn('slow');$bgmenuimagehorizontal.fadeIn('slow');$bgmenuimagevertical.fadeIn('slow');$bgmenuimagesize.fadeIn('slow');
    } else {
      $bgmenuimage.fadeOut('slow');$bgmenuimagehorizontal.fadeOut('slow');$bgmenuimagevertical.fadeOut('slow');$bgmenuimagesize.fadeOut('slow');
    }
  });


  /* sticky menu */
  var $stickymenu =$('#grade_config-dt-sticky-menu .cb-enable,#grade_config-dt-sticky-menu .cb-disable'),
  $headercolorsticky=$('#grade_config-header-color-sticky').closest('.form-table tr'),
  $headerfontcolorsticky=$('#grade_config-header-font-color-sticky').closest('.form-table tr'),
  $homeheadercolorsticky=$('#grade_config-homepage-header-color-sticky').closest('.form-table tr'),
  $homeheaderfontcolorsticky=$('#grade_config-homepage-header-font-color-sticky').closest('.form-table tr');


    $stickymenu.live('click',function(e){
    e.preventDefault();
    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){

        $stickylogo.fadeIn('fast');
        $homestickylogo.fadeIn('fast');
        $stickylogomargin.fadeIn('fast');
        $headercolorsticky.fadeIn('fast');
        $headerfontcolorsticky.fadeIn('fast');
        $pagebackgroundstickyrow.fadeIn('fast');
        $homeheadercolorsticky.fadeIn('fast');
        $homeheaderfontcolorsticky.fadeIn('fast');
        $homebackgroundstickyrow.fadeIn('fast');
        $pagebackgroundsticky.trigger('change');
        $homebackgroundsticky.trigger('change');
      }

    }else{
      if($(this).hasClass('selected')){
        $stickylogo.fadeOut('fast');
        $homestickylogo.fadeOut('fast');
        $stickylogomargin.fadeOut('fast');
        $headercolorsticky.fadeOut('fast');
        $headerfontcolorsticky.fadeOut('fast');
        $pagebackgroundstickyrow.fadeOut('fast');
        $homeheadercolorsticky.fadeOut('fast');
        $homeheaderfontcolorsticky.fadeOut('fast');
        $homebackgroundstickyrow.fadeOut('fast');
        $pagebackgroundstickyColor.fadeOut('fast');
        $homebackgroundstickyColor.fadeOut('fast');
      }
    }

  }).live('change',function(e){

    e.preventDefault();
    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){

       $stickylogo.fadeIn('fast');
       $homestickylogo.fadeIn('fast');
       $stickylogomargin.fadeIn('fast');
       $headercolorsticky.fadeIn('fast');
       $headerfontcolorsticky.fadeIn('fast');
       $pagebackgroundstickyrow.fadeIn('fast');
       $homeheadercolorsticky.fadeIn('fast');
       $homeheaderfontcolorsticky.fadeIn('fast');
       $homebackgroundstickyrow.fadeIn('fast');
       $pagebackgroundsticky.trigger('change');
       $homebackgroundsticky.trigger('change');
      }
    }else{


      if($(this).hasClass('selected')){
        $stickylogo.fadeOut('fast');
        $homestickylogo.fadeOut('fast');
        $stickylogomargin.fadeOut('fast');
        $headercolorsticky.fadeOut('fast');
        $headerfontcolorsticky.fadeOut('fast');
        $pagebackgroundstickyrow.fadeOut('fast');
        $homeheadercolorsticky.fadeOut('fast');
        $homeheaderfontcolorsticky.fadeOut('fast');
        $homebackgroundstickyrow.fadeOut('fast');
        $pagebackgroundstickyColor.fadeOut('fast');
        $homebackgroundstickyColor.fadeOut('fast');
      }
    }

  });

/* Auto update */

  var $disable_automatic_update =$('#grade_config-disable_automatic_update .cb-enable,#grade_config-disable_automatic_update .cb-disable'),
  $core_automatic_update=$('#grade_config-core_automatic_update').closest('.form-table tr');

    $disable_automatic_update.live('click',function(e){
    e.preventDefault();
    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){
        $core_automatic_update.fadeIn('fast');
      }

    }else{
      if($(this).hasClass('selected')){
        $core_automatic_update.fadeOut('fast');

      }
    }

  }).live('change',function(e){

    e.preventDefault();
    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){
       $core_automatic_update.fadeIn('fast');
      }
    }else{

      if($(this).hasClass('selected')){
        $core_automatic_update.fadeOut('fast');
      }
    }

  });


  $background.live('change',function(){

    var background = $(this).val();
    switch ( background ) {
      case 'image':
        $backgroundImage.fadeIn('slow');
        $backgroundColor.fadeOut('slow');
        $backgroundVideo.fadeOut('slow');
        break;
      case 'color':
        $backgroundColor.fadeIn('fast');
        $backgroundImage.fadeOut('slow');
        $backgroundVideo.fadeOut('slow');
        break;
      case 'video':
        $backgroundVideo.fadeIn('fast');
        $backgroundColor.fadeOut('slow');
        $backgroundImage.fadeOut('slow');
        break;
      default:
        $backgroundColor.fadeOut('slow');
        $backgroundImage.fadeIn('slow');
        $backgroundVideo.fadeOut('slow');
      }

  });

  /* menu type */

  var headertypevalue="";

  $headertype.live('change',function(e){

    if($(this).prop('checked')){
      headertypevalue=$(this).val();
    }
     if(headertypevalue=='center'){
      $logotoppadding.fadeIn('fast');
     }
     else{
      $logotoppadding.fadeOut('fast');
     }
  });


  /* Background Color */
  $pagebackground.live('click',function(e){

    e.preventDefault();

    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){
        $pagebackgroundColor.fadeIn('fast');
      }

    }else{
      if($(this).hasClass('selected')){
        $pagebackgroundColor.fadeOut('fast');
      }
    }
  }).
  live('change',function(e){
    e.preventDefault();

    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){
        $pagebackgroundColor.fadeIn('fast');
      }

    }else{
      if($(this).hasClass('selected')){
        $pagebackgroundColor.fadeOut('fast');
      }
    }
  });

  $pagebackgroundsticky.live('click',function(e){

    e.preventDefault();

    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){
        $pagebackgroundstickyColor.fadeIn('fast');
      }

    }else{
      if($(this).hasClass('selected')){
        $pagebackgroundstickyColor.fadeOut('fast');
      }
    }
  }).
  live('change',function(e){
    e.preventDefault();

    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){
        $pagebackgroundstickyColor.fadeIn('fast');
      }

    }else{
      if($(this).hasClass('selected')){
        $pagebackgroundstickyColor.fadeOut('fast');
      }
    }
  });


  $homebackground.live('click',function(e){

    e.preventDefault();

    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){
        $homebackgroundColor.fadeIn('fast');
      }

    }else{
      if($(this).hasClass('selected')){
        $homebackgroundColor.fadeOut('fast');
      }
    }
  }).
  live('change',function(e){
    e.preventDefault();

    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){
        $homebackgroundColor.fadeIn('fast');
      }

    }else{
      if($(this).hasClass('selected')){
        $homebackgroundColor.fadeOut('fast');
      }
    }
  });

  $homebackgroundsticky.live('click',function(e){

    e.preventDefault();

    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){
        $homebackgroundstickyColor.fadeIn('fast');
      }

    }else{
      if($(this).hasClass('selected')){
        $homebackgroundstickyColor.fadeOut('fast');
      }
    }
  }).
  live('change',function(e){
    e.preventDefault();
    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){
        $homebackgroundstickyColor.fadeIn('fast');
      }

    }else{
      if($(this).hasClass('selected')){
        $homebackgroundstickyColor.fadeOut('fast');
      }
    }
  });

  $scrollsidebgtype.live('click',function(e){

    e.preventDefault();

    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){
        $scrollingsidebar_bg_color.fadeIn('fast');
      }

    }else{
      if($(this).hasClass('selected')){
        $scrollingsidebar_bg_color.fadeOut('fast');
      }
    }
  }).
  live('change',function(e){
    e.preventDefault();

    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){
        $scrollingsidebar_bg_color.fadeIn('fast');
      }

    }else{
      if($(this).hasClass('selected')){
        $scrollingsidebar_bg_color.fadeOut('fast');
      }
    }
  });

 $showfooterwidget.live('click',function(e){


    e.preventDefault();
    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){
          $footerwidget.fadeIn('fast');
      }

    }else{
      if($(this).hasClass('selected')){

          $footerwidget.fadeOut('fast');
      }
    }

  }).live('change',function(e){
    e.preventDefault();
    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){
          $footerwidget.fadeIn('fast');
      }
    }else{
      if($(this).hasClass('selected')){

          $footerwidget.fadeOut('fast');
      }
    }

  });

  $showfooterpage.live('click',function(e){

    e.preventDefault();
    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){
          $footerpage.fadeIn('fast');$postfooterpage.fadeIn('fast');
      }

    }else{
      if($(this).hasClass('selected')){

          $footerpage.fadeOut('fast');$postfooterpage.fadeOut('fast');
      }
    }

  }).live('change',function(e){
    e.preventDefault();
    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){
          $footerpage.fadeIn('fast');$postfooterpage.fadeIn('fast');
      }
    }else{
      if($(this).hasClass('selected')){

          $footerpage.fadeOut('fast');$postfooterpage.fadeOut('fast');
      }
    }

  });

 $showfooterarea.live('click',function(e){
    e.preventDefault();
    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){
        $footertext.fadeIn('fast');
        $footerwidget.fadeIn('fast');
        $showfooterwidgetcountainer.fadeIn('fast');
        $footercolor.fadeIn('fast');
        $footerfontcolor.fadeIn('fast');  
        $footertextposition.fadeIn('fast');
      }

    }else{
      if($(this).hasClass('selected')){
        $footertext.fadeOut('fast');
        $footerwidget.fadeOut('fast');
        $showfooterwidgetcountainer.fadeOut('fast');
        $footercolor.fadeOut('fast');
        $footerfontcolor.fadeOut('fast');  
        $footertextposition.fadeOut('fast');
      }
    }

  }).live('change',function(e){
    e.preventDefault();
    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){
        $footertext.fadeIn('fast');
        $footerwidget.fadeIn('fast');
        $showfooterwidgetcountainer.fadeIn('fast');
        $footercolor.fadeIn('fast');
        $footerfontcolor.fadeIn('fast');  
        $footertextposition.fadeIn('fast');
      }
    }else{
      if($(this).hasClass('selected')){
        $footertext.fadeOut('fast');
        $footerwidget.fadeOut('fast');
        $showfooterwidgetcountainer.fadeOut('fast');
        $footercolor.fadeOut('fast');
        $footerfontcolor.fadeOut('fast');  
        $footertextposition.fadeOut('fast');
      }
    }

  });

 $page_loader.live('click',function(e){
    e.preventDefault();
    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){
        $page_loader_ball_1.fadeIn('fast');
        $page_loader_ball_2.fadeIn('fast');
        $page_loader_ball_3.fadeIn('fast');
        $page_loader_ball_4.fadeIn('fast');
        $page_loader_background.fadeIn('fast');
      }

    }else{
      if($(this).hasClass('selected')){
        $page_loader_ball_1.fadeOut('fast');
        $page_loader_ball_2.fadeOut('fast');
        $page_loader_ball_3.fadeOut('fast');
        $page_loader_ball_4.fadeOut('fast');
        $page_loader_background.fadeOut('fast');
      }
    }

  }).live('change',function(e){
    e.preventDefault();
    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){
        $page_loader_ball_1.fadeIn('fast');
        $page_loader_ball_2.fadeIn('fast');
        $page_loader_ball_3.fadeIn('fast');
        $page_loader_ball_4.fadeIn('fast');
        $page_loader_background.fadeIn('fast');
      }
    }else{
      if($(this).hasClass('selected')){
        $page_loader_ball_1.fadeOut('fast');
        $page_loader_ball_2.fadeOut('fast');
        $page_loader_ball_3.fadeOut('fast');
        $page_loader_ball_4.fadeOut('fast');
        $page_loader_background.fadeOut('fast');
      }
    }

  });

   $scrollingsidebar.live('click',function(e){
    e.preventDefault();
    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){
        $scrollingsidebar_bg_type.fadeIn('fast');
        $scrollingsidebar_bg_color.fadeIn('fast');
        $scrollingsidebar_top_margin.fadeIn('fast');
        $scrollingsidebar_position.fadeIn('fast');
        $scrollingsidebar_margin.fadeIn('fast');
        $scrollsidebgtype.trigger('change');
      }

    }else{
      if($(this).hasClass('selected')){
        $scrollingsidebar_bg_type.fadeOut('fast');
        $scrollingsidebar_bg_color.fadeOut('fast');
        $scrollingsidebar_top_margin.fadeOut('fast');
        $scrollingsidebar_position.fadeOut('fast');
        $scrollingsidebar_margin.fadeOut('fast');
      }
    }

  }).live('change',function(e){
    e.preventDefault();
    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){
        $scrollingsidebar_bg_type.fadeIn('fast');
        $scrollingsidebar_bg_color.fadeIn('fast');
        $scrollingsidebar_top_margin.fadeIn('fast');
        $scrollingsidebar_position.fadeIn('fast');
        $scrollingsidebar_margin.fadeIn('fast');
        $scrollsidebgtype.trigger('change');
      }
    }else{
      if($(this).hasClass('selected')){
        $scrollingsidebar_bg_type.fadeOut('fast');
        $scrollingsidebar_bg_color.fadeOut('fast');
        $scrollingsidebar_top_margin.fadeOut('fast');
        $scrollingsidebar_position.fadeOut('fast');
        $scrollingsidebar_margin.fadeOut('fast');
      }
    }

  });

   $boxed_layout.live('click',function(e){
    e.preventDefault();
    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){
        $boxed_layout_boxed_background_image.fadeIn('fast');
        $boxed_layout_boxed_background_color.fadeIn('fast');
        $boxed_layout_stretched.fadeIn('fast');
      }

    }else{
      if($(this).hasClass('selected')){
        $boxed_layout_boxed_background_image.fadeOut('fast');
        $boxed_layout_boxed_background_color.fadeOut('fast');
        $boxed_layout_stretched.fadeOut('fast');
      }
    }

  }).live('change',function(e){
    e.preventDefault();
    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){
        $boxed_layout_boxed_background_image.fadeIn('fast');
        $boxed_layout_boxed_background_color.fadeIn('fast');
        $boxed_layout_stretched.fadeIn('fast');
      }
    }else{
      if($(this).hasClass('selected')){
        $boxed_layout_boxed_background_image.fadeOut('fast');
        $boxed_layout_boxed_background_color.fadeOut('fast');
        $boxed_layout_stretched.fadeOut('fast');
      }
    }

  });

  $dtshowheader.live('click',function(e){
    e.preventDefault();
    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){
         $dtshowheader_child.siblings().fadeIn('fast');
         $homebackground.trigger('change');
         $pagebackground.trigger('change');
      }

    }else{
      if($(this).hasClass('selected')){
        $dtshowheader_child.siblings().fadeOut('fast');
      }
    }

  }).live('change',function(e){

    e.preventDefault();
    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){
        $dtshowheader_child.siblings().fadeIn('fast');
         $homebackground.trigger('change');
         $pagebackground.trigger('change');
      }
    }else{
      if($(this).hasClass('selected')){
        $dtshowheader_child.siblings().fadeOut('fast');
      }
    }

  });

  $showbannerarea.live('click',function(e){
    e.preventDefault();
    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){
        $showbannerarea_child.siblings().fadeIn('fast');
        $background.trigger('change');
      }

    }else{
      if($(this).hasClass('selected')){
        $showbannerarea_child.siblings().fadeOut('fast');
      }
    }

  }).live('change',function(e){

    e.preventDefault();
    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){
        $showbannerarea_child.siblings().fadeIn('fast');
        $background.trigger('change');
      }
    }else{
      if($(this).hasClass('selected')){
        $showbannerarea_child.siblings().fadeOut('fast');
      }
    }

  });

  $select.live('change',function(){

    var this_value = $(this).val();

    switch ( this_value ) {
      case 'text':
        $menusource.fadeOut('fast');
        $textsource.fadeIn('slow');
        break;
      case 'menu':
      case 'icon':
        $textsource.fadeOut('fast');
        $menusource.fadeIn('slow');
        break;
      default:
        $textsource.fadeOut('fast');
        $menusource.fadeOut('slow');
    }
   });

  $rselect.live('change',function(){

    var this_value = $(this).val();

    switch ( this_value ) {
      case 'text':
        $rmenusource.fadeOut('fast');
        $rtextsource.fadeIn('slow');
        break;
      case 'menu':
      case 'icon':
        $rtextsource.fadeOut('fast');
        $rmenusource.fadeIn('slow');
        break;
      default:
        $rtextsource.fadeOut('fast');
        $rmenusource.fadeOut('slow');
    }


   });

  $showtopbar.live('click',function(e){

    var $showtopbar_child=$('#grade_config-showtopbar').closest('.form-table tr');

    e.preventDefault();
    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){

        $showtopbar_child.siblings().fadeIn('fast');
        $select.trigger('change');
        $rselect.trigger('change');

      }

    }else{
      if($(this).hasClass('selected')){

        $showtopbar_child.siblings().fadeOut('fast');
        $devider1.fadeOut('fast');
        $devider2.fadeOut('fast');
      }
    }

  }).live('change',function(e){

    e.preventDefault();

    var $showtopbar_child=$('#grade_config-showtopbar').closest('.form-table tr');
    if($(this).hasClass('cb-enable')){
      if($(this).hasClass('selected')){

        $showtopbar_child.siblings().fadeIn('fast');
        $select.trigger('change');
        $rselect.trigger('change');

      }
    }else{
      if($(this).hasClass('selected')){

        $showtopbar_child.siblings().fadeOut('fast');
        $devider1.fadeOut('fast');
        $devider2.fadeOut('fast');
      }
    }

  });

  /* blog type */
  var $blog_type =$('select#blog_type-select'),
  $masonry_column=$('#grade_config-masonry_column').closest('.form-table tr'),
  $masonry_column_tablet=$('#grade_config-masonry_column_tablet').closest('.form-table tr'),
  $masonry_column_mobile=$('#grade_config-masonry_column_mobile').closest('.form-table tr');

  $blog_type.live('change',function(){

    var blog_type = $(this).val();

    if(blog_type=='masonry'){
      $masonry_column.fadeIn('fast');
      $masonry_column_tablet.fadeIn('fast');
      $masonry_column_mobile.fadeIn('fast');

    }
    else{
      $masonry_column.fadeOut('fast');
      $masonry_column_tablet.fadeOut('fast');
      $masonry_column_mobile.fadeOut('fast');

    }
  });

  $blog_type.trigger('change');


   $disable_automatic_update.trigger('change');
   $dtshowheader.trigger('change');
   $stickymenu.trigger('change');
   $showbannerarea.trigger('change');
   $showtopbar.trigger('change');
   $page_loader.trigger('change');
   $showfooterwidget.trigger('change');
   $scrollingsidebar.trigger('change');
   $boxed_layout.trigger('change');
   $showfooterpage.trigger('change');
   $showfooterarea.trigger('change');
   $headerlayout.trigger('change');
 });
