<?php
defined('ABSPATH') or die();

require_once( get_template_directory().'/lib/tgm/class-tgm-plugin-activation.php');
add_action( 'tgmpa_register', 'grade_register_required_plugins' );

if ( ! isset( $content_width ) ) $content_width = 2000;


function grade_register_required_plugins() {

	$plugins = array(

		array(
			'name'     				=> esc_html('WPBakery Visual Composer','grade'), 
			'slug'     				=> 'js_composer', 
			'source'   				=> get_template_directory() . '/plugins/js_composer_5.4.5.zip',
			'required' 				=> true, 
		),
		array(
			'name'     				=> esc_html('Grade Visual Composer Add On','grade'),
			'slug'     				=> 'grade_vc_addon', 
			'source'   				=> get_template_directory() . '/plugins/grade_vc_addon_1.0.1.zip',
			'required' 				=> false,
		),
		array(
			'name'     				=> esc_html('Contact Form 7','grade'),
			'slug'     				=> 'contact-form-7', 
			'required' 				=> false, 
		),
		array(
			'name'     				=> esc_html('WooCommerce - excelling eCommerce','grade'),
			'slug'     				=> 'woocommerce', 
			'required' 				=> false, 
		),
		array(
			'name'     				=> esc_html('Revolution Slider','grade'),
			'slug'     				=> 'revslider',
			'source'   				=> get_template_directory() . '/plugins/rev-slider_5.4.6.4.zip',
			'required' 				=> true, 
		),
		array(
			'name'     				=> esc_html('Grade Icon Font - Add-on','grade'),
			'slug'     				=> 'grade_theme_icons', 
			'source'   				=> get_template_directory() . '/plugins/grade_theme_icons_1.0.0.zip', 
			'required' 				=> true,
		),
		array(
			'name'     				=> esc_html('Detheme Megamenu Plugin','grade'),
			'slug'     				=> 'dt-megamenu', 
			'source'   				=> get_template_directory() . '/plugins/dt-megamenu.zip', 
			'required' 				=> false, 
		),
		array(
			'name'     				=> esc_html('WP Spechtech Plugin','grade'),
			'slug'     				=> 'detheme-spectech', 
			'source'   				=> get_template_directory() . '/plugins/detheme-spectech.zip', 
			'required' 				=> true,
		),
		array(
			'name'     				=> esc_html('Detheme Custom Post','grade'),
			'slug'     				=> 'detheme-post',
			'source'   				=> get_template_directory() . '/plugins/detheme-post.zip',
			'required' 				=> false,
		),
		array(
			'name'     				=> esc_html('Essential Grid','grade'),
			'slug'     				=> 'essential-grid', 
			'source'   				=> get_template_directory() . '/plugins/essential-grid-2.1.6.2.2.zip',
			'required' 				=> false, 
		),
		array(
			'name'     				=> esc_html('DT Recruitment','grade'), 
			'slug'     				=> 'detheme-career', 
			'source'   				=> get_template_directory() . '/plugins/detheme-career.zip', 
			'required' 				=> false, 
		),
		array(
			'name'     				=> esc_html('Grade Demo Installer','grade'),
			'slug'     				=> 'grade-demo', 
			'source'   				=> get_template_directory() . '/plugins/grade-demo.zip', 
			'required' 				=> true,
		),
		);


	$config = array(
		'domain'       		=> 'grade',         			
		'default_path' 		=> '',                         	
		'parent_slug' 		=> 'themes.php', 				
		'menu'         		=> 'install-required-plugins', 	
		'has_notices'      	=> true,                       	
		'is_automatic'    	=> false,					   	
		'message' 			=> ''							
	);

	tgmpa( $plugins, $config );

}

function grade_startup() {

	global $detheme_revealData, $detheme_Scripts,$detheme_Style, $dt_el_id, $woocommerce_loop;

	$woocommerce_loop = array();
	$dt_el_id = 0;

	$detheme_revealData=array();
	$detheme_Scripts=array();
	$detheme_Style=array();

	$locale = get_locale();

	if((is_child_theme() && !load_textdomain( 'grade', get_stylesheet_directory() . "/languages/{$locale}.mo")) || !is_child_theme()){
		load_theme_textdomain('grade',get_template_directory()."/languages");
	}

	add_theme_support('post-thumbnails');
	add_theme_support( 'title-tag' );

	add_theme_support( 'post-formats', array( 'quote', 'video', 'audio', 'gallery', 'link' , 'image' , 'aside' ) );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'woocommerce' );


	register_nav_menus(array(
		'primary' => esc_html__('Top Navigation', 'grade')
	));

	add_action('wp_head', 'grade_load_preloader', 10000);
  	add_action('wp_head','grade_og_generator',2);
	add_action('admin_head','grade_load_admin_stylesheet');
  	add_action('wp_enqueue_scripts','grade_define_var_script'); // begining <head>
	add_action('wp_enqueue_scripts', 'grade_enqueue_scripts', 999); // bottom of <head>
	add_action('wp_enqueue_scripts', 'grade_print_inline_style' );
  	add_action('wp_footer','grade_footer',99999);
} 

add_action('after_setup_theme','grade_startup');


if ( ! function_exists( '_wp_render_title_tag' ) ) :
/* backword compatibility */
	
	function grade_slug_render_title() {
		$tag="title";
		echo "<$tag>".wp_title('|', false, 'left')."</$tag>\n";
	}
	add_action( 'wp_head', 'grade_slug_render_title',1);

	function grade_page_title($title){

	  if(defined('WPSEO_VERSION'))
	    return $title;

	  $blogname=get_bloginfo('name','raw'); 

	  if($blogname!='')
	    return $blogname." | ".$title;
	  return $title;
	}

	add_filter('wp_title','grade_page_title',1);

endif;

function grade_enqueue_scripts(){

	grade_css_style();
	grade_scripts();

}


function grade_css_style(){

	if(is_admin())
		return;

	wp_enqueue_style("theme-style", get_template_directory_uri() . "/style.css");
	wp_enqueue_style("bootstrap", get_template_directory_uri() . "/css/bootstrap.css");
	wp_enqueue_style("grade-spectech-icon", get_template_directory_uri() . "/css/flaticon/flaticon.css");
	wp_enqueue_style("grade-socialicons", get_template_directory_uri() . "/css/socialicons/flaticon.css");
	wp_enqueue_style("grade-menuicon", get_template_directory_uri() . "/css/menuicon/flaticon.css");
	wp_enqueue_style("grade-socialfont", get_template_directory_uri() . "/iconfonts/social/flaticon.css");

	if(!defined('IFRAME_REQUEST')){

		wp_enqueue_style('grade-style', get_template_directory_uri() . '/css/grade.css');

		if(is_rtl()){
			wp_enqueue_style("grade-style-rtl", get_template_directory_uri() . '/css/grade-rtl.css');;
		}
	}

	if(is_child_theme()){
		wp_enqueue_style('grade-child-style', get_stylesheet_directory_uri() . '/style.css');
	}

	$blog_id="";

	if ( is_multisite()){
		$blog_id="-site".get_current_blog_id();
	}

	wp_enqueue_style("grade-custom-style",get_template_directory_uri() . '/css/customstyle'.$blog_id.'.css');
	wp_enqueue_style( 'styleable-select-style', get_template_directory_uri() . '/css/select-theme-default.css', array(), '0.4.0', 'all' );
	wp_enqueue_style( 'grade-style-ie', get_template_directory_uri() . '/css/ie9.css', array());
	wp_style_add_data( 'grade-style-ie', 'conditional', 'IE 9' );

	add_filter( "get_post_metadata",'grade_check_vc_custom_row',1,3);

	if(function_exists('vc_set_as_theme')){

	    $assetPath=plugins_url( 'js_composer/assets/css','js_composer');

	    $front_css_file = version_compare(WPB_VC_VERSION,"4.2.3",'>=')?$assetPath.'/js_composer.css':$assetPath.'/js_composer_front.css';

	    $upload_dir = wp_upload_dir();

	    if(function_exists('vc_settings')){

	      if ( vc_settings()->get( 'use_custom' ) == '1' && is_file( $upload_dir['basedir'] . '/js_composer/js_composer_front_custom.css' ) ) {
	        $front_css_file = $upload_dir['baseurl'] . '/js_composer/js_composer_front_custom.css';
	      }
	    }
	    else{
	      if ( WPBakeryVisualComposerSettings::get('use_custom') == '1' && is_file( $upload_dir['basedir'] . '/js_composer/js_composer_front_custom.css' ) ) {
	        $front_css_file = $upload_dir['baseurl'] . '/js_composer/js_composer_front_custom.css';
	      }

	    }

	    wp_register_style( 'js_composer_front', $front_css_file, false, WPB_VC_VERSION, 'all' );
	    
	    if ( is_file( $upload_dir['basedir'] . '/js_composer/custom.css' ) ) {
	      wp_register_style( 'js_composer_custom_css', $upload_dir['baseurl'] . '/js_composer/custom.css', array(), WPB_VC_VERSION, 'screen' );
	    }

	    wp_enqueue_style('js_composer_front');
	    wp_enqueue_style('js_composer_custom_css');

	}

    if (get_grade_option('blog_type')=='masonry' && is_home()){
		wp_enqueue_style( 'masonry-component', get_template_directory_uri() . '/css/masonry/component.css', array(), '1.0.0', 'all' );
	}


	$primaryFont=get_grade_option('primary-font');

	if (isset($primaryFont['font-family']) && isset($primaryFont['google']) && $primaryFont['font-family']!='') {
		if (isset($primaryFont['google']) && $primaryFont['google']) {
			$fontfamily = $primaryFont['font-family'];
			$subsets = (!empty($primaryFont['subsets'])) ? $primaryFont['subsets']: '';

			wp_enqueue_style( sanitize_title($fontfamily) , esc_url(grade_theme_slug_font_url($fontfamily,$subsets)));
		}	
	} else {
		wp_enqueue_style("source-sans-pro", esc_url(grade_theme_slug_font_url('Source Sans Pro','')));
	}

	$secondaryFont=get_grade_option('secondary-font');

	if (isset($secondaryFont['font-family']) && isset($secondaryFont['google']) && $secondaryFont['font-family']!='') {
		if ($secondaryFont['google']) {
			$fontfamily = $secondaryFont['font-family'];
			$subsets = (!empty($secondaryFont['subsets'])) ? $secondaryFont['subsets'] : '';

			wp_enqueue_style( sanitize_title($fontfamily), esc_url(grade_theme_slug_font_url($fontfamily,$subsets)));
		}	
	} else {

		wp_enqueue_style("oxygen-font", esc_url(grade_theme_slug_font_url('Oxygen','')));
	}

	$sectionFont=get_grade_option('section-font');

	if (isset($sectionFont['font-family']) && isset($sectionFont['google']) && $sectionFont['font-family']!='') {

		if ($sectionFont['google']) {
			$fontfamily = $sectionFont['font-family'];
			$subsets = (!empty($sectionFont['subsets'])) ? $sectionFont['subsets'] : '';

			if (!empty($sectionFont['font-weight'])) {
				$fontweight = $sectionFont['font-weight'].','.$sectionFont['font-weight'].'italic';
				wp_enqueue_style( sanitize_title($fontfamily), esc_url(grade_theme_slug_font_url($fontfamily,$subsets,$fontweight)));

			} else {
				wp_enqueue_style( sanitize_title($fontfamily), esc_url(grade_theme_slug_font_url($fontfamily,$subsets)));
			}

		}	
	} else {
		wp_enqueue_style("oxygen-font", esc_url(grade_theme_slug_font_url('Oxygen','')));
	}

	$tertiaryFont=get_grade_option('tertiary-font');

	if (isset($tertiaryFont['font-family']) && isset($tertiaryFont['google']) && $tertiaryFont['font-family']!='') {
		if ($tertiaryFont['google']) {
			$fontfamily = $tertiaryFont['font-family'];
			$subsets = (!empty($tertiaryFont['subsets'])) ? $tertiaryFont['subsets'] : '';

			wp_enqueue_style( sanitize_title($fontfamily), esc_url(grade_theme_slug_font_url($fontfamily,$subsets)));
		}	
	} else {
		wp_enqueue_style("playfair-display-font", esc_url(grade_theme_slug_font_url('Playfair Display','')));
	}



}

function grade_scripts(){

    $suffix       = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

  	if(get_grade_option('js-code')){
  		add_action('wp_footer',create_function('','print "<script type=\"text/javascript\">".get_grade_option(\'js-code\',\'\')."</script>\n";'),99998);
	}

	if(get_grade_option('boxed_layout_activate') && get_grade_option('dt-header-type')!='leftbar' && get_grade_option('boxed_layout_stretched')){
		wp_enqueue_script('paspartu',get_template_directory_uri() . '/js/paspartu.js', array('jquery'),'',true);
	}


    wp_enqueue_script( 'modernizr' , get_template_directory_uri() . '/js/modernizr.js', array( ), '2.6.2', true );
    wp_enqueue_script( 'bootstrap' , get_template_directory_uri() . '/js/bootstrap.js', array( 'jquery' ), '3.0', true );
    wp_enqueue_script( 'grade-script' , get_template_directory_uri() . '/js/myscript.js', array( 'jquery','bootstrap'), '1.0', true );
    wp_enqueue_script( 'styleable-select', get_template_directory_uri() . '/js/select'.$suffix.'.js', array(), '0.4.0', true );
    wp_enqueue_script( 'styleable-select-exec' , get_template_directory_uri() . '/js/select.init.js', array('styleable-select'), '1.0.0', true );
    wp_enqueue_script( 'jquery.appear' , get_template_directory_uri() . '/js/jquery.appear'.$suffix.'.js', array(), '', true );
    wp_enqueue_script( 'jquery.counto' , get_template_directory_uri() . '/js/jquery.counto'.$suffix.'.js', array(), '', true );
	wp_enqueue_script( 'classie',get_template_directory_uri()."/js/classie.js",array(), '1.0', true);
	wp_enqueue_script( 'modal-effects',get_template_directory_uri()."/js/modal_effects.js",array(), '1.0', true);

	if (get_grade_option('blog_type')=='masonry' && is_home()){
		wp_enqueue_script( 'modernizr-custom' , get_template_directory_uri() . '/js/masonry/modernizr.custom.js', array(), '1.0', true );
		wp_enqueue_script( 'masonry-pkgd' , get_template_directory_uri() . '/js/masonry/masonry.pkgd.min.js', array(), '1.0', true );
		wp_enqueue_script( 'imagesloaded' , get_template_directory_uri() . '/js/masonry/imagesloaded.js', array(), '1.0', true );
		wp_enqueue_script( 'AnimOnScroll' , get_template_directory_uri() . '/js/masonry/AnimOnScroll.js', array(), '1.0', true );
		wp_enqueue_script( 'masonry-script' , get_template_directory_uri() . '/js/masonry/script.js', array(), '1.0', true );
	}
	if(get_post_type()=='dtcareer'){
			 wp_enqueue_script( 'grade-career-reply' , get_template_directory_uri() . '/js/career.js', array( 'jquery' ), '3.0', true );
	}
	else{
		if ( is_singular() ) { 
			 wp_enqueue_script( 'grade-comment-reply' , get_template_directory_uri() . '/js/comment-reply.min.js', array( 'jquery' ), '3.0', true );
		} 
	}

	if(function_exists('vc_set_as_theme')){
	    wp_enqueue_script( 'wpb_composer_front_js' );
	}

}

function grade_define_var_script(){
	print "<script type=\"text/javascript\">var ajaxurl = '".admin_url('admin-ajax.php')."';var themecolor='".get_grade_option('primary-color','#000000')."';</script>\n";
}


function grade_footer(){

	grade_load_custom_script();
	grade_load_modal_content();
	grade_load_vc_custom_css_style();


}

function grade_load_custom_script(){

	global $detheme_Scripts;
	if(count($detheme_Scripts)) {
		print "<script type=\"text/javascript\">\n".implode("\n",$detheme_Scripts)."\n</script>\n";
	}

}

function grade_load_modal_content(){

		global $detheme_revealData; 

		if(count($detheme_revealData)) { 
			print implode("\n",$detheme_revealData);
			print "<div class=\"md-overlay\"></div>";
		}

}

function grade_load_vc_custom_css_style(){

	global $detheme_Style; 

	if(count($detheme_Style)){
		print "<style type=\"text/css\">".implode("",$detheme_Style)."</style>";
	}
}

function grade_check_vc_custom_row($post=null,$object_id, $meta_key=''){

  if('_wpb_shortcodes_custom_css'==$meta_key){

    $meta_cache = wp_cache_get($object_id, 'post_meta');
    return '';
   }
}

function grade_og_generator(){

	if(is_admin())
		return;

	if (!get_grade_option('meta-og'))
		return;

	$ogimage = "";
	if (function_exists('wp_get_attachment_thumb_url')) {
		$ogimage = wp_get_attachment_thumb_url(get_post_thumbnail_id(get_the_ID())); 
	}

	print '<meta property="og:title" content="'.esc_attr(get_the_title()).'" />'."\n";
	print '<meta property="og:type" content="article"/>'."\n";
	print '<meta property="og:locale" content="'.get_locale().'" />'."\n";
	print '<meta property="og:site_name" content="'.esc_attr(get_bloginfo('name')).'"/>'."\n";
	print '<meta property="og:url" content="'.esc_url(get_permalink()).'" />'."\n";
	print '<meta property="og:description" content="'.esc_attr(str_replace( '[&hellip;]', '&hellip;', strip_tags( get_the_excerpt() ))).'" />'."\n";
	print '<meta property="og:image" content="'.esc_attr($ogimage).'" />'."\n";

}


function grade_theme_slug_font_url($font_family,$subset,$font_weight='300,300italic,400,400italic,700,700italic') {
	$fonts_url = '';
	 
	/* Translators: If there are characters in your language that are not
	* supported by Open Sans, translate this to 'off'. Do not translate
	* into your own language.
	*/
 
	if ( !preg_match('/Open Sans/',$font_family )) {
		$font_families = array();
	 
		$font_families[] = $font_family.':'.$font_weight;
		 
		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( $subset ),
		);
		 
		$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	}
	 
	return esc_url_raw( $fonts_url );
} 

function grade_print_inline_style(){

	if(is_admin() || in_array($GLOBALS['pagenow'], array('wp-login.php', 'wp-register.php')))
		return;


	$is_ssl=wp_http_supports( array( 'ssl' ));

  	$css_banner=array();

	if(get_grade_option('banner')){
		$css_banner[]= 'background: url('.esc_url(get_grade_option('banner','')).') no-repeat 50% 50%; max-height: 100%; background-size: cover;'; 
	}

	if(get_grade_option('banner-color')){
		$css_banner[]='background-color: '.get_grade_option('banner-color','').';'; 
	}


	$banner_height=grade_get_banner_height();
	if($banner_height){
		$css_banner[]='min-height:'.$banner_height.";";
		$css_banner[]='height:'.$banner_height.";";
	}

	wp_enqueue_style( 'fontello-css', get_template_directory_uri() . '/iconfonts/list-style/fontello.css');

	print "<style type=\"text/css\">\n";

	if(get_grade_option('sandbox-mode')){
  		$customstyle=grade_custom_style_compile(get_grade_options(),"",false);

  		print $customstyle."\n";
  	}


	if(count($css_banner)){
		print (count($css_banner))?"section#banner-section {".@implode("\n",$css_banner)."}\n":"";

		if (get_grade_option('logo-top')) {
			print "div#head-page #dt-menu.dt-menu-center ul li.logo-desktop a {margin-top:".get_grade_option('logo-top','')."px;}\n"; 
			print "div#head-page #dt-menu.dt-menu-left ul li.logo-desktop a {margin-top:".get_grade_option('logo-top','')."px;}\n"; 
			print "div#head-page #dt-menu.dt-menu-leftbar ul li.logo-desktop a {margin-top:".get_grade_option('logo-top','')."px;}\n"; 
			print "div#head-page #dt-menu.dt-menu-right ul li.logo-desktop a {margin-top:".get_grade_option('logo-top','')."px;}\n"; 
		}

	}
	print get_grade_option('logo-left') ?"div#head-page #dt-menu ul li.logo-desktop a {margin-left:".get_grade_option('logo-left','')."px;}\n":"";
	print get_grade_option('body_background') ? get_grade_option('body_background',''):"";

	print "</style>\n";

	/* favicon handle */

	$favicon=get_grade_option('dt-favicon-image');

	if($favicon && isset($favicon['url']) && ''!=$favicon['url'] && !function_exists('wp_site_icon')){
		$favicon_url=$favicon['url'];
		print "<link rel=\"shortcut icon\" type=\"image/png\" href=\"".esc_url(grade_maybe_ssl_url($favicon_url))."\">\n";
	}

}



function grade_load_preloader(){

	if(!get_grade_option('page_loader') || defined('IFRAME_REQUEST') || is_404() || (defined('DOING_AJAX') && DOING_AJAX) )
		return '';
?>
<script type="text/javascript">
jQuery(document).ready(function ($) {
	'use strict';
    $("body").queryLoader2({
        barColor: "#fff",
        backgroundColor: "none",
        percentage: false,
        barHeight: 0,
        completeAnimation: "fade",
        minimumTime: 500,
        onLoadComplete: function() { $('.modal_preloader').fadeOut(300,function () {$('.modal_preloader').remove();})}
    });
});
</script>
	<?php 
}

function grade_load_admin_stylesheet(){
	wp_enqueue_style( 'detheme-admin',get_template_directory_uri() . '/lib/css/admin.css', array(), '', 'all' );
}

require_once( get_template_directory().'/lib/webicon.php'); // load detheme icon
require_once( get_template_directory().'/lib/options.php'); // load bootstrap stylesheet and scripts
require_once( get_template_directory().'/lib/custom_functions.php'); // load specific functions
require_once( get_template_directory().'/lib/metaboxes.php'); // load custom metaboxes
require_once( get_template_directory().'/lib/widgets.php'); // load custom widgets
require_once( get_template_directory().'/lib/updater.php'); // load easy update
require_once( get_template_directory().'/lib/fonts.php'); // load detheme font family


if(function_exists('vc_set_as_theme')){
	vc_set_as_theme(true);
}

?>