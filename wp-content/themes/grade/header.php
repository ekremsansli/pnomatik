<?php
/**
 * @package WordPress
 * @subpackage Grade
 * @since Grade 1.0
*/
defined('ABSPATH') or die();
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<?php get_template_part('lib/page','options'); ?>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php echo esc_url(bloginfo( 'pingback_url' )); ?>">
<?php wp_head(); ?>
</head>

<?php
switch (get_grade_option('dt-header-type')) {
    case 'center':
    	$classmenu = 'menu-center';
        break;
    case 'leftvc' :
        $classmenu = 'menu-leftvc';
        break;
    default:
        $classmenu = 'menu-right';
}

?>

<body <?php body_class(esc_attr($classmenu)); print get_grade_option('body_tag','');?>>
<?php get_template_part('pagetemplates/preloader'); ?>
<?php 
if(!is_404()): 
	/*** Boxed layout ***/

	if(get_grade_option('boxed_layout_activate')){

		if (get_grade_option('dt-header-type')!='leftbar') {

			if (get_grade_option('boxed_layout_stretched')) {
				echo '<div class="paspartu_outer paspartu_on_bottom_fixed body_background_color">
					    <div class="paspartu_top body_background_color"></div>
					    <div class="paspartu_left body_background_color"></div>
					    <div class="paspartu_right body_background_color"></div>
					    <div class="paspartu_bottom body_background_color"></div>
					    <div class="paspartu_inner body_background_color">'.
				'<div class="container dt-boxed-stretched-container body_background_color">';

			} else {
				echo '<div class="container dt-boxed-container">';
			}

		} 

	}

	?>	
<input type="checkbox" name="nav" id="main-nav-check">

<?php if(get_grade_option('showtopbar',true)): 
	get_template_part('pagetemplates/top-bar');?>
<?php elseif ('leftvc' == get_grade_option('dt-header-type','leftvc')) :?>
<div id="top-bar"></div>
<?php endif;?>

<div class="top-head<?php  print is_admin_bar_showing()?" adminbar-is-here":"";?><?php print get_grade_option('dt-sticky-menu') ?" is-sticky-menu":" no-sticky-menu";?> <?php print get_grade_option('dt-header-type')=='leftbar'?" vertical_menu":"";?>">

<?php 
	if(get_grade_option('dt-show-header',true)): 
		get_template_part('pagetemplates/heading');
	endif;
?>

</div>

<?php
	if(get_grade_option('show-banner-area') and !is_404()){
		get_template_part('pagetemplates/banner');
	}
?>

<?php do_action('after_menu_section'); ?>

<?php endif; ?>
