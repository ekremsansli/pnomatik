<?php
defined('ABSPATH') or die();
/**
 * Template Name: Fullwidth
 *
 * This is custom template
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Grade
 * @since Grade 1.0
 */

get_header();
set_query_var('sidebar','nosidebar');
?>
<div class="content no-padding<?php print sanitize_html_class(get_grade_option('dt-header-type')=='leftbar' ? " vertical_menu_container":"");?>">
<div class="nosidebar">
<?php 
	$i = 0;

	while ( have_posts() ) :
		$i++;
		if ($i==1) :
?>
	<div class="blank-reveal-area"></div>
<?php 	endif; 
	the_post();
?>
<?php if(get_grade_option('dt-show-title-page')):?>
						<h1 class="page-title"><?php the_title();?></h1>
<?php endif;?>
						<div class="post-article">
<!-- start content -->
<?php the_content(); 
	wp_link_pages( grade_get_link_pages_args() );
?>
<!-- end content -->
						</div>

<?php
	if(comments_open()):?>
						<div class="container">
							<div class="comment-count">
								<h3><?php comments_number(esc_html__('No Comments','grade'),esc_html__('1 Comment','grade'),esc_html__('% Comments','grade')); ?></h3>
							</div>

							<div class="section-comment">
								<?php comments_template('/comments.php', true); ?>
							</div><!-- Section Comment -->
						</div>
<?php endif;?>

<?php endwhile; ?>
			</div>
	</div>
<?php
get_footer();
?>