<?php
defined('ABSPATH') or die();
/**
 * Template Name: Blank Page
 * This is custom template
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Grade
 * @since Grade 1.0
 */

set_query_var('sidebar','nosidebar');

?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<?php get_template_part('lib/page','options'); ?>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php echo esc_url(bloginfo( 'pingback_url' )); ?>">
<?php wp_head();?>
</head>
<body <?php body_class(); print get_grade_option('body_tag');?>>

<?php get_template_part('pagetemplates/preloader'); ?>

<!-- start content -->
<div class="content">
<div class="nosidebar">
<?php 
while ( have_posts() ) : 
the_post();
?>
	<div class="post-article">
	<?php 
		the_content();
		wp_link_pages( grade_get_link_pages_args() );
	 ?>
	</div>

<?php
	if(comments_open()):?>
						<div class="container">
							<div class="comment-count">
								<h3><?php comments_number(esc_html__('No Comments','grade'),esc_html__('1 Comment','grade'),esc_html__('% Comments','grade')); ?></h3>
							</div>

							<div class="section-comment">
								<?php comments_template('/comments.php', true); ?>
							</div><!-- Section Comment -->
						</div>
<?php endif;?>

<?php endwhile; ?>
			</div>
	</div>
<!-- end content -->
<?php wp_footer(); ?>
</body>
</html>