<?php
defined('ABSPATH') or die();
/**
 *
 * @package WordPress
 * @subpackage Grade
 * @since Grade 1.0
 */
?>
<div class="row">
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="col-xs-12">
		<div class="postcontent">
			<div class="row">
			<?php if(get_grade_option('dt-show-title-page') && (bool)get_grade_option('essential_grid-title')):?>
			<div class="col-xs-12">
				<h2 class="blog-post-title"><a href="<?php echo esc_url(get_permalink()); ?>"><?php the_title();?></a></h2>
			</div>
    		<?php endif;?>
    		<?php
				the_content();
			?>
			</div>
		</div>
		<?php get_template_part('essential_grid/postaboutcomment'); ?>
	</div>
</article>
</div>
