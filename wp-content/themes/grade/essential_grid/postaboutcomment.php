<?php
defined('ABSPATH') or die();
/**
 *
 * @package WordPress
 * @subpackage Grade
 * @since Grade 1.0
 */
?>
<?php 
if(comments_open()):?>
	<div class="comment-count">
		<h3><?php comments_number(esc_html__('No Comments','grade'),esc_html__('1 Comment','grade'),esc_html__('% Comments','grade')); ?></h3>
	</div>

	<div class="section-comment">
		<?php comments_template('/comments.php', true); ?>
	</div><!-- Section Comment -->
<?php endif;?>
