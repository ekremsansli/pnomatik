<?php
defined('ABSPATH') or die();
/**
 *
 * @package WordPress
 * @subpackage Grade
 * @since Grade 1.0
 * @version 1.0
 */
?>
<?php 
do_action('grade_before_footer_section');
//showfooterarea
if(get_grade_option('showfooterarea')){
	get_footer('footerarea');
}
do_action('grade_after_footer_section');
?>
<?php

	/*** Boxed layout ***/
	$is_vertical_menu = false;
	$is_boxed = false;
	if(get_grade_option('boxed_layout_activate')){
		$is_boxed = true;
		$is_boxed_stretched = get_grade_option('boxed_layout_stretched');

		if (get_grade_option('dt-header-type')=='leftbar') {
			$is_vertical_menu = true;
			echo '</div></div>';
		} else {
			if ($is_boxed_stretched) {
				echo '</div></div></div>';
			} else if($is_boxed) {
				echo '</div>';
			}
		}

	}
?>
<?php wp_footer(); ?>
</body>
</html>