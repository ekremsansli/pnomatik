<?php
defined('ABSPATH') or die();
/**
 * The default template for displaying content search result
 *
 * Used for search.
 *
 * @package WordPress
 * @subpackage Grade
 * @since Grade 1.0
 */

$post_type_object=get_post_type_object(get_post_type());
$label = $post_type_object->labels->singular_name;

$short_description= get_the_excerpt(get_the_ID());

if ( ''==$short_description ) {
	$short_description = get_the_content();
	$short_description = grade_strip_shortcodes( $short_description );

	$excerpt_length = apply_filters( 'excerpt_length', 55 );

	$short_description=wp_trim_words($short_description, $excerpt_length,"");
}


?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<h2 class="blog-post-title"><a href="<?php echo esc_url(get_permalink()); ?>"><?php the_title();?></a></h2>
	<div class="blog-post-type"><?php print $label;?></div>
	<div class="postcontent">
		<?php 
			print wp_kses_post($short_description);
		?>
	</div>
</article>