<?php
defined('ABSPATH') or die();
/**
 * display single specification post type
 *
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Grade
 * @since Grade 1.0
 * @version 1.0
 */

get_header();

get_template_part('pagetemplates/scrollingsidebar');

$sidebar_position=get_grade_sidebar_position();
$sidebar= $sidebar_position=='fullwidth' ? "fullwidth": (is_active_sidebar( 'detheme-sidebar' )?'detheme-sidebar':false);

if(!$sidebar){
	$sidebar_position = "nosidebar";
}

set_query_var('sidebar',$sidebar);

$class_vertical_menu = get_grade_option('dt-header-type')=='leftbar' ? ' vertical_menu_container' :'';
?>
<div  class="content <?php print sanitize_html_class($sidebar_position).' '.sanitize_html_class($class_vertical_menu); ?>">
<?php if($sidebar_position != 'fullwidth'):?>
	<div class="container">
<?php endif;?>
<?php if($sidebar_position == 'sidebar-right' || $sidebar_position=='sidebar-left'){?>
		<div class="row">
			<div class="col-xs-12 col-sm-8 <?php print ($sidebar_position=='sidebar-left')?" col-sm-push-4":"";?> col-md-9 <?php print ($sidebar_position=='sidebar-left')?" col-md-push-3":"";?>">
<?php	} ?>
<?php

while ( have_posts() ) : ?>

	<div class="blank-reveal-area"></div>
<?php
	the_post();
	get_template_part('detheme-spectech/content');
?>

<?php endwhile;?>
<?php if($sidebar_position == 'sidebar-right' || $sidebar_position=='sidebar-left'):?>
</div>
<?php endif;?>

<?php if ('sidebar-right'==$sidebar_position) { ?>
			<div class="col-xs-12 col-sm-4 col-md-3 sidebar">
				<?php get_sidebar(); ?>
			</div></div><!-- .row -->
<?php }
	elseif ($sidebar_position=='sidebar-left') { ?>
			<div class="col-xs-12 col-sm-4 col-md-3 sidebar col-sm-pull-8 col-md-pull-9">
				<?php get_sidebar(); ?>
			</div></div><!-- .row -->
<?php }?>

<?php if($sidebar_position != 'fullwidth'):?>
	</div><!-- .container -->
<?php endif;?>
</div><!-- .blog .single-post .content -->
<?php echo do_shortcode('[vc_row expanded="2" css=".vc_custom_1469170203479{background-color: #fec56b !important;}"][vc_column][vc_row_inner css=".vc_custom_1469171164271{margin-top: 50px !important;margin-bottom: 30px !important;}"][vc_column_inner width="7/12"][section_header main_heading=" Sipariş ve Detaylı Bilgi için <b>Bize Ulaşın</b>" text_align="left" font_size="small" use_multi_color="1" multi_color="#4a4a4a | #4a4a4a | #4a4a4a | #4a4a4a | #202020 | #202020"][/vc_column_inner][vc_column_inner width="2/12" css=".vc_custom_1469171062885{margin-top: 8px !important;}"][vc_btn title="Bize Ulaşın" style="outline-custom" outline_custom_color="#4a4a4a" outline_custom_hover_background="#4a4a4a" outline_custom_hover_text="#ffffff" align="right" link="url:%23|||"][/vc_column_inner][vc_column_inner width="2/12" css=".vc_custom_1469171069297{margin-top: 8px !important;}"][vc_btn title="Ekipmanlar" style="custom" custom_background="#696969" custom_text="#ffffff" align="left" link="url:%23|||"][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row]');?>
<?php
get_footer();
?>
<style>

section#banner-section{

	background: url(http://localhost/wp-content/uploads/2019/10/banner9.jpg) no-repeat 50% 50%;
max-height: 100%;
background-size: cover;
background-color: #ededed;


</style>
