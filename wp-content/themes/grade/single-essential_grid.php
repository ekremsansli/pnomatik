<?php
defined('ABSPATH') or die();
/**
 * display single essential grid post type
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Grade
 * @since Grade 1.0
 * @version 1.0
 */

get_header(); 

get_template_part('pagetemplates/scrollingsidebar');

$sidebar_position=get_grade_sidebar_position();
$sidebar= $sidebar_position=='fullwidth' ? "fullwidth": (is_active_sidebar( 'detheme-sidebar' )?'detheme-sidebar':false);

if(!$sidebar){
	$sidebar_position = "nosidebar";
}

set_query_var('sidebar',$sidebar);

$class_vertical_menu = get_grade_option('dt-header-type')=='leftbar' ? ' vertical_menu_container' :'';
?>
<div  class="content <?php print sanitize_html_class($sidebar_position).' '.sanitize_html_class($class_vertical_menu); ?>">
<?php if($sidebar_position != 'fullwidth'):?>
	<div class="container"> 
<?php endif;?>
<?php if($sidebar_position == 'sidebar-right' || $sidebar_position=='sidebar-left'){?>
		<div class="row">
			<div class="col-xs-12 col-sm-8 <?php print ($sidebar_position=='sidebar-left')?" col-sm-push-4":"";?> col-md-9 <?php print ($sidebar_position=='sidebar-left')?" col-md-push-3":"";?>">
<?php	} ?>
<?php

while ( have_posts() ) : ?>

	<div class="blank-reveal-area"></div>
<?php
	the_post();
	get_template_part('essential_grid/content');
?>

<?php endwhile;?>
<?php if($sidebar_position == 'sidebar-right' || $sidebar_position=='sidebar-left'):?>
</div>
<?php endif;?>

<?php if ('sidebar-right'==$sidebar_position) { ?>
			<div class="col-xs-12 col-sm-4 col-md-3 sidebar">
				<?php get_sidebar(); ?>
			</div></div><!-- .row -->
<?php }
	elseif ($sidebar_position=='sidebar-left') { ?>
			<div class="col-xs-12 col-sm-4 col-md-3 sidebar col-sm-pull-8 col-md-pull-9">
				<?php get_sidebar(); ?>
			</div></div><!-- .row -->
<?php }?>
	
<?php if($sidebar_position != 'fullwidth'):?>
	</div><!-- .container -->
<?php endif;?>
</div><!-- .blog .single-post .content -->
<?php
get_footer();
?>