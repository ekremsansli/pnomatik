<?php
defined('ABSPATH') or die();

function grade_custom_tinymce_plugin_translation() {
    $strings = array(
        'insert_dt_shortcode' => esc_html__('Insert Grade Shortcode', 'grade'),
        'dt_shortcode' => esc_html__('Grade Shortcode', 'grade'),
        'icon_title' => esc_html__('Icon', 'grade'),
        'button_title'=>esc_html__('Buttons','grade'),
        'counto_title'=>esc_html__('Count To','grade'),
    );
    $locale = _WP_Editors::$mce_locale;

    $translated = 'tinyMCE.addI18n("' . $locale . '.dtshortcode", ' . json_encode( $strings ) . ");\n";

     return $translated;
}
$strings = grade_custom_tinymce_plugin_translation();
?>