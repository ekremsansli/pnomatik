<?php
defined('ABSPATH') or die();

/*
Plugin Name: WP Spechtech
Plugin URI: http://www.detheme.com/
Description: Custom post with technical spesification fields
Version: 1.0.1
Author: detheme.com
Author URI: http://www.detheme.com/
Domain Path: /languages/
Text Domain: spectech
*/

define('SPECTECH_BASENAME',dirname(plugin_basename(__FILE__)));
define('SPECTECH_DIR',plugin_dir_path(__FILE__));
define('SPECTECH_DIR_URL',plugin_dir_url(__FILE__)); 
define('SPECTECH_INSTALLED',1);
define('SPECTECH_FILE',__FILE__);
define('SPECTECH_UPDATE_URL','http://repo.detheme.com/plugin/spechtec');
define('SPECTECH_TABLE_NAME','spectech');

load_plugin_textdomain('spectech', false, SPECTECH_BASENAME. '/languages/');

require_once( SPECTECH_DIR . '/lib/spectech-table.class.php' );
require_once( SPECTECH_DIR . '/lib/functions.php' );
require_once( SPECTECH_DIR . '/lib/spectech.class.php' );


$wp_spectech= new detheme_spectech();
?>