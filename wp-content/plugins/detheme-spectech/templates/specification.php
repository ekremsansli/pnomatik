<?php
defined('ABSPATH') or die();
/**
 * @version: 1.0.0
 * @since: 1.0.0
 */

$specifications=$post->specifications;
?>
<div class="spectech-specifications">
	<ul class="spectech-conversion-tool">
		<li class="conversion-tool active conversion-to-metric" data-conversion="metric"><a href="#"><?php _e('Metric','spectech');?></a></li>
		<li class="conversion-tool conversion-to-imperial" data-conversion="imperial"><a href="#"><?php _e('Imperial','spectech');?></a></li>
	</ul>
<?php foreach($specifications as $field_group => $spec){
	?>
	<fieldset class="spectech-group">
	<legend class="spectech-group-name"><?php print $field_group;?></legend>
	<ul class="spectech-specifications-list">
		<?php foreach($spec as $specification){

			$unit_id=isset($specification->unit_id) ? $specification->unit_id : 0;
			$value=spectech_get_field_display($specification->value , $unit_id);

			print "<li><label>".$specification->field_name."</label>: ".$value."</li>";
		} ?>
	</ul>

	</fieldset>
<?php
}?>
</div>
