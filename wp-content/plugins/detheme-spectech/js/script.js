jQuery(document).ready(function($){
	function spec_conversion(conversion){

		var field_spec=$('.spectech-specification.has-conversion');

		field_spec.each(function(){
			var field=$(this);
			var conversion_unit=field.data(conversion+'-unit');
			var conversion_value=field.data(conversion+'-value');
			var field_value=field.find('.spectech-value'),unit_value=field.find('.spectech-unit-value');

			field_value.text(conversion_value);
			unit_value.text(conversion_unit);

		});
	}

	if($(".conversion-tool").length){
		$(".conversion-tool").each(function(i,e){
			var $tool=$(this);

			$tool.click(function(e){
				e.preventDefault();
				var conversion=$tool.data('conversion') ? $tool.data('conversion') : 'metric';
				$tool.siblings().removeClass('active')
				$tool.addClass('active');

				spec_conversion(conversion);

			});
		});
	}
});