<?php
defined('ABSPATH') or die();
if ( class_exists( 'WP_Importer' ) ) {

class WP_Spectech_Field_Import extends WP_Importer {
	var $id; 

	var $datas = array();
	var $result_datas = array();
	var $bank_type = '';
	var $force_value = false;

	function WP_Spectech_Field_Import() { /* nothing */ }

	function dispatch() {
		$this->header();

		$step = empty( $_GET['step'] ) ? 0 : (int) $_GET['step'];

		switch ( $step ) {
			case 0:
				$this->greet();
				break;
			case 1:
				check_admin_referer( 'import-upload' );
				if ( $this->handle_upload() ){
					$file = get_attached_file( $this->id );
					set_time_limit(0);
					$this->import( $file );
				}
				break;
		}

		$this->footer();
	}

	function import( $file ) {

		$datas=WP_Spectech_Field_Import::import_from_file( $file );
		WP_Spectech_Field_Import::parse($datas);
		$this->import_end();
	}

	static function import_from_file( $file ) {
		if ( ! is_file($file) ) {
			echo '<p><strong>' . __( 'Sorry, there has been an error.' ,'spectech') . '</strong><br />';
			echo __( 'The file does not exist, please try again.' ,'spectech') . '</p>';
			$this->footer();
			die();
		}

		$datas=array();
		$wp_filesystem= new WP_Filesystem_Direct(array());

	    if ($buffers=$wp_filesystem->get_contents($file)) {
	    	$datas=json_decode(($buffers));
	    }

		return $datas;
	}

	function import_end() {
		wp_import_cleanup( $this->id );
		echo '<p>' . __( 'All done.','spectech') . ' <a href="' . admin_url() . '">' . __( 'Have fun!' ,'spectech') . '</a>' . '</p>';
	}

	function handle_upload() {
		$file = wp_import_handle_upload();

		if ( isset( $file['error'] ) ) {
			echo '<p><strong>' . __( 'Sorry, there has been an error.','spectech') . '</strong><br />';
			echo esc_html( $file['error'] ) . '</p>';
			return false;
		} else if ( ! file_exists( $file['file'] ) ) {
			echo '<p><strong>' . __( 'Sorry, there has been an error.','spectech' ) . '</strong><br />';
			printf( __( 'The export file could not be found at <code>%s</code>. It is likely that this was caused by a permissions problem.','spectech' ), esc_html( $file['file'] ) );
			echo '</p>';
			return false;
		} 

		$this->id = (int) $file['id'];
		return true;
	}

	static function parse( $datas=array() ) {

		global $wpdb;


		if(count($datas)){

			$table_name = $wpdb->get_blog_prefix().SPECTECH_TABLE_NAME."_field";

			$values = array();
			$place_holders = array();

			foreach ($datas as $data) {

				if(!isset($data->field_name) || empty($data->field_name)) continue;

				$field_group=isset($data->field_group) ? trim($data->field_group) : "";
				$field_name= trim($data->field_name);
				$unit_id=isset($data->unit_id) ? trim($data->unit_id) : "0";

				array_push($values, $field_group, $field_name, $unit_id);
				$place_holders[] = "('%s', '%s','%d')";
			}

			$query = $wpdb->prepare("INSERT INTO {$table_name} (field_group,field_name,unit_id) VALUES ".implode(', ', $place_holders),$values);

			return $wpdb->query($query);

		}


	}


	function header() {
		echo '<div class="wrap">';
		screen_icon();
		echo '<h2>' . __( 'Import Specification Field','spectech') . '</h2>';
	}

	function footer() {
		echo '</div>';
	}

	function greet() {
		echo '<div class="narrow">';
		echo '<p>'.__( 'Howdy! Upload your json file and we&#8217;ll import the edd payment into this site.','spectech').'</p>';
		echo '<p>'.sprintf(__( 'Choose a .json file to upload, then click %s.','spectech'),__('Upload file and import')).'</p>';
		wp_import_upload_form( 'admin.php?import=spectech_field_import&amp;step=1' );
		echo '</div>';
	}

}

}
?>