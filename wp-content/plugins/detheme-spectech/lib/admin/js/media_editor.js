jQuery(document).ready(function ($) {
    'use strict';
//    var dti18n = window.dtvc_i18nLocale;

      function remove_attach_media(event){

        event.preventDefault();

        var button=$(this),container=button.closest('.attach_media_field'),imagepreview=$('a.gallery_widget_add_media',container);
        var imgID=imagepreview.find('.media-preview img').data('id'),input=container.find('input').val();

        if(input==''){
          input=new Array();
        }
        else{
          input=input.split(/,/);
        }
        for(var i = 0; i < input.length; i++) {
              if(input[i] == imgID) {
                input.splice(i, 1);
              }
        }

        imagepreview.html(imagepreview.attr('title'));
        container.find('input').val('').trigger('change');
        button.hide();
      }

      wp.media.DethemeMediaEdit = {
            getData:function () {
              return this.formfield.val();
            },
            set:function (selection) {

            if(this.getData()==selection.get('id')){
               return false;
            }

              var mediapreview=$('<div class="media-preview"></div>');
              var imagepreview=$('<img class="'+selection.attributes.type+'" src="'+(selection.attributes.type=='image'?selection.attributes.url:selection.attributes.icon)+'" data-id="'+selection.get('id')+'" />');
              var imagelabel=$('<span class="media-label"></span>');
              imagelabel.text(selection.attributes.filename);
              mediapreview.append(imagepreview,imagelabel);
              this.preview.find('a.gallery_widget_add_media').html(mediapreview);
              this.formfield.val(selection.get('id')).trigger('change');

              this.field.find('.remove_attach_media').show();
  
              return false;
            },
          frame: function($field) {


          var button=$field.find('.gallery_widget_add_media'),remove_image=$field.find('.remove_attach_media'),
          formfield=$field.find('input'),preview=$field.find('.gallery_widget_attached_medias_list li'),mediaType=$field.data('type');

          this.field=$field;
          this.formfield =$field.find('input');
          this.preview =preview;
          remove_image.click(remove_attach_media);

              if ( this._frame )
                  return this._frame;
       
              this._frame = wp.media({
                  id:         'detheme_media',               
                  state:      'insert-image',
                  title:      'Insert Media',//dti18n.insert_media,
                  editing:    true,
                  multiple:   false,
                  toolbar:    'insert-image',
                  menu:'insert-image',
                  type : 'image',
                  states:[ new wp.media.controller.DethemeMediaEdit({library: wp.media.query( {type:mediaType})}) ]
              });

                this._frame.on('toolbar:create:insert-image', function (toolbar) {
                    this.createSelectToolbar(toolbar, {
                        text: 'Select Media'//dti18n.select_media
                    });
                }, this._frame);

                this._frame.state('insert-image').on('select', this.select);
              return this._frame;
          },
            select:function () {

                var selection = this.get('selection').single();
                wp.media.DethemeMediaEdit.set(selection ? selection : -1);
            }
      };

        wp.media.controller.DethemeMediaEdit = wp.media.controller.FeaturedImage.extend({
            defaults:_.defaults({
                id:'insert-image',
                filterable:'uploaded',
                multiple:false,
                toolbar:'insert-image',
                title:'Select Media To',//dti18n.select_media_to,
                priority:60,
                syncSelection:false,
            }, wp.media.controller.Library.prototype.defaults),
            updateSelection:function () {
                var selection = this.get('selection'),
                    id = wp.media.DethemeMediaEdit.getData(),
                    attachment;

                if ('' !== id && -1 !== id) {
                    attachment = wp.media.model.Attachment.get( id );;
                    attachment.fetch();
                }
                selection.reset(attachment ? [ attachment ] : []);
            }
        });

      if($('.attach_media_field').length){
        $('.attach_media_field').each(function(){

          var element=$(this);

          element.find('.gallery_widget_add_media').unbind('click').click(function(){

             wp.media.DethemeMediaEdit.frame(element).open();
          });

          element.find('.remove_attach_media').unbind('click').click(remove_attach_media);
        });
    }

});