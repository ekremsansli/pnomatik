<?php
defined('ABSPATH') or die();
/** 
 * @version: 1.0.0
 * @since: 1.0.0
 */
?>
<div class="wrap">
<div class="campaigns-container content-container">
<h1><?php _e('Specification Fields','spectech');?><a href="<?php print admin_url("edit.php?post_type=specification&page=spectech-setting&action=edit");?>" class="btn page-title-action"><?php print ucwords(__('create new field','spectech'));?></a></h1>
<?php   
//$this->search_box(__('search','spectech'), 'search_id');
?>
<?php	
$singular = $this->_args['singular'];?>
<form method="post">
<?php 
$this->display_tablenav( 'top' );
?>
  <input type="hidden" name="page" value="spectech-setting" />
<hr />
<div class="form-field-list">
<table class="wp-list-table <?php echo implode( ' ', $this->get_table_classes() ); ?>">
	<thead>
	<tr>
		<?php $this->print_column_headers(); ?>
	</tr>
	</thead>

	<tbody id="the-list"<?php
		if ( $singular ) {
			echo " data-wp-lists='list:$singular'";
		} ?>>
		<?php $this->display_rows_or_placeholder(); ?>
	</tbody>

	<tfoot>
	<tr>
		<?php $this->print_column_headers( false ); ?>
	</tr>
	</tfoot>

</table>
</div>
<?php
		$this->display_tablenav( 'bottom' );
?>
</form>
</div>
</div>