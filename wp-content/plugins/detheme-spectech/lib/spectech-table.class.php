<?php
defined('ABSPATH') or die();
/** 
 * @version: 1.0.0
 * @since: 1.0.0
 */

require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
class Spectech_Table extends WP_List_Table {

	function __construct() {
		// Set parent defaults
		parent::__construct(array(
			'singular' => 'specification',
			'plural'   => 'specifications',
			'ajax'     => false        
		));
	}
	function column_default($item, $column_name) {

		return $item->{$column_name};
	}

	function get_columns(){
	  $columns = array(
		'cb'   => '<input type="checkbox">',
	    'field_name' => ucwords(__('Specification','spectech')),
	    'field_group'    => ucwords(__('Group','spectech')),
	    'unit_id'      => ucwords(__('Unit','spectech'))
	  );

	  $columns = apply_filters( "manage_spechtech_columns", $columns );

	  return $columns;
	}

	function get_sortable_columns() {
		return array(
			'field_name' => array('title', false),
			'field_group' => array('field_group', false),
			'unit_id' => array('unit_id', false),
		);
	}

	protected function single_row_columns( $item ) {
		list( $columns, $hidden ) = $this->get_column_info();

		foreach ( $columns as $column_name => $column_display_name ) {
			$class = "class='$column_name column-$column_name'";

			$style = '';
			if ( in_array( $column_name, $hidden ) )
				$style = ' style="display:none;"';

			$attributes = "$class$style";

			if ( 'cb' == $column_name ) {
				echo '<th scope="row" class="check-column">';
				echo $this->column_cb( $item );
				do_action( "manage_spectech_posts_custom_column", $column_name, $item );
				echo '</th>';
			}
			elseif ( method_exists( $this, 'column_' . $column_name ) ) {
				echo "<td $attributes>";
				echo call_user_func( array( $this, 'column_' . $column_name ), $item );
				do_action( "manage_spectech_posts_custom_column", $column_name, $item );
				echo "</td>";
			}
			else {
				echo "<td $attributes>";
				echo $this->column_default( $item, $column_name );
				do_action( "manage_spectech_posts_custom_column", $column_name, $item );
				echo "</td>";
			}
		}
	}

	function column_cb( $specification ) {
		return '<input type="checkbox" ' .
		              'name="post[]" ' .
		              'value="' . $specification->ID. '">';
	}

	function column_unit_id($specification){

		$unit_data=spectech_get_measurent();

		$unit_id=isset($specification->unit_id) ? $specification->unit_id : 0;
		$unit_label=isset($unit_data[$unit_id]['metric']['symbol']) ? $unit_data[$unit_id]['metric']['symbol'] : ($unit_id!='0'? $unit_id : __( 'none','spectech' ));

		return apply_filters('spectech_unit_single_row_view', $unit_label,$specification);
	}


	function column_field_name($specification){

		$can_edit_post = (!is_network_admin()) && current_user_can('manage_options');

		$row="<strong>";

		if ( $can_edit_post) {
			$row.='<a class="row-title" href="'.get_edit_spectech_field_link($specification->ID,'url').'" title="' . esc_attr__( 'edit','spectech' ) . '">'.esc_js($specification->title).'</a>';
		} else {
			$row.= esc_js($specification->title);
		}

		$row.="</strong>";


		$actions = array();

		if ( $can_edit_post ) {
			$actions['edit'] = '<a href="' . get_edit_spectech_field_link($specification->ID,'url') . '" title="' . esc_attr__( 'Edit','spectech' ) . '">' . __( 'Edit','spectech' ) . '</a>';

			$delete_post_link=wp_nonce_url(admin_url( sprintf( "edit.php?page=spectech-setting&post_type=specification&post[]=%d&action=delete", $specification->ID ) ), $specification->ID ,'delete_specification' );
			$actions['delete'] = "<a class='submitdelete' title='" . esc_attr__( 'Delete this item permanently' ) . "' href='" . $delete_post_link. "'>" . __( 'Delete Permanently' ) . "</a>";
		}

		$actions = apply_filters( 'spectech_row_actions', $actions, $specification );
		$action_count = count( $actions );

		if ( $action_count ){

			$i = 0;

			$row .= '<div class="row-actions">';
			foreach ( $actions as $action => $link ) {

			++$i;

			( $i == $action_count ) ? $sep = '' : $sep = ' | ';

				$row .= "<span class='$action'>$link$sep</span>";
			}
			$row .= '</div>';


		}

		return $row;
	}

	public static function get_form_fields( $per_page = 5, $page_number = 1 ) {

	  global $wpdb;

	  $table_name = SPECTECH_TABLE_NAME."_field";

	  $sql = "SELECT field_id AS ID,field_name AS title,t1.* FROM {$wpdb->prefix}{$table_name} AS t1";

	  if ( ! empty( $_REQUEST['orderby'] ) ) {
	    $sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
	    $sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
	  }

	  $sql .= " LIMIT $per_page";

	  $sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;

	  $result = $wpdb->get_results( $sql);

	  return $result;
	}

	public static function record_count() {
	  global $wpdb;

	  $table_name = SPECTECH_TABLE_NAME."_field";

	  $sql = "SELECT COUNT(*) FROM {$wpdb->prefix}{$table_name}";

	  return $wpdb->get_var( $sql );
	}

	function prepare_items() {

	    $per_page = "edit_spechtech_per_page";

	    if(isset( $_REQUEST[$per_page] ) && intVal($_REQUEST[$per_page])){
	    	update_user_option(get_current_user_id(), $per_page, intVal($_REQUEST[$per_page]));

	    }

		$columns = $this->get_columns();
		$hidden = array();
		$sortable = $this->get_sortable_columns();

		$this->_column_headers = array($columns, $hidden, $sortable);


		$total_items = self::record_count();

		$post_per_page = $this->get_items_per_page( $per_page , 10);
		$current_page = $this->get_pagenum();

		$this->items=self::get_form_fields( $post_per_page, $current_page );



		$this->set_pagination_args( array(
			'total_items' => $total_items,
			'per_page' => $post_per_page
		) );


	}

	function get_bulk_actions() {

		$actions=array();

		if ( current_user_can('manage_options') ) {
				$actions['delete'] = __( 'Delete','spectech' );
		}

		return $actions;
	}

	public function current_action() {
		if ( isset( $_REQUEST['delete_all'] ))
			return 'delete_all';

		return parent::current_action();
	}


	function process_bulk_action() {

		global $wpdb;

		$action = $this->current_action();


		if ( empty( $action ) ) {
			return;
		}

		if (!array_key_exists( 'post', $_REQUEST ) ) {
			return;
		}

		$field_ids=$_REQUEST['post'];

		if(is_array($field_ids)){
			$field_ids=array_map( 'stripslashes_deep', $field_ids );
		}
		else{
			$field_ids=array(intval($field_ids));
		}

		if( ! count($field_ids)){
			return;
		}


		switch ( $action ) {
			case 'delete':


					if ( ! current_user_can('manage_options') )
						wp_die( __('You are not allowed to delete this item.') );

					if ( !wp_spectech_delete_field_post($field_ids) )
						wp_die( __('Error in deleting.') );
				break;
		}

	}
	
	protected function extra_tablenav( $which ) {

		if ( 'top' == $which && !is_singular()) {
			$this->render_per_page_options();
		}

	}

	public function render_per_page_options() {

		$per_page = "edit_spechtech_per_page";

		$post_per_page = $this->get_items_per_page( $per_page,10);
		?>
		<div class="alignleft <?php print sanitize_html_class($per_page);?>">
				<label for="<?php echo esc_attr( $per_page ); ?>"><?php _e('Number of items per page:' ); ?></label>
				<input type="number" step="1" min="1" max="999" class="screen-per-page" name="<?php print $per_page;?>"
					id="<?php echo esc_attr( $per_page ); ?>" maxlength="3"
					value="<?php echo esc_attr( $post_per_page ); ?>" />

			<?php echo get_submit_button( __( 'Apply'), 'button', 'screen-options-apply', false ); ?>
		</div>
		<?php
	}

	public function display() {
		require_once (SPECTECH_DIR.'/lib/admin/templates/fields.php');
	}

}
?>