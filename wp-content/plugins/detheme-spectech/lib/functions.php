<?php
defined('ABSPATH') or die();
/** 
 * @version: 1.0.0
 * @since: 1.0.0
 */


function get_spectech_fields_list(){

    $fields = new Spectech_Table();
    $fields->process_bulk_action();
    $fields->prepare_items();
    $fields->display();

}

function get_spectech_field_post($field_id){

    global $wpdb;

    $table_name = SPECTECH_TABLE_NAME."_field";

    $query=$wpdb->prepare("SELECT * FROM {$wpdb->prefix}{$table_name} WHERE field_id='%d' LIMIT 1", $field_id);

    return $wpdb->get_row($query);
}

function get_spectech_fields(){

    global $wpdb;

    $table_name = SPECTECH_TABLE_NAME."_field";

    $query="SELECT * FROM {$wpdb->prefix}{$table_name} WHERE 1 ORDER BY field_group ASC";

    return $wpdb->get_results($query);
}


function wp_spectech_insert_post($specification=array()){

  global $wpdb;

  $table_name = SPECTECH_TABLE_NAME."_field";

  $specification=wp_parse_args($specification,array('field_id'=>'','field_name'=>'','field_group'=>'','unit_id'=>''));

  $query=$wpdb->prepare("INSERT INTO {$wpdb->prefix}{$table_name} (field_name,field_group,unit_id) VALUES ('%s','%s','%s')", $specification['field_name'], $specification['field_group'], $specification['unit_id']);
  $wpdb->query($query);

  return $wpdb->insert_id;
}

function wp_spectech_delete_field_post($field_ids=array()){

  if(!count($field_ids) || !current_user_can('manage_options')) return false;

  global $wpdb;

  $table_name = SPECTECH_TABLE_NAME."_field";
  $placeholders=implode(',', array_fill(0, count($field_ids),'%d'));
  $query=$wpdb->prepare("DELETE FROM {$wpdb->prefix}{$table_name} WHERE field_id IN ({$placeholders})", $field_ids);
  return $wpdb->query($query);
}


function wp_spectech_update_post($field_post){
    global $wpdb;

    $table_name = SPECTECH_TABLE_NAME."_field";

    $query=$wpdb->prepare("UPDATE {$wpdb->prefix}{$table_name} SET field_name='%s',field_group='%s',unit_id='%s' WHERE field_id='%d'", $field_post->field_name, $field_post->field_group, $field_post->unit_id, $field_post->field_id);

    return $wpdb->query($query);
}

function get_edit_spectech_field_link($id = 0, $context = 'display'){

    if ( ! $id )
        return;

    if(current_user_can('manage_options')){
      return admin_url( sprintf( "edit.php?page=spectech-setting&post_type=specification&spectech_id=%d&action=edit", $id ) );
    }

    return;
}

function spectech_get_field_groups(){

    global $wpdb;

    $table_name = SPECTECH_TABLE_NAME."_field";

    $query="SELECT DISTINCT field_group FROM {$wpdb->prefix}{$table_name} WHERE 1 ORDER BY field_group ASC ";

    return $wpdb->get_col($query);
}

function spectech_get_field_display($value="",$unit_id){

  $unit_data=spectech_get_measurent();
  $unit_label=isset($unit_data[$unit_id]['metric']['symbol']) ? $unit_data[$unit_id]['metric']['symbol'] : ($unit_id!='0'? $unit_id : "");
  $has_conversion=isset($unit_data[$unit_id]['imperial']['symbol']) ? true : false ;

  if($has_conversion){

    $value=(float) $value;
    $conversion= ($unit_data[$unit_id]['convertion']) ? (float) $unit_data[$unit_id]['convertion']: 0;

    $operation = isset($unit_data[$unit_id]['operation']) && !empty($unit_data[$unit_id]['operation']) ? trim($unit_data[$unit_id]['operation']) : "*";

    switch ($operation) {
      case '+':
            $converted_value= $value + $conversion;
        break;
      case '-':
            $converted_value= $value - $conversion;
        break;
      case '*': 
      default:
            $converted_value= $value * $conversion;
        break;
    }


    $imp_value= round($converted_value, 2);

    $return="<div class=\"spectech-specification has-conversion\" ".
    "data-metric-unit=\"".$unit_data[$unit_id]['metric']['symbol']."\" ".
    "data-imperial-unit=\"".$unit_data[$unit_id]['imperial']['symbol']."\" ".
    "data-metric-value=\"".$value."\" ".
    "data-imperial-value=\"".$imp_value."\"><span class=\"spectech-value\">{$value}</span><span class=\"spectech-unit-value\">{$unit_label}</span></div>";
  }
  else{
    $return="<div class=\"spectech-specification\"><span class=\"spectech-value\">{$value}</span><span class=\"spectech-unit-value\">{$unit_label}</span></div>";
  }

  return $return;;
}

function spectech_get_measurent(){

  $measurent=array(
    1 => array(
      'metric'=>array('symbol'=>'m',
                      'singular'=>__('meter','spectech'),
                      'plural'=>__('meters','spectech')
                ),
      'imperial'=>array('symbol'=>'ft',
                      'singular'=>__('feet','spectech'),
                      'plural'=>__('feets','spectech')
                ),
      'convertion'=>3.28084,
      ),
    2 => array(
      'metric'=>array('symbol'=>'km',
                      'singular'=>__('km','spectech'),
                      'plural'=>__('km','spectech')
                ),
      'imperial'=>array('symbol'=>'miles',
                      'singular'=>__('miles','spectech'),
                      'plural'=>__('miles','spectech')
                ),
      'convertion'=>0.621371
      ),
    3 => array(
      'metric'=>array('symbol'=>'kg',
                      'singular'=>__('kg','spectech'),
                      'plural'=>__('kgs','spectech')
                ),
      'imperial'=>array('symbol'=>'lb',
                      'singular'=>__('pound','spectech'),
                      'plural'=>__('pounds','spectech')
                ),
      'convertion'=>2.20462
    ),
    4 => array(
      'metric'=>array('symbol'=>'ton',
                      'singular'=>__('tonne','spectech'),
                      'plural'=>__('tonnes','spectech')
                ),
      'imperial'=>array('symbol'=>'lb',
                      'singular'=>__('pound','spectech'),
                      'plural'=>__('pounds','spectech')
                ),
      'convertion'=>2204.62
      ),
    5 => array(
      'metric'=>array('symbol'=>'m2',
                      'singular'=>__('square meter','spectech'),
                      'plural'=>__('square meters','spectech')
                ),
      'imperial'=>array('symbol'=>'ft2',
                      'singular'=>__('square foot','spectech'),
                      'plural'=>__('square foots ','spectech')
                ),
      'convertion'=>10.7639
      ),
    6 => array(
      'metric'=>array('symbol'=>'m/s',
                      'singular'=>__('meter/second','spectech'),
                      'plural'=>__('meters/second','spectech')
                ),
      'imperial'=>array('symbol'=>'ft/s',
                      'singular'=>__('foot/second','spectech'),
                      'plural'=>__('foots/second','spectech')
                ),
      'convertion'=>3.28084
      ),
    7 => array(
      'metric'=>array('symbol'=>'kmh',
                      'singular'=>__('kilo meter/hour','spectech'),
                      'plural'=>__('kilo meter/hour','spectech')
                ),
      'imperial'=>array('symbol'=>'mph',
                      'singular'=>__('mile/hour','spectech'),
                      'plural'=>__('miles/hour','spectech')
                ),
      'convertion'=>0.621371
      ),
    8 => array(
      'metric'=>array('symbol'=>'mm',
                      'singular'=>__('milimeter','spectech'),
                      'plural'=>__('milimeters','spectech')
                ),
      'imperial'=>array('symbol'=>'in',
                      'singular'=>__('inchi','spectech'),
                      'plural'=>__('inchi','spectech')
                ),
      'convertion'=>0.0393701
      ),
    9 => array(
      'metric'=>array('symbol'=>'km/l',
                      'singular'=>__('km/liter','spectech'),
                      'plural'=>__('km/liters','spectech')
                ),
      'imperial'=>array('symbol'=>'mile/gal',
                      'singular'=>__('mile/gallon','spectech'),
                      'plural'=>__('miles/gallon','spectech')
                ),
      'convertion'=>2.82481
      ),
    10 => array(
      'metric'=>array('symbol'=>'m/min',
                      'singular'=>__('meter/minute','spectech'),
                      'plural'=>__('meters/minute','spectech')
                ),
      'imperial'=>array('symbol'=>'ft/min',
                      'singular'=>__('foot/minute','spectech'),
                      'plural'=>__('foots/minute','spectech')
                ),
      'convertion'=>196.8504
      ),
    11 => array(
      'metric'=>array('symbol'=>'kw',
                      'singular'=>__('kilowatt','spectech'),
                      'plural'=>__('kilowatt','spectech')
                ),
      'imperial'=>array('symbol'=>'hp',
                      'singular'=>__('hoursepower','spectech'),
                      'plural'=>__('hoursepower','spectech')
                ),
      'convertion'=>1.34102209
      ),
    12 => array(
      'metric'=>array('symbol'=>'atm',
                      'singular'=>__('atmosphere','spectech'),
                      'plural'=>__('atmosphere','spectech')
                ),
      'imperial'=>array('symbol'=>'psi',
                      'singular'=>__('pound per square inch','spectech'),
                      'plural'=>__('pound per square inch','spectech')
                ),
      'convertion'=>14.6959
      ),
    13 => array(
      'metric'=>array('symbol'=>'C',
                      'singular'=>__('celcius','spectech'),
                      'plural'=>__('celcius','spectech')
                ),
      'imperial'=>array('symbol'=>'K',
                      'singular'=>__('kelvin','spectech'),
                      'plural'=>__('kelvin','spectech')
                ),
      'convertion'=>273.15,
      'operation'=>'+'
      ),
    );

  return apply_filters('spectech_get_measurent',$measurent);

}

function get_spectech_edit_page($field_id=0){

    $field_post=get_spectech_field_post($field_id);

    if(!$field_post){
      $field_post= new stdClass();
      $field_post->field_id='';
      $field_post->field_name="";
      $field_post->field_group="";
      $field_post->unit_id=0;
    }

    require_once (SPECTECH_DIR.'/lib/edit_field.php');
}
