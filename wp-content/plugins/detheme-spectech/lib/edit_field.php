<?php
defined('ABSPATH') or die();
/** 
 * @version: 1.0.0
 * @since: 1.0.0
 */

$measurenments=spectech_get_measurent();
$field_groups=spectech_get_field_groups();
?>
<div class="wrap">
<h1><?php print ($field_post->field_id) ? __('Edit Field', 'spectech').'<a href="'.admin_url("edit.php?post_type=specification&page=spectech-setting&action=new").'" class="btn page-title-action">'.__('create new field','spectech').'</a>' :  __('Add New Field', 'spectech');?></h1>
<form method="post" action="<?php print admin_url( "edit.php?page=spectech-setting&post_type=specification");?>">
<?php wp_nonce_field( 'spectech_form_nonce',SPECTECH_BASENAME);?>
<input name="field_id" id="field_id" max-length="50" value="<?php print $field_post->field_id;?>" class="" type="hidden">
<table class="form-table">
<tbody>
<tr>
<th scope="row"><label for="field_name"><?php _e('Specification','spectech');?></label></th>
<td>
<input name="field_name" id="field_name" max-length="50" value="<?php print $field_post->field_name;?>" class="" type="text"></td>
</tr>
<tr>
<th scope="row"><label for="field_group"><?php _e('Specification Group','spectech');?></label></th>
<td>
<?php if(count($field_groups)):?>
<select name="field_group" id="field_group">
<?php 
foreach ($field_groups as $field_group) {
    print "<option value='".$field_group."'".((strtolower($field_post->field_group)==strtolower($field_group))?" selected":"").">".$field_group."</option>";
}
?>
</select>
<span><?php _e('or create new','spectech');?></span> 
<?php endif;?>
<input name="new_field_group" id="new_field_group" max-length="50" value="" class="" type="text"></td>
</td>
</tr>
<tr>
<th scope="row"><label for="unit_id"><?php _e('Measurement Unit','spectech');?></label></th>
<td>
<select name="unit_id" id="unit_id">
<?php 
print "<option value='0'".(($field_post->unit_id=='0')?" selected":"").">".__('none','spectech')."</option>";

if(count($measurenments)):
  foreach ($measurenments as $unit_id => $measurenment) {
    if(isset($measurenment['metric']['symbol'])){
      print "<option value='".$unit_id."'".(($field_post->unit_id==$unit_id)?" selected":"").">".$measurenment['metric']['symbol']." - ".$measurenment['imperial']['symbol']."</option>";

    }
  }

endif;
?>
</select>

<span><?php _e('or custom text','spectech');?></span> 
<input name="custom_unit" id="custom_unit" max-length="50" value="<?php print ($field_post->unit_id!='0' && !intval($field_post->unit_id)) ? $field_post->unit_id : ""; ?>" class="" type="text"></td>
</td>
</tr>
</tbody></table>
<p class="submit"><input name="submit" id="submit" class="button button-primary" value="<?php print ($field_post->field_id) ? __('Save Changes') : __('Save Specification','spectech');?>" type="submit"></p>
</form>
</div>