<?php
defined('ABSPATH') or die();
/** 
 * @version: 1.0.0
 * @since: 1.0.0
 */

class detheme_spectech{

    function __construct() {

          if(!function_exists('is_plugin_active')){
            include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
          }

          register_activation_hook( SPECTECH_FILE, array($this,'prepareTable' ));
          add_action('init', array($this,'_init'));

          add_filter( 'pre_set_site_transient_update_plugins', array( $this, 'check_update' ) );
    }


    function prepareTable($network_wide){

      global $wpdb;
      if ( is_multisite() && is_network_admin() ) {


              // store the current blog id
              $current_blog = $wpdb->blogid;
              // Get all blogs in the network and activate plugin on each one
              $blog_ids = $wpdb->get_col( "SELECT blog_id FROM $wpdb->blogs" );
              foreach ( $blog_ids as $blog_id ) {
                  switch_to_blog( $blog_id );
                  $this->createTable();
                  restore_current_blog();
              }
      } else {
              $this->createTable();
      }
    }



    function createTable(){

        global $wpdb;

        $table_name = SPECTECH_TABLE_NAME."_field";
        $table_prefix = $wpdb->get_blog_prefix();

        if($wpdb->get_var("SHOW TABLES LIKE '".$table_prefix.$table_name."'") != $table_prefix.$table_name) {

            $query = "CREATE TABLE ".$table_prefix.$table_name ." (
                                  field_id int(9) NOT NULL AUTO_INCREMENT,
                                  field_group varchar(255),
                                  field_name varchar(64),
                                  unit_id varchar(64),
                                  UNIQUE KEY field_id (field_id)
                                );";  
            $wpdb->hide_errors();
            $result=$wpdb->query($query);

            $wpdb->show_errors();
            if(!$result){
                print $wpdb->last_error;
                exit;
           }         
        }
    }

  public function check_update($transient){


    if ( empty( $transient->checked ) ) {
      return $transient;
    }

   $plugin_slug = basename(dirname(dirname(__FILE__)));
   $plugin_name = $plugin_slug . '/spectech.php';

   $plugin_data = get_plugin_data( dirname(dirname(__FILE__)) . '/spectech.php' );
   $version= $plugin_data['Version'];
   $update= self::get_update($version);


   if ( $update) {
      $obj = new stdClass();
      $obj->slug = $plugin_slug;
      $obj->new_version = $update['new_version'];
      $obj->url = $update['url'];
      $obj->package = $update['package'];
      $obj->plugin = $plugin_name;
      $transient->response[$plugin_name] = $obj;
    }

    return $transient;
  }

  public function get_update($version){

     include ABSPATH . WPINC . '/version.php'; // include an unmodified $wp_version

      $options = array(
        'timeout' => 60,
        'body' => array(
          'version'=>$version,
        ),
        'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo( 'url' )
      );

      $url = $http_url = SPECTECH_UPDATE_URL."/update.php";

      $raw_response = wp_remote_post( $url, $options );

      if ( is_wp_error( $raw_response ) || 200 != wp_remote_retrieve_response_code( $raw_response ) ){
        return false;
      }

      $response = json_decode( wp_remote_retrieve_body( $raw_response ), true );
      return $response;
  }


  public function _init(){


      $admin      = get_role('administrator');
      $admin-> add_cap( 'specification_setting' );

      $this->register_post();

      add_action( 'admin_menu', array($this,'register_menu_page'));
      add_action( 'admin_menu', array( $this, 'field_spectech_export' ) ,-1);
      add_action( 'admin_init', array( $this, 'field_spectech_importer' ) );
      add_action( 'admin_init', array( $this, 'save_spectech_field' ) );
      add_action( 'admin_init', array( $this, 'setup_category_image' ) );
      add_action( 'admin_init', array( $this, 'prepareTable' ) );
      add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ), 'specification',30 );
      add_action( 'save_post', array($this,'save_spectech_post_fields') );
      add_filter( 'the_content' , array($this, 'spectech_add_specification'),999);

      self::load_front_script();


  }

  function field_spectech_importer() {

    if ( ! defined( 'WP_LOAD_IMPORTERS' ) )
    return;

    // Load Importer API
    require_once ABSPATH . 'wp-admin/includes/import.php';

    if ( ! class_exists( 'WP_Importer' ) ) {
      $class_wp_importer = ABSPATH . 'wp-admin/includes/class-wp-importer.php';
      if ( file_exists( $class_wp_importer ) )
        require $class_wp_importer;
    }
    if(!class_exists('WP_Filesystem_Direct')){

      require_once(ABSPATH . 'wp-admin/includes/class-wp-filesystem-base.php');
      require_once(ABSPATH . 'wp-admin/includes/class-wp-filesystem-direct.php');
    }

    require_once ( plugin_dir_path(__FILE__).'importer.class.php');

    $GLOBALS['spectech_import'] = new WP_Spectech_Field_Import();
    register_importer( 'spectech_field_import', __('Specification: Field Import','spectech'), __('Import <strong>specification field</strong> from a json file.','spectech'), array( $GLOBALS['spectech_import'], 'dispatch' ) );
  }


  function setup_category_image(){

      if(get_option( 'db_version' ) >= 34370 ) {

            add_action( 'spectech_cat_edit_form', array( $this, 'add_category_image_edit'),10,2);
            add_action( 'spectech_cat_add_form', array( $this, 'add_category_image_edit'));
            add_action( 'edit_term', array( $this, 'save_category_image' ), 10, 3 );
            add_action( 'create_term', array($this, 'save_category_image'), 10, 3 );

      }

  }

  function add_meta_boxes(){


       add_meta_box( 'specificationfield', __( 'Technical Specifications', 'spectech' ),array($this,'post_custom_meta_box'), 'specification','normal','high');
  }


  public static function get_spectech_post_fields($post){

    if(!($fields=get_spectech_fields()))  return array();


    $output=array();

    foreach($fields as $field){
      $metaname="_spectech_field_".sanitize_key($field->field_name);
      $field->value=get_post_meta( $post->ID, $metaname, true );
      $output[$field->field_group][$field->field_id]=$field;
    }

    return $output;
  }

  function save_spectech_post_fields($post_id){

    if ( (defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE) || !isset($_POST[SPECTECH_BASENAME]))
     return $post_id;

   if(!wp_verify_nonce( $_POST[SPECTECH_BASENAME], 'spectech_post_specification_form_nonce'.$post_id)) return;

   if(!($fields = get_spectech_fields())) return $post_id;

   foreach($fields as $field){

    $metaname="_spectech_field_".sanitize_key($field->field_name);

    $old = get_post_meta( $post_id, $metaname, true );
    $new = (isset($_POST[$metaname]))?$_POST[$metaname]:'';
    update_post_meta( $post_id, $metaname, $new,$old );
   } 

    $old = get_post_meta( $post_id, '_spectech_download', true );
    $new = (isset($_POST['spectech_download']))?$_POST['spectech_download']:'';
    update_post_meta( $post_id, '_spectech_download', $new,$old );

  }

  function post_custom_meta_box($post) {

      global $wp_locale;


      wp_enqueue_style( 'spectech-admin-css',SPECTECH_DIR_URL."lib/admin/css/admin.css?".time(),array());
      $fields=self::get_spectech_post_fields($post);
      $unit_data=spectech_get_measurent();
      $spectech_download = get_post_meta( $post->ID, '_spectech_download', true );


      if(count($fields)):
  ?>
<div class="spectech-metabox">
<?php wp_nonce_field( 'spectech_post_specification_form_nonce'.$post->ID,SPECTECH_BASENAME);?>

<?php foreach($fields as $group => $group_fields):?>  
<h2 class="group-name"><?php print $group;?></h2>
  <table border="0" cellpadding="0" width="100%">
<?php foreach ($group_fields as $key => $field) {

    $metaname="_spectech_field_".sanitize_key($field->field_name);


    $unit_id=isset($field->unit_id) ? $field->unit_id : 0;
    $unit_label=isset($unit_data[$unit_id]['metric']['symbol']) ? $unit_data[$unit_id]['metric']['symbol'] : ($unit_id!='0'? $unit_id : "");


      ?>
      <tr>
      <td width="200"><strong><?php print $field->field_name; ?></strong></td>
      <td><input name="<?php print $metaname;?>" type="text" id="<?php print $metaname;?>" class="field-input" value="<?php echo esc_attr( $field->value); ?>" /> <span class="unit-measurenment"><?php print $unit_label; ?></span></td>
      
      </tr>
<?php };?>
  </table>

<?php endforeach;?>
<h2 class="group-name"><?php esc_html_e('Download/Brochure','spectech');?></h2>
  <table border="0" cellpadding="0" width="100%">
      <tr>
      <td width="200"><strong><?php esc_html_e('File URL','spectech'); ?></strong></td>
      <td><input name="spectech_download" type="text" id="spectech_download" class="widefat field-input" value="<?php print !empty($spectech_download) ? $spectech_download: "";?>" /></td>
      </tr>
  </table>
</div>
  <?php
      endif;
  }

  function field_spectech_export(){

     if(!isset($_POST[SPECTECH_BASENAME])) return;
     if(!wp_verify_nonce( $_POST[SPECTECH_BASENAME], 'spectech_export_specification_form_nonce')) return;

      $sitename = sanitize_key( get_bloginfo( 'name' ) );
      if ( ! empty( $sitename ) ) {
        $sitename .= '.';
      }
      $date = date( 'Y-m-d' );
      $wp_filename = "spectech-field.".$sitename . 'wordpress.' . $date . '.json';

      global $wpdb;

      $table_name = $wpdb->get_blog_prefix().SPECTECH_TABLE_NAME."_field";

      $query="SELECT field_group,field_name,unit_id FROM {$table_name} WHERE 1";

      $results=$wpdb->get_results($query);

      if($results){
        header( 'Content-Description: File Transfer' );
        header( 'Content-Disposition: attachment; filename='.$wp_filename);
        header( 'Content-Type: application/json; charset=' . get_option( 'blog_charset' ), true );
        print json_encode($results);
        }
      exit;
  }

  function save_spectech_field(){

      if(!isset($_POST[SPECTECH_BASENAME])) return;

      if(!wp_verify_nonce( $_POST[SPECTECH_BASENAME], 'spectech_form_nonce')) return;

      $field_id=isset($_REQUEST['field_id']) ? intval($_REQUEST['field_id']) : 0;

      $field_name=isset($_REQUEST['field_name']) ? sanitize_text_field($_REQUEST['field_name']) : "";
      $field_group=isset($_REQUEST['field_group']) ? sanitize_text_field($_REQUEST['field_group']) : "";
      $unit_id=isset($_REQUEST['unit_id']) ? intval($_REQUEST['unit_id']) : '';
      $new_field_group=isset($_REQUEST['new_field_group']) ? sanitize_text_field($_REQUEST['new_field_group']) : "";
      $custom_unit=isset($_REQUEST['custom_unit']) ? sanitize_text_field(trim($_REQUEST['custom_unit'])) : "";


      $form_fields=get_spectech_field_post($field_id);

      if(!empty($new_field_group)) $field_group = $new_field_group;
      if(!empty($custom_unit)) $unit_id = $custom_unit;

      if(!$form_fields || !$field_id){


          $field_id=wp_spectech_insert_post(array('field_name'=>$field_name, 'field_group'=>$field_group, 'unit_id'=> $unit_id));


          $redirect=get_edit_spectech_field_link($field_id, 'url');
          wp_redirect($redirect);
          exit;

      }
      else{

            $form_fields->field_name=$field_name;
            $form_fields->field_group=$field_group;
            $form_fields->unit_id=$unit_id;

            wp_spectech_update_post($form_fields);


            $redirect=get_edit_spectech_field_link($field_id,'url');
            wp_redirect($redirect);
            exit;


      }
    }

 
   function register_menu_page(){

      if(!is_network_admin()) {

       add_submenu_page( 'edit.php?post_type=specification', __('Specification Field Settings', 'spectech'), __('Specification Field', 'spectech'),'specification_setting','spectech-setting', array($this,'specification_fields_page'));
       add_submenu_page( 'edit.php?post_type=specification', __('Import/Export Fields', 'spectech'), __('Import/Export Fields', 'spectech'),'specification_setting','spectech-import', array($this,'specification_import_page'));

     }
    }

    function register_post(){

        $post_settings=array(
                'labels' => array(
                    'name' => __('Specifications', 'spectech'),
                    'singular_name' => __('Specification', 'spectech'),
                    'add_new' => __('Add New Specification', 'spectech'),
                    'edit_item' => __('Edit Specification', 'spectech')
                ),
                'public' => true,
                'show_ui' => true,
                'show_in_nav_menus' => true,
                'rewrite' => array(
                    'slug' => 'specification',
                    'with_front' => false
                ),
                'has_archive'=>true,
                'taxonomies'=>array('post_tag','spectech_cat'),
                'hierarchical' => true,
                'menu_position' => 5,
                'supports' => array(
                    'title',
    //                'custom-fields',
                    'editor',
                    'excerpt',
                    'thumbnail'
                )
        );

        register_post_type('specification', $post_settings);
        register_taxonomy('spectech_cat', 'specification', array('hierarchical' => true, 'label' => sprintf(__('%s Category', 'spectech'),__('Specification', 'spectech')), 'singular_name' => __('Category')));

    }

    function specification_import_page(){
?>
<div class="wrap">
<h1><?php _e('Import/Export Specification Field', 'spectech');?></h1>
<p><?php _e( 'Select action below to import or export specification field.','spectech');?></p>
<form method="post" action="<?php print admin_url( "edit.php?page=spectech-import&post_type=specification");?>" target="_blank">
 <?php wp_nonce_field( 'spectech_export_specification_form_nonce',SPECTECH_BASENAME);?>
<p class="submit"><input name="import_action" class="button button-primary" value="<?php _e('Export Specification Fields','spectech');?>" type="submit"> <?php _e('or','spectech');?> <a href="<?php print esc_url(admin_url('admin.php?import=spectech_field_import'));?>" class="button button-primary"><?php _e('Import Specification Fields','spectech');?></a></p>
</form>
</div>
<?php

    }

    function specification_fields_page(){

        if ( empty( $_POST[ 'action' ] ) ) {
            if ( empty( $_GET[ 'action' ] ) ) {
                $action = '';
            } else {
                $action = $_GET[ 'action' ];
            }
        } else {
            $action = $_POST[ 'action' ];
        }

        if ( isset( $_GET['spectech_id'] ) )
            $field_id = (int) $_GET['spectech_id'];
        elseif ( isset( $_POST['spectech_id'] ) )
            $field_id = (int) $_POST['spectech_id'];
        else
            $field_id = 0;

        switch ($action) {
            case 'edit':
            case 'new':
                         get_spectech_edit_page($field_id);
                break;
            default:
                        get_spectech_fields_list();
                break;
        }


    }

    public static function load_front_script(){

        wp_enqueue_style('spectech-style', SPECTECH_DIR_URL.'css/style.css');
        wp_enqueue_script('spectech-script',SPECTECH_DIR_URL."/js/script.js",array( 'jquery' ));
    }

    function spectech_add_specification($content){

      $post=get_post();

      if(!$post || 'specification'!=$post->post_type) return $content;


        if(!is_single()) return $content;


        $templateName="specification";

        if ( $templateName ) {
            $template = locate_template( array( "{$templateName}.php","detheme-spectech/{$templateName}.php","templates/{$templateName}.php" ),false );
        }

        // Get default slug-name.php
        if ( ! $template && $templateName && file_exists( SPECTECH_DIR. "templates/{$templateName}.php" ) ) {

            $template = SPECTECH_DIR. "templates/{$templateName}.php";
        }

        // Allow 3rd party plugin filter template file from their plugin
        $template = apply_filters( 'detheme_spectech_get_template_part', $template,$templateName );

        if ( $template ) {

            $specification_fields=self::get_spectech_post_fields($post);
            $post->specifications=$specification_fields;
            ob_start();
            load_template( $template, false );
            $content.=ob_get_clean();
        }

      return $content;
    }



  function add_category_image_edit($tag, $taxonomy=""){

  wp_enqueue_media();
  wp_enqueue_script('spectech-media-script',SPECTECH_DIR_URL."lib/admin/js/categoryimage.js",array( 'jquery' ));

  wp_localize_script(
      'spectech-media-script',
      'CategoryImage',
      array(
          'label'      => array(
              'title'  => __('Choose Category Image', 'wpcustom-category-image'),
              'button' => __('Choose Image', 'wpcustom-category-image')
          )
      )
  );

  $term_id= is_object($tag) && $tag->term_id ? $tag->term_id : 0;


  $category_image=get_term_meta($term_id, '_category_image', true);

  $image_url="";
  $category_image_data = wp_get_attachment_image_url( $category_image, array( 266,266 ));

  if($category_image && ($category_image_data = wp_get_attachment_image_url( $category_image, array( 266,266 )))) {
     $image_url = $category_image_data;
  }

  ?>
<table class="form-table"> 
<tr class="form-field">
    <th scope="row">
        <label for="taxonomy_image"><?php _e('Image','spectech') ?></label>
    </th>
    <td>
        <input type="hidden" name="categoryimage_attachment" id="categoryimage_attachment" value="<?php echo $category_image; ?>" />

        <div id="categoryimage_imageholder">
            <?php if (!empty($image_url)): ?>
                <img src="<?php echo $image_url; ?>" width="180" id="categoryimage_image" />
            <?php endif; ?>
        </div>

        <div class="options">
            <button class="button" id="categoryimage_upload_button"><?php _e('Upload','spectech');?></button>
            <button class="button" id="categoryimage_remove_button"><?php _e('Remove Image','spectech');?></button>
        </div>
    </td>
</tr>
</table>
<?php
  }

  function save_category_image($term_id){

        $attachment_id = isset($_POST['categoryimage_attachment']) ? (int) $_POST['categoryimage_attachment'] : null;

        $old_image=get_term_meta($term_id, '_category_image', true);
        $new_image='';

        if (! is_null($attachment_id) && $attachment_id > 0 && !empty($attachment_id)) {

          $new_image=$attachment_id;

        }

        update_term_meta($term_id, '_category_image', $new_image, $old_image);
  }
}
