<?php
/**
 * WordPress için taban ayar dosyası.
 *
 * Bu dosya şu ayarları içerir: MySQL ayarları, tablo öneki,
 * gizli anahtaralr ve ABSPATH. Daha fazla bilgi için
 * {@link https://codex.wordpress.org/Editing_wp-config.php wp-config.php düzenleme}
 * yardım sayfasına göz atabilirsiniz. MySQL ayarlarınızı servis sağlayıcınızdan edinebilirsiniz.
 *
 * Bu dosya kurulum sırasında wp-config.php dosyasının oluşturulabilmesi için
 * kullanılır. İsterseniz bu dosyayı kopyalayıp, ismini "wp-config.php" olarak değiştirip,
 * değerleri girerek de kullanabilirsiniz.
 *
 * @package WordPress
 */

// ** MySQL ayarları - Bu bilgileri sunucunuzdan alabilirsiniz ** //
/** WordPress için kullanılacak veritabanının adı */
define( 'DB_NAME', 'pnomatik' );

/** MySQL veritabanı kullanıcısı */
define( 'DB_USER', 'root' );

/** MySQL veritabanı parolası */
define( 'DB_PASSWORD', 'root' );

/** MySQL sunucusu */
define( 'DB_HOST', 'localhost' );

/** Yaratılacak tablolar için veritabanı karakter seti. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Veritabanı karşılaştırma tipi. Herhangi bir şüpheniz varsa bu değeri değiştirmeyin. */
define('DB_COLLATE', '');

/**#@+
 * Eşsiz doğrulama anahtarları.
 *
 * Her anahtar farklı bir karakter kümesi olmalı!
 * {@link http://api.wordpress.org/secret-key/1.1/salt WordPress.org secret-key service} servisini kullanarak yaratabilirsiniz.
 * Çerezleri geçersiz kılmak için istediğiniz zaman bu değerleri değiştirebilirsiniz. Bu tüm kullanıcıların tekrar giriş yapmasını gerektirecektir.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'YzunRp:+g8*T#>Z<I,KyZ`Hc$N>Zwb}dS8DNT4d@>n-]-`XKXK!fs<1/?;&2PG`:' );
define( 'SECURE_AUTH_KEY',  'hekHsC<,~X91PTxk~^4W* x<ZfW^9 SS)68b@.2o`~74`t zFo|Zj]mB`>42MYs:' );
define( 'LOGGED_IN_KEY',    '~k#&NPi$VF=5qC1J,KZ9c(P<Q*AJ?7[v0vM j<n7(YB_DPlkAaIugX EDeBpi5jq' );
define( 'NONCE_KEY',        'B>Gl-tbI7T8l+&zgt2Pl+_qe][D>kWzHVwUugZZh#+@%i(]rj8K}*7AtWh^|BL}9' );
define( 'AUTH_SALT',        'lj@KuEjJ>Yw0o}%;k:4U?VW%22>w{=3DBPW0-rgw-|ly rmZsMI@1u&I.zknz:T8' );
define( 'SECURE_AUTH_SALT', '1i).H p9|3UE2lkhAhhqL_6`K}0y=5S;iYA?LM2<(^Yc0wn.riS(m_O`S?F@IA4d' );
define( 'LOGGED_IN_SALT',   '7iAS&SKk,8w_wca|i;u$#1P+JFdI{!iIEQyWA2]5[6F7-pE&|vl@.2P4Dma>,&!b' );
define( 'NONCE_SALT',       '${N*m$P_in7#}9=9+Er%dK>z0MyeW+D&mxYSsFJt0:v88yl.)a.3 l5qL9}xV-M{' );
/**#@-*/

/**
 * WordPress veritabanı tablo ön eki.
 *
 * Tüm kurulumlara ayrı bir önek vererek bir veritabanına birden fazla kurulum yapabilirsiniz.
 * Sadece rakamlar, harfler ve alt çizgi lütfen.
 */
$table_prefix = 'wp_';

/**
 * Geliştiriciler için: WordPress hata ayıklama modu.
 *
 * Bu değeri "true" yaparak geliştirme sırasında hataların ekrana basılmasını sağlayabilirsiniz.
 * Tema ve eklenti geliştiricilerinin geliştirme aşamasında WP_DEBUG
 * kullanmalarını önemle tavsiye ederiz.
 */
define('WP_DEBUG', false);

/* Hepsi bu kadar. Mutlu bloglamalar! */

/** WordPress dizini için mutlak yol. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** WordPress değişkenlerini ve yollarını kurar. */
require_once(ABSPATH . 'wp-settings.php');
